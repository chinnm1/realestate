package com.devcam.shop24h.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Street;

import com.devcam.shop24h.service.StreetService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")

public class StreetController {

    @Autowired
    StreetService streetService;

    @GetMapping("/street")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<List<Street>> getStreet() {
        try {
            return streetService.getStreet();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all_street")

    public ResponseEntity<List<Street>> getAllStreet() {
        try {
            return streetService.getStreet();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/street/{districtId}/{provinceId}")
    public ResponseEntity<List<Street>> getStreetByDistrictIdAndProvinceId(@PathVariable("districtId") int districtid,
            @PathVariable("provinceId") int provinceid) {
        try {
            return streetService.getStreetByDistrictIdAndProvinceId(districtid, provinceid);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/street/{id}")
    public ResponseEntity<Optional<Street>> getStreetById(@PathVariable("id") int id) {
        try {
            return streetService.getStreetById(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/street/create/{districtId}/{provinceId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> createStreet(@PathVariable("districtId") int districtid,
            @PathVariable("provinceId") int provinceid,
            @RequestBody Street street) {
        try {
            return streetService.createStreet(districtid, provinceid, street);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified street: " +
                            e.getCause().getCause().getMessage());
        }

    }

    @PutMapping("/street/update/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> updateStreet(@PathVariable("id") int id, @RequestBody Street street) {
        try {
            return streetService.updateStreet(id, street);

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified street: " + id + "  for update.");
        }

    }

    @DeleteMapping("/street/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> deleteStreetById(@PathVariable int id) {
        try {
            return streetService.deleteStreetById(id);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
