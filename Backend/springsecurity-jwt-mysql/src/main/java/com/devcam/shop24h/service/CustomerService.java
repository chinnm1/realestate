package com.devcam.shop24h.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.management.ObjectName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcam.shop24h.entity.Customer;
import com.devcam.shop24h.entity.Realestate;
import com.devcam.shop24h.entity.User;
import com.devcam.shop24h.repository.CustomerRepository;
import com.devcam.shop24h.repository.RealestateRepository;
import com.devcam.shop24h.repository.UserRepository;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    RealestateRepository realestateRepository;
    @Autowired
    UserRepository userRepository;

    // lấy ra tất cả khách hàng
    public ResponseEntity<List<Customer>> getCustomer() {

        List<Customer> listCustomer = customerRepository.findAll();
        if (!listCustomer.isEmpty()) {
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // lấy ra khách hàng theo id
    public ResponseEntity<Optional<Customer>> getCustomerById(int id) {
        Optional<Customer> customer = customerRepository.findCustomerById(id);
        if (!customer.isEmpty()) {
            return new ResponseEntity<>(customer, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // lấy danh sách khách hàng theo tin đăng
    public ResponseEntity<List<Customer>> getCustomerByRealestateId(int id) {
        List<Customer> listCustomer = customerRepository.findCustomerByRealesteId(id);
        if (!listCustomer.isEmpty()) {
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }
    // update tình trạng khách hàng đã liên lac

    // tạo khách hàng mới theo realestate id
    public ResponseEntity<Object> createCustomer(int id, Customer customer) {
        Optional<Realestate> realestateData = realestateRepository.findRealestateByRealeastedId(id);
        if (realestateData.isPresent()) {
            Customer newRole = new Customer();
            newRole.setContactName(customer.getContactName());
            newRole.setContactTitle(customer.getContactTitle());
            newRole.setMobile(customer.getMobile());
            newRole.setEmail(customer.getEmail());
            newRole.setNote(customer.getNote());
            newRole.setCreatedAt(new Date());
            newRole.setUsername(customer.getUsername());
            Realestate _realestate = realestateData.get();
            newRole.setRealestates(_realestate);
            Customer savedRole = customerRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // tạo mới khách hàng không theo realestate id
    public ResponseEntity<Object> createCustomerWithoutRealestate(Customer customer) {
        Customer newRole = new Customer();
        newRole.setContactName(customer.getContactName());
        newRole.setMobile(customer.getMobile());
        newRole.setEmail(customer.getEmail());
        newRole.setUsername(customer.getUsername());
        // newRole.setRealestates();
        Customer savedRole = customerRepository.save(newRole);
        return new ResponseEntity<>(savedRole, HttpStatus.CREATED);

    }

    // cập nhật khách hàng
    public ResponseEntity<Object> updateCustomer(int id, Customer customer) {
        Optional<Customer> customerData = customerRepository.findCustomerById(id);
        if (customerData.isPresent()) {
            Customer newRole = customerData.get();
            newRole.setContactName(customer.getContactName());
            newRole.setContactTitle(customer.getContactTitle());
            newRole.setMobile(customer.getMobile());
            newRole.setEmail(customer.getEmail());
            newRole.setNote(customer.getNote());
            newRole.setUpdatedAt(new Date());
            newRole.setUserRequest(customer.getUserRequest());
            newRole.setUsername(customer.getUsername());
            Customer savedRole = customerRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // cập nhật request của khách hàng
    public ResponseEntity<Object> updateCustomerRequest(int userId, Customer customer) {
        Optional<User> userCreate = userRepository.findById(Long.valueOf(userId));

        Optional<Customer> customerUpdate = customerRepository.findCustomerByUsername(userCreate.get().getUsername());
        if (customerUpdate.isPresent()) {
            Customer newRole = customerUpdate.get();
            newRole.setUserRequest(customer.getUserRequest());
            Customer savedRole = customerRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    // cập nhật tình trạng liên lạc khách hàng

    // cập nhật request của khách hàng
    public ResponseEntity<Object> updateCustomerContact(int customerId, Customer customer) {
        Optional<Customer> customerUpdate = customerRepository.findCustomerById(customerId);
        if (customerUpdate.isPresent()) {
            Customer newRole = customerUpdate.get();
            newRole.setIsContact(customer.getIsContact());
            Customer savedRole = customerRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // cập nhật request khách hàng và realestateId cho người bán
    public ResponseEntity<Object> updateCustomerRequestAndRealestateForSeller(int userId, int realestateId) {
        Optional<User> userCreate = userRepository.findById(Long.valueOf(userId));
        Optional<Realestate> realestateData = realestateRepository.findRealestateByRealeastedId(realestateId);
        Optional<Customer> customerUpdate = customerRepository.findCustomerByUsername(userCreate.get().getUsername());
        if (customerUpdate.isPresent() && realestateData.isPresent()) {
            Customer newRole = customerUpdate.get();
            newRole.setUserRequest("cần bán");
            Realestate _realestate = realestateData.get();
            newRole.setRealestates(_realestate);
            Customer savedRole = customerRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // cập nhật request khách hàng và realestateId cho người mua
    public ResponseEntity<Object> updateCustomerRequestAndRealestateForBuyer(int userId, int realestateId,
            Customer customer) {
        Optional<User> userCreate = userRepository.findById(Long.valueOf(userId));
        Optional<Realestate> realestateData = realestateRepository.findRealestateByRealeastedId(realestateId);
        Optional<Customer> customerUpdate = customerRepository.findCustomerByUsername(userCreate.get().getUsername());
        if (customerUpdate.isPresent() && realestateData.isPresent()) {
            Customer newRole = customerUpdate.get();
            newRole.setUserRequest(customer.getUserRequest());
            newRole.setAddress(customer.getAddress());
            newRole.setContactTitle(customer.getContactTitle());
            Realestate _realestate = realestateData.get();
            newRole.setRealestates(_realestate);
            Customer savedRole = customerRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // xóa khách hàng
    public ResponseEntity<Object> deleteCustomerById(int id) {

        Optional<Customer> customerData = customerRepository.findCustomerById(id);
        Customer customerDelete = customerData.get();
        customerRepository.delete(customerDelete);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
