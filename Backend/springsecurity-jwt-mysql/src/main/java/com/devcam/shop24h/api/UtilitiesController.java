package com.devcam.shop24h.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Utilities;
import com.devcam.shop24h.repository.UtilitiesRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class UtilitiesController {
    @Autowired
    UtilitiesRepository utilitiesRepository;

    @GetMapping("/utilities/detail/{id}")
    public ResponseEntity<List<Utilities>> getUtilitiesByProjectId(@PathVariable int id) {
        try {
            List<Utilities> UtilitiesData = utilitiesRepository.findUtilitiesByProjectId(id);
            if (!UtilitiesData.isEmpty()) {
                return new ResponseEntity<>(UtilitiesData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
