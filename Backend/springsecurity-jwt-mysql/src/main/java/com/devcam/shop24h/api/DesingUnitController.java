package com.devcam.shop24h.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.DesignUnit;
import com.devcam.shop24h.repository.DesignUnitRepository;
import com.devcam.shop24h.service.DesignUnitService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class DesingUnitController {
    @Autowired
    DesignUnitRepository designUnitRepository;
    @Autowired
    DesignUnitService designUnitService;

    @GetMapping("/designunit")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<List<DesignUnit>> getDesignunit() {
        try {
            return designUnitService.getDesignunit();
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all_designunit")

    public ResponseEntity<List<DesignUnit>> getAllDesignunit() {
        try {
            return designUnitService.getDesignunit();
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/designunit/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Optional<DesignUnit>> getDesignunitById(@PathVariable("id") int id) {
        try {
            return designUnitService.getDesignunitById(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/designunit/create")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> createDesignunit(@RequestBody DesignUnit designUnit) {
        try {
            return designUnitService.createDesignunit(designUnit);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified design unit: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/designunit/update/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> updateDesignUnit(@PathVariable("id") int id, @RequestBody DesignUnit designuntir) {
        try {
            return designUnitService.updateDesignUnit(id, designuntir);

        } catch (Exception e) {

            return ResponseEntity.badRequest().body("Failed to get specified designunit: " + id + "  for update.");
        }

    }

    @DeleteMapping("/designunit/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> deletedesignuntirById(@PathVariable int id) {
        try {
            return designUnitService.deletedesignuntirById(id);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
