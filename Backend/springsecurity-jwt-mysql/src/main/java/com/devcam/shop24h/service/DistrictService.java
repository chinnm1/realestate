package com.devcam.shop24h.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcam.shop24h.entity.District;
import com.devcam.shop24h.entity.Province;
import com.devcam.shop24h.repository.DistrictRepository;
import com.devcam.shop24h.repository.ProvinceRepository;

@Service
public class DistrictService {
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    ProvinceRepository provinceRepository;

    // lấy ra tát cả quận huyện
    public ResponseEntity<List<District>> getDistrict() {

        List<District> listDistrict = districtRepository.findAll();
        if (!listDistrict.isEmpty()) {
            return new ResponseEntity<>(listDistrict, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // lấy ra quận huyện theo tỉnh thành phố
    public ResponseEntity<List<District>> getDistrictByProvinceId(int id) {

        List<District> listDistrict = districtRepository.findDistrictByProvinceId(id);
        if (!listDistrict.isEmpty()) {
            return new ResponseEntity<>(listDistrict, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // lấy ra quận huyện theo id
    public ResponseEntity<Optional<District>> getDistrictById(int id) {

        Optional<District> district = districtRepository.findDistrictById(id);
        if (!district.isEmpty()) {
            return new ResponseEntity<>(district, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // tạo mới quận huyện
    public ResponseEntity<Object> createDistrict(int id,

            District district) {

        Optional<Province> provincelData = provinceRepository.findProvinceById(id);

        if (provincelData.isPresent()) {
            District newRole = new District();
            newRole.setName(district.getName());
            newRole.setPrefix(district.getPrefix());
            Province _province = provincelData.get();
            newRole.setProvince(_province);

            District savedRole = districtRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // cập nhật quận huyện
    public ResponseEntity<Object> updateDistrict(int id, District district) {

        Optional<District> districtData = districtRepository.findDistrictById(id);
        if (districtData.isPresent()) {
            District newRole = districtData.get();
            newRole.setName(district.getName());
            newRole.setPrefix(district.getPrefix());
            District savedRole = districtRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // xóa quận huyện
    public ResponseEntity<Object> deleteDistrictById(int id) {

        Optional<District> districtData = districtRepository.findDistrictById(id);
        District districtDelete = districtData.get();
        districtRepository.delete(districtDelete);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
