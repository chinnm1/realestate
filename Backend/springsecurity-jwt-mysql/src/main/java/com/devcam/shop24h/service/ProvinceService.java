package com.devcam.shop24h.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcam.shop24h.entity.Province;
import com.devcam.shop24h.repository.ProvinceRepository;

@Service
public class ProvinceService {
    @Autowired
    ProvinceRepository provinceRepository;

    // lấy ra tất cả thành phố
    public ResponseEntity<List<Province>> getProvince() {

        List<Province> listRealestate = provinceRepository.findAll();
        if (!listRealestate.isEmpty()) {
            return new ResponseEntity<>(listRealestate, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // lấy ra tỉnh theo id
    public ResponseEntity<Optional<Province>> getProvinceById(int id) {

        Optional<Province> listRealestate = provinceRepository.findProvinceById(id);
        if (!listRealestate.isEmpty()) {
            return new ResponseEntity<>(listRealestate, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // tạo mưới tỉnh thành phố
    public ResponseEntity<Object> createProvince(Province province) {
        Province newRole = new Province();
        newRole.setName(province.getName());
        newRole.setCode(province.getCode());
        Province savedRole = provinceRepository.save(newRole);
        return new ResponseEntity<>(savedRole, HttpStatus.CREATED);

    }

    // cập nhật tỉnh thành phố
    public ResponseEntity<Object> updateProvince(int id, Province province) {

        Optional<Province> provinceData = provinceRepository.findProvinceById(id);
        if (provinceData.isPresent()) {
            Province newRole = provinceData.get();
            newRole.setName(province.getName());
            newRole.setCode(province.getCode());
            Province savedProvince = provinceRepository.save(newRole);
            return new ResponseEntity<>(savedProvince, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // xóa tỉnh thành phố
    public ResponseEntity<Object> deleteProvinceById(int id) {

        Optional<Province> provinceData = provinceRepository.findProvinceById(id);
        Province provinceDelete = provinceData.get();
        provinceRepository.delete(provinceDelete);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
