package com.devcam.shop24h.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Investor;
import com.devcam.shop24h.repository.InvestorRepository;
import com.devcam.shop24h.service.InvestorService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class InvestorController {
    @Autowired
    InvestorRepository investorRepository;
    @Autowired
    InvestorService investorService;

    @GetMapping("/investor")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<List<Investor>> getInvestor() {
        try {
            return investorService.getInvestor();
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all_investor")
    public ResponseEntity<List<Investor>> getAllInvestor() {
        try {
            return investorService.getInvestor();
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/investor/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Optional<Investor>> getInvestorById(@PathVariable("id") int id) {
        try {
            return investorService.getInvestorById(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/investor/create")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> createInvestor(@RequestBody Investor investor) {
        try {
            return investorService.createInvestor(investor);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified investor: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/investor/update/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> updateEmployee(@PathVariable("id") int id, @RequestBody Investor investor) {
        try {
            return investorService.updateEmployee(id, investor);
        } catch (Exception e) {

            return ResponseEntity.badRequest().body("Failed to get specified investor: " + id + "  for update.");
        }

    }

    @DeleteMapping("/investor/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> deleteInvestorById(@PathVariable int id) {
        try {
            return investorService.deleteInvestorById(id);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
