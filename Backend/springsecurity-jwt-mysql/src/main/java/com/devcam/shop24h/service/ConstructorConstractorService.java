package com.devcam.shop24h.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcam.shop24h.entity.ConstructorContractor;
import com.devcam.shop24h.repository.ConstructionContractorRepository;

@Service
public class ConstructorConstractorService {
    @Autowired
    ConstructionContractorRepository constructionContractorRepository;

    // lấy nhà thầu theo id
    public ResponseEntity<Optional<ConstructorContractor>> getConstuctorById(int id) {

        Optional<ConstructorContractor> ConstructorData = constructionContractorRepository.findConstructionById(id);
        if (!ConstructorData.isEmpty()) {
            return new ResponseEntity<>(ConstructorData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // lấy tất cả nhà thầu
    public ResponseEntity<List<ConstructorContractor>> getConstrcutor() {

        List<ConstructorContractor> listConstructor = constructionContractorRepository.findAll();
        if (!listConstructor.isEmpty()) {
            return new ResponseEntity<>(listConstructor, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // tạo mới nhà thầu
    public ResponseEntity<Object> createConstructor(ConstructorContractor constructorContractor) {

        ConstructorContractor newRole = new ConstructorContractor();
        newRole.setName(constructorContractor.getName());
        newRole.setDescription(constructorContractor.getDescription());

        newRole.setAddress(constructorContractor.getAddress());
        newRole.setPhone(constructorContractor.getPhone());
        newRole.setPhone2(constructorContractor.getPhone2());
        newRole.setFax(constructorContractor.getFax());
        newRole.setEmail(constructorContractor.getEmail());
        newRole.setWebsite(constructorContractor.getWebsite());
        newRole.setNote(constructorContractor.getNote());

        ConstructorContractor savedRole = constructionContractorRepository.save(newRole);
        return new ResponseEntity<>(savedRole, HttpStatus.CREATED);

    }

    // cập nhật nahf thầu theo id
    public ResponseEntity<Object> updateConstructor(int id,
            ConstructorContractor constructorContractor) {

        Optional<ConstructorContractor> constructorData = constructionContractorRepository.findConstructionById(id);
        if (constructorData.isPresent()) {
            ConstructorContractor newRole = constructorData.get();
            newRole.setName(constructorContractor.getName());
            newRole.setDescription(constructorContractor.getDescription());

            newRole.setAddress(constructorContractor.getAddress());
            newRole.setPhone(constructorContractor.getPhone());
            newRole.setPhone2(constructorContractor.getPhone2());
            newRole.setFax(constructorContractor.getFax());
            newRole.setEmail(constructorContractor.getEmail());
            newRole.setWebsite(constructorContractor.getWebsite());
            newRole.setNote(constructorContractor.getNote());

            ConstructorContractor savedconstructor = constructionContractorRepository.save(newRole);
            return new ResponseEntity<>(savedconstructor, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // xóa nhà thầu theo id
    public ResponseEntity<Object> deleteConstructorById(int id) {

        Optional<ConstructorContractor> constructorData = constructionContractorRepository.findConstructionById(id);
        ConstructorContractor constructorDelete = constructorData.get();
        constructionContractorRepository.delete(constructorDelete);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
