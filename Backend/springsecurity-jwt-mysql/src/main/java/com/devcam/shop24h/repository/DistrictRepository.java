package com.devcam.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcam.shop24h.entity.District;

public interface DistrictRepository extends JpaRepository<District, Long> {
    @Query(value = "SELECT * FROM district  WHERE district.id = :id ", nativeQuery = true)
    Optional<District> findDistrictById(@Param("id") int id);

    @Query(value = "SELECT district.* FROM district INNER JOIN province ON province.id=district.province_id WHERE province.id=:id ", nativeQuery = true)
    List<District> findDistrictByProvinceId(@Param("id") int id);

}
