package com.devcam.shop24h.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcam.shop24h.entity.ConstructorContractor;
import com.devcam.shop24h.entity.DesignUnit;
import com.devcam.shop24h.entity.District;
import com.devcam.shop24h.entity.Investor;
import com.devcam.shop24h.entity.Project;
import com.devcam.shop24h.entity.Province;
import com.devcam.shop24h.entity.Street;
import com.devcam.shop24h.entity.Ward;
import com.devcam.shop24h.repository.ConstructionContractorRepository;
import com.devcam.shop24h.repository.DesignUnitRepository;
import com.devcam.shop24h.repository.DistrictRepository;
import com.devcam.shop24h.repository.InvestorRepository;
import com.devcam.shop24h.repository.ProjectRepository;
import com.devcam.shop24h.repository.ProvinceRepository;
import com.devcam.shop24h.repository.StreetRepository;
import com.devcam.shop24h.repository.WardRepository;

@Service
public class ProjectService {
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    ProvinceRepository provinceRepository;
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    WardRepository wardRepository;
    @Autowired
    StreetRepository streetRepository;
    @Autowired
    InvestorRepository investorRepository;
    @Autowired
    ConstructionContractorRepository constructionContractorRepository;
    @Autowired
    DesignUnitRepository designUnitRepository;

    // lấy ra tất cả dự án
    public ResponseEntity<List<Project>> getProject() {
        List<Project> listProject = projectRepository.findAll();
        if (!listProject.isEmpty()) {
            return new ResponseEntity<>(listProject, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    // lấy ra dự án theo id
    public ResponseEntity<Project> getProjectById(int id) {

        Optional<Project> ProjectData = projectRepository.findProjectById(id);
        if (ProjectData.isPresent()) {
            return new ResponseEntity<>(ProjectData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // lấy ra tất cả project có phân trang
    public ResponseEntity<List<Project>> getAllProject(int numberPage) {
        int Length = 6;
        int start = numberPage - 1;

        List<Project> listProject = projectRepository.findProjects(PageRequest.of(start, Length));
        if (!listProject.isEmpty()) {
            return new ResponseEntity<>(listProject, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // lấy ra project theo tỉnh huyện thị xã đường
    public ResponseEntity<List<Project>> getAllProjectByProivnceDistrictWardStreet(
            int provinceId,
            int districtId,
            int wardId,
            int streetId) {

        List<Project> listProject = projectRepository.findProjectByProvinceDistrictWardStreet(provinceId,
                districtId, wardId, streetId);
        if (!listProject.isEmpty()) {
            return new ResponseEntity<>(listProject, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // tạo project mới
    public ResponseEntity<Object> createRealestateNew(
            Project project) {

        Optional<Province> provinceDetailData = provinceRepository
                .findProvinceById(project.getIdOfProvinceFromClient());
        Optional<District> districtData = districtRepository.findDistrictById(project.getIdOfDistrictFromClient());
        Optional<Ward> wardData = wardRepository.findWardById(project.getIdOfWardFromClient());
        Optional<Street> streetData = streetRepository.findStreetById(project.getIdOfStreetFromClient());
        Optional<ConstructorContractor> constructorData = constructionContractorRepository
                .findConstructionById(project.getIdOfConstructionFromClient());
        Optional<Investor> investorData = investorRepository.findInvestorById(project.getIdOfInvestorFromClient());
        Optional<DesignUnit> designUnitData = designUnitRepository
                .findDesighUnitById(project.getIdOfDesignUnitFromClient());

        if (provinceDetailData.isPresent()
                && districtData.isPresent()
                && wardData.isPresent()
                && streetData.isPresent()
                && constructorData.isPresent()
                && investorData.isPresent()
                && designUnitData.isPresent()) {
            Project newRole = new Project();
            newRole.setName(project.getName());
            newRole.setAddress(project.getAddress());
            newRole.setSlogan(project.getSlogan());
            newRole.setDescription(project.getDescription());
            newRole.setAcreage(project.getAcreage());
            newRole.setConstructArea(project.getConstructArea());
            newRole.setNumBlock(project.getNumBlock());
            newRole.setNumFloor(project.getNumFloor());
            newRole.setNumApartment(project.getNumApartment());
            newRole.setApartmentArea(project.getApartmentArea());
            newRole.setUtilities(project.getUtilities());
            newRole.setRegionLink(project.getRegionLink());
            newRole.setPhoto(project.getPhoto());
            newRole.setLat(project.getLat());
            newRole.setLng(project.getLng());

            Province _province = provinceDetailData.get();
            newRole.setProvince(_province);
            District _district = districtData.get();
            newRole.setDistrict(_district);
            Ward _ward = wardData.get();
            newRole.setWard(_ward);
            Street _street = streetData.get();
            newRole.setStreet(_street);
            ConstructorContractor _constructorContractor = constructorData.get();
            newRole.setConstructionContractor(_constructorContractor);
            Investor _investor = investorData.get();
            newRole.setInvestor(_investor);
            DesignUnit _designUnit = designUnitData.get();
            newRole.setDesignUnit(_designUnit);

            Project saveRole = projectRepository.save(newRole);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // update project
    public ResponseEntity<Object> updateProject(int id,
            Project project) {

        Optional<Province> provinceDetailData = provinceRepository
                .findProvinceById(project.getIdOfProvinceFromClient());
        Optional<District> districtData = districtRepository.findDistrictById(project.getIdOfDistrictFromClient());
        Optional<Ward> wardData = wardRepository.findWardById(project.getIdOfWardFromClient());
        Optional<Street> streetData = streetRepository.findStreetById(project.getIdOfStreetFromClient());
        Optional<ConstructorContractor> constructorData = constructionContractorRepository
                .findConstructionById(project.getIdOfConstructionFromClient());
        Optional<Investor> investorData = investorRepository.findInvestorById(project.getIdOfInvestorFromClient());
        Optional<DesignUnit> designUnitData = designUnitRepository
                .findDesighUnitById(project.getIdOfDesignUnitFromClient());
        Optional<Project> projectData = projectRepository.findProjectById(id);
        if (projectData.isPresent()) {
            Project newRole = projectData.get();
            newRole.setName(project.getName());
            newRole.setAddress(project.getAddress());
            newRole.setSlogan(project.getSlogan());
            newRole.setDescription(project.getDescription());
            newRole.setAcreage(project.getAcreage());
            newRole.setConstructArea(project.getConstructArea());
            newRole.setNumBlock(project.getNumBlock());
            newRole.setNumFloor(project.getNumFloor());
            newRole.setNumApartment(project.getNumApartment());
            newRole.setApartmentArea(project.getApartmentArea());
            newRole.setUtilities(project.getUtilities());
            newRole.setRegionLink(project.getRegionLink());
            newRole.setPhoto(project.getPhoto());
            newRole.setLat(project.getLat());
            newRole.setLng(project.getLng());

            Province _province = provinceDetailData.get();
            newRole.setProvince(_province);
            District _district = districtData.get();
            newRole.setDistrict(_district);
            Ward _ward = wardData.get();
            newRole.setWard(_ward);
            Street _street = streetData.get();
            newRole.setStreet(_street);
            ConstructorContractor _constructorContractor = constructorData.get();
            newRole.setConstructionContractor(_constructorContractor);
            Investor _investor = investorData.get();
            newRole.setInvestor(_investor);
            DesignUnit _designUnit = designUnitData.get();
            newRole.setDesignUnit(_designUnit);

            Project saveRole = projectRepository.save(newRole);

            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // xóa project
    public ResponseEntity<Object> deleteProjectById(int id) {

        Optional<Project> projectDetailData = projectRepository.findProjectById(id);
        Project projectlDelete = projectDetailData.get();
        projectRepository.delete(projectlDelete);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

}
