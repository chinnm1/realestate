package com.devcam.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcam.shop24h.entity.Ward;

public interface WardRepository extends JpaRepository<Ward, Long> {
    @Query(value = "SELECT * FROM ward  WHERE ward.id = :id ", nativeQuery = true)
    Optional<Ward> findWardById(@Param("id") int id);

    @Query(value = "SELECT ward.* FROM ward INNER JOIN district ON district.id= ward.district_id WHERE district.id=:id ", nativeQuery = true)
    List<Ward> findWardByDistrictId(@Param("id") int id);

}
