package com.devcam.shop24h.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.ConstructorContractor;

@Repository
public interface ConstructionContractorRepository extends JpaRepository<ConstructorContractor, Long> {
    @Query(value = "SELECT * FROM construction_contractor WHERE id=:id", nativeQuery = true)
    Optional<ConstructorContractor> findConstructionById(@Param("id") int id);

}
