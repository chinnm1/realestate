package com.devcam.shop24h.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcam.shop24h.entity.District;
import com.devcam.shop24h.entity.Province;
import com.devcam.shop24h.entity.Street;
import com.devcam.shop24h.repository.DistrictRepository;
import com.devcam.shop24h.repository.ProvinceRepository;
import com.devcam.shop24h.repository.StreetRepository;

@Service
public class StreetService {
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    StreetRepository streetRepository;
    @Autowired
    ProvinceRepository provinceRepository;

    // lấy ra tất cả đường
    public ResponseEntity<List<Street>> getStreet() {

        List<Street> listStreet = streetRepository.findAll();
        if (!listStreet.isEmpty()) {
            return new ResponseEntity<>(listStreet, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // lấy ra đường theo thành phó quận huyện
    public ResponseEntity<List<Street>> getStreetByDistrictIdAndProvinceId(int districtid,
            int provinceid) {

        List<Street> listStreet = streetRepository.findStreetByProvinceiDAnDistrictId(provinceid, districtid);
        if (!listStreet.isEmpty()) {
            return new ResponseEntity<>(listStreet, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // lấy ra đường theo id
    public ResponseEntity<Optional<Street>> getStreetById(int id) {

        Optional<Street> street = streetRepository.findStreetById(id);
        if (!street.isEmpty()) {
            return new ResponseEntity<>(street, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // tạo mới đường
    public ResponseEntity<Object> createStreet(int districtid,
            int provinceid,
            Street street) {

        Optional<District> streetData = districtRepository.findDistrictById(districtid);
        Optional<Province> provinceData = provinceRepository.findProvinceById(provinceid);

        if (streetData.isPresent() && provinceData.isPresent()) {
            Street newRole = new Street();
            newRole.setName(street.getName());
            newRole.setPrefix(street.getPrefix());

            District _District = streetData.get();
            newRole.setDistrict(_District);
            Province _Province = provinceData.get();
            newRole.setProvince(_Province);

            Street savedRole = streetRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // cập nhật đường
    public ResponseEntity<Object> updateStreet(int id, Street street) {

        Optional<Street> streetData = streetRepository.findStreetById(id);
        if (streetData.isPresent()) {
            Street newRole = streetData.get();
            newRole.setName(street.getName());
            newRole.setPrefix(street.getPrefix());
            Street savedRole = streetRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // xóa đường
    public ResponseEntity<Object> deleteStreetById(int id) {

        Optional<Street> streetData = streetRepository.findStreetById(id);
        Street streetDelete = streetData.get();
        streetRepository.delete(streetDelete);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

}
