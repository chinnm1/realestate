package com.devcam.shop24h.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Project;

import com.devcam.shop24h.service.ProjectService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @GetMapping("/project")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<List<Project>> getProject() {
        try {
            return projectService.getProject();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all_project")
    public ResponseEntity<List<Project>> getAllProject() {
        try {
            return projectService.getProject();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/project/detail/{id}")
    public ResponseEntity<Project> getProjectById(@PathVariable int id) {
        try {
            return projectService.getProjectById(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/project/{numberPage}")
    public ResponseEntity<List<Project>> getAllProject(@PathVariable("numberPage") int numberPage) {

        try {
            return projectService.getAllProject(numberPage);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/project/{provinceId}/{districtId}/{wardId}/{streetId}")
    public ResponseEntity<List<Project>> getAllProjectByProivnceDistrictWardStreet(
            @PathVariable("provinceId") int provinceId,
            @PathVariable("districtId") int districtId, @PathVariable("wardId") int wardId,
            @PathVariable("streetId") int streetId) {

        try {
            return projectService.getAllProjectByProivnceDistrictWardStreet(provinceId, districtId, wardId, streetId);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/project/create")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> createRealestateNew(
            @RequestBody Project project) {

        try {

            return projectService.createRealestateNew(project);

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getMessage());

            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified project: " +
                            e.getMessage());
        }

    }

    @PutMapping("/project/update/{projectid}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> updateProject(@PathVariable("projectid") int id,
            @RequestBody Project project) {
        try {
            return projectService.updateProject(id, project);

        } catch (Exception e) {

            return ResponseEntity.badRequest().body("Failed to get specified project:" + id + " for update.");
        }

    }

    @DeleteMapping("/project/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> deleteProjectById(@PathVariable int id) {
        try {
            return projectService.deleteProjectById(id);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
