package com.devcam.shop24h.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcam.shop24h.entity.Customer;
import com.devcam.shop24h.entity.District;
import com.devcam.shop24h.entity.Project;
import com.devcam.shop24h.entity.Province;
import com.devcam.shop24h.entity.Realestate;
import com.devcam.shop24h.entity.Street;
import com.devcam.shop24h.entity.User;
import com.devcam.shop24h.entity.Ward;
import com.devcam.shop24h.repository.CustomerRepository;
import com.devcam.shop24h.repository.DistrictRepository;
import com.devcam.shop24h.repository.ProjectRepository;
import com.devcam.shop24h.repository.ProvinceRepository;
import com.devcam.shop24h.repository.RealestateRepository;
import com.devcam.shop24h.repository.StreetRepository;
import com.devcam.shop24h.repository.UserRepository;
import com.devcam.shop24h.repository.WardRepository;
import com.devcam.shop24h.repository.RealestateRepository.ProjectOnly;
import com.devcam.shop24h.repository.RealestateRepository.ProvinceDistrictWardStreetOnly;

@Service
public class RealestateService {
    @Autowired
    RealestateRepository realestateRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    ProvinceRepository provinceRepository;
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    WardRepository wardRepository;
    @Autowired
    StreetRepository streetRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CustomerRepository customerRepository;

    // Lấy ra bđs có phân trang
    public ResponseEntity<List<Realestate>> getAllRealestatePageable(int numberPage) {
        int Length = 6;
        int start = numberPage - 1;

        List<Realestate> listRealestate = realestateRepository
                .findRealestatePageableConfirm(PageRequest.of(start, Length));
        if (!listRealestate.isEmpty()) {
            return new ResponseEntity<>(listRealestate, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // Lấy ra bđs không phân trang
    public ResponseEntity<List<Realestate>> getRealestate() {

        List<Realestate> listRealestate = realestateRepository.findAllRealestate();
        if (!listRealestate.isEmpty()) {
            return new ResponseEntity<>(listRealestate, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // Lấy ra bđs không phân trang và có status là confirm
    public ResponseEntity<List<Realestate>> getRealestateConfirm() {

        List<Realestate> listRealestate = realestateRepository.findAllRealestateConfirm();
        if (!listRealestate.isEmpty()) {
            return new ResponseEntity<>(listRealestate, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // Lấy ra bđs được đăng gần đây nhất và có status là confirm
    public ResponseEntity<List<Realestate>> getRealestateOrderByDate() {

        List<Realestate> listRealestate = realestateRepository.findAllRealestateOrderByDateCreate();
        if (!listRealestate.isEmpty()) {
            return new ResponseEntity<>(listRealestate, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // Lấy ra bđs theo id
    public ResponseEntity<Optional<Realestate>> getRealestateById(int id) {
        Optional<Realestate> RealestateData = realestateRepository.findRealestateByRealeastedId(id);
        if (!RealestateData.isEmpty()) {
            return new ResponseEntity<>(RealestateData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // Lấy ra bđs đã confirm theo id
    public ResponseEntity<Optional<Realestate>> getRealestateConfirmById(int id) {
        Optional<Realestate> RealestateData = realestateRepository.findRealestateByRealeastedIdStatusConfirm(id);
        if (!RealestateData.isEmpty()) {
            return new ResponseEntity<>(RealestateData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // filter bđs theo tỉnh thành phố quận huyện phường xã và có status là confirm
    public ResponseEntity<List<Realestate>> filterRealestate(int provinceId,
            int districtId, int wardId) {
        if (provinceId != 0 && districtId != 0 && wardId != 0) {
            List<Realestate> RealestateData = realestateRepository.findRealestateByProvinceDistrictWard(provinceId,
                    districtId, wardId);
            if (!RealestateData.isEmpty()) {
                return new ResponseEntity<>(RealestateData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } else if (provinceId != 0 && districtId == 0 && wardId == 0) {
            List<Realestate> RealestateData = realestateRepository.findRealestateByProvince(provinceId);
            if (!RealestateData.isEmpty()) {
                return new ResponseEntity<>(RealestateData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } else if (provinceId != 0 && districtId != 0 && wardId == 0) {
            List<Realestate> RealestateData = realestateRepository.findRealestateByProvinceDistrict(provinceId,
                    districtId);
            if (!RealestateData.isEmpty()) {
                return new ResponseEntity<>(RealestateData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // filter bđs theo tỉnh thành phố
    public ResponseEntity<List<Realestate>> filterRealestateByProvince(int provinceId) {

        List<Realestate> RealestateData = realestateRepository.findRealestateByProvince(provinceId);

        if (!RealestateData.isEmpty()) {
            return new ResponseEntity<>(RealestateData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // filter bđs theo tỉnh thành phố quận huyện
    public ResponseEntity<List<Realestate>> filterRealestateByProvinceDistrict(int provinceId, int districtId) {

        List<Realestate> RealestateData = realestateRepository.findRealestateByProvinceDistrict(provinceId, districtId);

        if (!RealestateData.isEmpty()) {
            return new ResponseEntity<>(RealestateData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // lấy ra bđs theo id dự án
    public ResponseEntity<Object> getRealestateProjectIdById(int id) {
        ProjectOnly RealestateData = realestateRepository.findProjectByNativeQuery(id);
        if (RealestateData != null) {
            return new ResponseEntity<>(RealestateData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // lấy ra bđs theo project id
    public ResponseEntity<List<Realestate>> getAllRealestateByProjectId(int id,
            int numberPage) {
        int Length = 6;
        int start = numberPage - 1;

        List<Realestate> listRealestate = realestateRepository.findRealestateByProjectId(id,
                PageRequest.of(start, Length));
        if (!listRealestate.isEmpty()) {
            return new ResponseEntity<>(listRealestate, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // lấy idthành phố, huyện, thị xã ,đường theo bđs id
    public ResponseEntity<Object> getRealestateProvinceDistrictWardStreetById(int id) {

        ProvinceDistrictWardStreetOnly RealestateData = realestateRepository.findByNativeQuery(id);
        if (RealestateData != null) {
            return new ResponseEntity<>(RealestateData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // Update dữ liệu
    public ResponseEntity<Object> updateRealestate(int id, Realestate realestate) {
        Optional<Province> provinceDetailData = provinceRepository
                .findProvinceById(realestate.getIdOfProvinceFromClient());
        Optional<District> districtData = districtRepository
                .findDistrictById(realestate.getIdOfDistrictFromClient());
        Optional<Ward> wardData = wardRepository.findWardById(realestate.getIdOfWardFromClient());
        Optional<Street> streetData = streetRepository.findStreetById(realestate.getIdOfStreetFromClient());
        Optional<Project> projectData = projectRepository
                .findProjectByProjectId(realestate.getIdOfProjectFromClient());
        Optional<Realestate> realestateData = realestateRepository.findRealestateByRealeastedId(id);
        if (realestateData.isPresent()) {
            Realestate newRole = realestateData.get();
            newRole.setTitle(realestate.getTitle());
            newRole.setType(realestate.getType());
            newRole.setRequest(realestate.getRequest());
            newRole.setAddress(realestate.getAddress());
            newRole.setPrice(realestate.getPrice());
            newRole.setPriceMin(realestate.getPriceMin());
            newRole.setPriceTime(realestate.getPriceTime());
            newRole.setUpdateAt(new Date());
            newRole.setAcreage(realestate.getAcreage());
            newRole.setDirection(realestate.getDirection());
            newRole.setNumberFloor(realestate.getNumberFloor());
            newRole.setBath(realestate.getBath());
            newRole.setApartCode(realestate.getApartCode());
            newRole.setWallArea(realestate.getWallArea());
            newRole.setBedroom(realestate.getBedroom());
            newRole.setBalcony(realestate.getBalcony());
            newRole.setLandscapeView(realestate.getLandscapeView());
            newRole.setApartLoca(realestate.getApartLoca());
            newRole.setApartType(realestate.getApartType());
            newRole.setFurnitureType(realestate.getFurnitureType());
            newRole.setPriceRent(realestate.getPriceRent());
            newRole.setReturnRate(realestate.getReturnRate());
            newRole.setLegalDoc(realestate.getLegalDoc());
            newRole.setDescription(realestate.getDescription());
            newRole.setWidth(realestate.getWidth());
            newRole.setLong(realestate.getLong());

            newRole.setViewNum(realestate.getViewNum());
            newRole.setShape(realestate.getShape());
            newRole.setDistance2facade(realestate.getDistance2facade());
            newRole.setAdjacentFacadeNum(realestate.getAdjacentFacadeNum());
            newRole.setAdjacentRoad(realestate.getAdjacentRoad());
            newRole.setAlleyMinWidth(realestate.getAlleyMinWidth());
            newRole.setAdjacentAlleyMinWidth(realestate.getAdjacentAlleyMinWidth());
            newRole.setFactor(realestate.getFactor());
            newRole.setStructure(realestate.getStructure());
            newRole.setDtsxd(realestate.getDtsxd());
            newRole.setClcl(realestate.getClcl());
            newRole.setCtxdPrice(realestate.getCtxdPrice());
            newRole.setCtxdValue(realestate.getCtxdValue());
            newRole.setAcreage(realestate.getAcreage());
            newRole.setPhoto(realestate.getPhoto());
            newRole.setLat(realestate.getLat());
            newRole.setLng(realestate.getLng());

            Province _province = provinceDetailData.get();
            newRole.setProvince(_province);
            District _district = districtData.get();
            newRole.setDistrict(_district);
            Ward _ward = wardData.get();
            newRole.setWard(_ward);
            Street _street = streetData.get();
            newRole.setStreet(_street);
            if (projectData.isPresent()) {
                Project _project = projectData.get();
                newRole.setProject(_project);
            } else {
                newRole.setProject(null);
            }
            Realestate saveRole = realestateRepository.save(newRole);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // Tạo bđs
    public ResponseEntity<Object> createRealestateNew(
            Realestate realestate, int userId) {
        Optional<User> userCreateReal = userRepository.findById(Long.valueOf(userId));
        Optional<Customer> customerCreate = customerRepository
                .findCustomerByUsername(userCreateReal.get().getUsername());
        Optional<Province> provinceDetailData = provinceRepository
                .findProvinceById(realestate.getIdOfProvinceFromClient());
        Optional<District> districtData = districtRepository
                .findDistrictById(realestate.getIdOfDistrictFromClient());
        Optional<Ward> wardData = wardRepository.findWardById(realestate.getIdOfWardFromClient());
        Optional<Street> streetData = streetRepository.findStreetById(realestate.getIdOfStreetFromClient());
        Optional<Project> projectData = projectRepository
                .findProjectByProjectId(realestate.getIdOfProjectFromClient());
        if (provinceDetailData.isPresent()
                && districtData.isPresent()
                && wardData.isPresent()
                && streetData.isPresent()) {
            Realestate newRole = new Realestate();
            newRole.setTitle(realestate.getTitle());
            newRole.setType(realestate.getType());
            newRole.setRequest(realestate.getRequest());
            newRole.setAddress(realestate.getAddress());
            newRole.setPrice(realestate.getPrice());
            newRole.setPriceMin(realestate.getPriceMin());
            newRole.setPriceTime(realestate.getPriceTime());
            newRole.setCreatedAt(new Date());
            newRole.setAcreage(realestate.getAcreage());
            newRole.setDirection(realestate.getDirection());
            newRole.setNumberFloor(realestate.getNumberFloor());
            newRole.setBath(realestate.getBath());
            newRole.setApartCode(realestate.getApartCode());
            newRole.setWallArea(realestate.getWallArea());
            newRole.setBedroom(realestate.getBedroom());
            newRole.setBalcony(realestate.getBalcony());
            newRole.setLandscapeView(realestate.getLandscapeView());
            newRole.setApartLoca(realestate.getApartLoca());
            newRole.setApartType(realestate.getApartType());
            newRole.setFurnitureType(realestate.getFurnitureType());
            newRole.setPriceRent(realestate.getPriceRent());
            newRole.setReturnRate(realestate.getReturnRate());
            newRole.setLegalDoc(realestate.getLegalDoc());
            newRole.setDescription(realestate.getDescription());
            newRole.setWidth(realestate.getWidth());
            newRole.setLong(realestate.getLong());
            newRole.setViewNum(realestate.getViewNum());
            newRole.setShape(realestate.getShape());
            newRole.setDistance2facade(realestate.getDistance2facade());
            newRole.setAdjacentFacadeNum(realestate.getAdjacentFacadeNum());
            newRole.setAdjacentRoad(realestate.getAdjacentRoad());
            newRole.setAlleyMinWidth(realestate.getAlleyMinWidth());
            newRole.setAdjacentAlleyMinWidth(realestate.getAdjacentAlleyMinWidth());
            newRole.setFactor(realestate.getFactor());
            newRole.setStructure(realestate.getStructure());
            newRole.setDtsxd(realestate.getDtsxd());
            newRole.setClcl(realestate.getClcl());
            newRole.setCtxdPrice(realestate.getCtxdPrice());
            newRole.setCtxdValue(realestate.getCtxdValue());
            newRole.setAcreage(realestate.getAcreage());
            newRole.setPhoto(realestate.getPhoto());
            newRole.setLat(realestate.getLat());
            newRole.setLng(realestate.getLng());
            newRole.setStatus("Pending");
            newRole.setCreatedBy(customerCreate.get().getContactName());
            Province _province = provinceDetailData.get();
            newRole.setProvince(_province);
            District _district = districtData.get();
            newRole.setDistrict(_district);
            Ward _ward = wardData.get();
            newRole.setWard(_ward);
            Street _street = streetData.get();
            newRole.setStreet(_street);
            if (projectData.isPresent()) {
                Project _project = projectData.get();
                newRole.setProject(_project);
            } else {
                newRole.setProject(null);
            }
            Realestate savedRole = realestateRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // thay đổi status của bđs
    public ResponseEntity<Object> changeRealestateStatus(int id,
            Realestate realestate) {
        Optional<Realestate> realestateData = realestateRepository.findRealestateByRealeastedId(id);
        if (realestateData.isPresent()) {
            Realestate newRole = realestateData.get();
            newRole.setStatus(realestate.getStatus());
            Realestate saveRole = realestateRepository.save(newRole);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // Xóa bđs
    public ResponseEntity<Object> deleteOrderDeatilById(int id) {

        Optional<Realestate> realestateDetailData = realestateRepository.findRealestateByRealeastedId(id);
        Realestate orderdetailDelete = realestateDetailData.get();
        realestateRepository.delete(orderdetailDelete);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

}
