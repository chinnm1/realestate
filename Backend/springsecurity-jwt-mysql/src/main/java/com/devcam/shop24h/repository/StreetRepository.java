package com.devcam.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.Street;

@Repository
public interface StreetRepository extends JpaRepository<Street, Long> {
    @Query(value = "SELECT * FROM street  WHERE street.id = :id ", nativeQuery = true)
    Optional<Street> findStreetById(@Param("id") int id);

    @Query(value = "SELECT street.* FROM street INNER JOIN province ON province.id =street._province_id INNER JOIN district ON district.id=street._district_id WHERE province.id=:provinceid AND district.id=:districtid ", nativeQuery = true)
    List<Street> findStreetByProvinceiDAnDistrictId(@Param("provinceid") int provinceid,
            @Param("districtid") int districtid);

}
