package com.devcam.shop24h.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "realestates")
public class Realestate {

    @Transient
    private int idOfProvinceFromClient;
    @Transient
    private int idOfDistrictFromClient;
    @Transient
    private int idOfWardFromClient;
    @Transient
    private int idOfStreetFromClient;
    @Transient
    private int idOfProjectFromClient;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "type")
    private int type;
    @Column(name = "request")
    private int request;
    @Column(name = "address")
    private String address;
    @Column(name = "price")
    private int price;
    @Column(name = "price_min")
    private int priceMin;
    @Column(name = "price_time")
    private int priceTime;

    @Column(name = "date_create")
    @CreatedDate
    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date createdAt;

    @Column(name = "date_update")
    @CreatedDate
    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date updateAt;

    @Column(name = "acreage")
    private BigDecimal acreage;
    @Column(name = "direction")
    private String direction;

    @Column(name = "number_floors")
    private int numberFloor;
    @Column(name = "bath")
    private int bath;
    @Column(name = "apart_code")
    private String apartCode;
    @Column(name = "wall_area")
    private BigDecimal wallArea;
    @Column(name = "bedroom")
    private int bedroom;
    @Column(name = "balcony")
    private int balcony;
    @Column(name = "landscape_view")
    private String landscapeView;
    @Column(name = "apart_loca")
    private int apartLoca;
    @Column(name = "apart_type")
    private int apartType;
    @Column(name = "furniture_type")
    private int furnitureType;
    @Column(name = "price_rent")
    private int priceRent;
    @Column(name = "return_rate")
    private double returnRate;
    @Column(name = "legal_doc")
    private int legalDoc;
    @Column(name = "description")
    private String description;
    @Column(name = "width_y")
    private int Width;
    @Column(name = "long_x", nullable = true)
    private int Long;

    @Column(name = "view_num")
    private int viewNum;

    @Column(name = "create_by")
    @CreatedBy
    private String createdBy;
    @Column(name = "update_by")
    @LastModifiedBy
    private int updatedBy;
    @Column(name = "shape")
    private String shape;
    @Column(name = "distance2facade")
    private int distance2facade;
    @Column(name = "adjacent_facade_num")
    private int adjacentFacadeNum;
    @Column(name = "adjacent_road")
    private String adjacentRoad;
    @Column(name = "alley_min_width")
    private int alleyMinWidth;
    @Column(name = "adjacent_alley_min_width")
    private int adjacentAlleyMinWidth;
    @Column(name = "factor")
    private String factor;
    @Column(name = "structure")
    private String structure;
    @Column(name = "DTSXD")
    private int dtsxd;
    @Column(name = "CLCL")
    private int clcl;
    @Column(name = "CTXD_price")
    private int ctxdPrice;
    @Column(name = "CTXD_value")
    private int ctxdValue;
    @Column(name = "photo")
    private String photo;
    @Column(name = "_lat")
    private double lat;
    @Column(name = "_lng")
    private double lng;
    @Column(name = "status")
    private String status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "district_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private District district;

    @Transient
    @JsonProperty
    private int idOfDistrict;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "province_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Province province;

    @Transient
    @JsonProperty
    private int idOfProvince;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = true)
    @JsonIgnore
    private Project project;

    @Transient
    @JsonProperty
    private int idOfProject;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wards_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Ward ward;

    @Transient
    @JsonProperty
    private int idOfWard;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "street_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Street street;

    @Transient
    @JsonProperty
    private int idOfStreet;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "realestates")
    private List<Customer> customers;

    public Realestate() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getRequest() {
        return request;
    }

    public void setRequest(int request) {
        this.request = request;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(int priceMin) {
        this.priceMin = priceMin;
    }

    public int getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(int priceTime) {
        this.priceTime = priceTime;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public int getNumberFloor() {
        return numberFloor;
    }

    public void setNumberFloor(int numberFloor) {
        this.numberFloor = numberFloor;
    }

    public int getBath() {
        return bath;
    }

    public void setBath(int bath) {
        this.bath = bath;
    }

    public String getApartCode() {
        return apartCode;
    }

    public void setApartCode(String apartCode) {
        this.apartCode = apartCode;
    }

    public BigDecimal getWallArea() {
        return wallArea;
    }

    public void setWallArea(BigDecimal wallArea) {
        this.wallArea = wallArea;
    }

    public int getBedroom() {
        return bedroom;
    }

    public void setBedroom(int bedroom) {
        this.bedroom = bedroom;
    }

    public int getBalcony() {
        return balcony;
    }

    public void setBalcony(int balcony) {
        this.balcony = balcony;
    }

    public String getLandscapeView() {
        return landscapeView;
    }

    public void setLandscapeView(String landscapeView) {
        this.landscapeView = landscapeView;
    }

    public int getApartLoca() {
        return apartLoca;
    }

    public void setApartLoca(int apartLoca) {
        this.apartLoca = apartLoca;
    }

    public int getApartType() {
        return apartType;
    }

    public void setApartType(int apartType) {
        this.apartType = apartType;
    }

    public int getFurnitureType() {
        return furnitureType;
    }

    public void setFurnitureType(int furnitureType) {
        this.furnitureType = furnitureType;
    }

    public int getPriceRent() {
        return priceRent;
    }

    public void setPriceRent(int priceRent) {
        this.priceRent = priceRent;
    }

    public double getReturnRate() {
        return returnRate;
    }

    public void setReturnRate(double returnRate) {
        this.returnRate = returnRate;
    }

    public int getLegalDoc() {
        return legalDoc;
    }

    public void setLegalDoc(int legalDoc) {
        this.legalDoc = legalDoc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWidth() {
        return Width;
    }

    public void setWidth(int width) {
        Width = width;
    }

    public int getViewNum() {
        return viewNum;
    }

    public void setViewNum(int viewNum) {
        this.viewNum = viewNum;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public int getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public int getDistance2facade() {
        return distance2facade;
    }

    public void setDistance2facade(int distance2facade) {
        this.distance2facade = distance2facade;
    }

    public int getAdjacentFacadeNum() {
        return adjacentFacadeNum;
    }

    public void setAdjacentFacadeNum(int adjacentFacadeNum) {
        this.adjacentFacadeNum = adjacentFacadeNum;
    }

    public String getAdjacentRoad() {
        return adjacentRoad;
    }

    public void setAdjacentRoad(String adjacentRoad) {
        this.adjacentRoad = adjacentRoad;
    }

    public int getAlleyMinWidth() {
        return alleyMinWidth;
    }

    public void setAlleyMinWidth(int alleyMinWidth) {
        this.alleyMinWidth = alleyMinWidth;
    }

    public int getAdjacentAlleyMinWidth() {
        return adjacentAlleyMinWidth;
    }

    public void setAdjacentAlleyMinWidth(int adjacentAlleyMinWidth) {
        this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
    }

    public String getFactor() {
        return factor;
    }

    public void setFactor(String factor) {
        this.factor = factor;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public int getDtsxd() {
        return dtsxd;
    }

    public void setDtsxd(int dtsxd) {
        this.dtsxd = dtsxd;
    }

    public int getClcl() {
        return clcl;
    }

    public void setClcl(int clcl) {
        this.clcl = clcl;
    }

    public int getCtxdPrice() {
        return ctxdPrice;
    }

    public void setCtxdPrice(int ctxdPrice) {
        this.ctxdPrice = ctxdPrice;
    }

    public int getCtxdValue() {
        return ctxdValue;
    }

    public void setCtxdValue(int ctxdValue) {
        this.ctxdValue = ctxdValue;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Ward getWard() {
        return ward;
    }

    public void setWard(Ward ward) {
        this.ward = ward;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public int getIdOfProject() {

        if (getProject() != null) {
            this.idOfDistrict = getProject().getId();
        }

        return idOfDistrict;
    }

    public void setIdOfProject(int idOfProject) {
        this.idOfProject = idOfProject;
    }

    public int getIdOfDistrict() {
        return getDistrict().getId();
    }

    public void setIdOfDistrict(int idOfDistrict) {
        this.idOfDistrict = idOfDistrict;
    }

    public int getIdOfProvince() {
        return getProvince().getId();
    }

    public void setIdOfProvince(int idOfProvince) {
        this.idOfProvince = idOfProvince;
    }

    public int getIdOfWard() {
        return getWard().getId();
    }

    public void setIdOfWard(int idOfWard) {
        this.idOfWard = idOfWard;
    }

    public int getIdOfStreet() {
        return getStreet().getId();
    }

    public void setIdOfStreet(int idOfStreet) {
        this.idOfStreet = idOfStreet;
    }

    public int getIdOfProvinceFromClient() {
        return idOfProvinceFromClient;
    }

    public void setIdOfProvinceFromClient(int idOfProvinceFromClient) {
        this.idOfProvinceFromClient = idOfProvinceFromClient;
    }

    public int getIdOfDistrictFromClient() {
        return idOfDistrictFromClient;
    }

    public void setIdOfDistrictFromClient(int idOfDistrictFromClient) {
        this.idOfDistrictFromClient = idOfDistrictFromClient;
    }

    public int getIdOfWardFromClient() {
        return idOfWardFromClient;
    }

    public void setIdOfWardFromClient(int idOfWardFromClient) {
        this.idOfWardFromClient = idOfWardFromClient;
    }

    public int getIdOfStreetFromClient() {
        return idOfStreetFromClient;
    }

    public void setIdOfStreetFromClient(int idOfStreetFromClient) {
        this.idOfStreetFromClient = idOfStreetFromClient;
    }

    public int getIdOfProjectFromClient() {
        return idOfProjectFromClient;
    }

    public void setIdOfProjectFromClient(int idOfProjectFromClient) {
        this.idOfProjectFromClient = idOfProjectFromClient;
    }

    public int getLong() {
        return Long;
    }

    public void setLong(int l) {
        Long = l;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
