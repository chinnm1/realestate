package com.devcam.shop24h.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcam.shop24h.entity.DesignUnit;
import com.devcam.shop24h.repository.DesignUnitRepository;

@Service
public class DesignUnitService {
    @Autowired
    DesignUnitRepository designUnitRepository;

    // lấy tất cả đơn vị thiết kế
    public ResponseEntity<List<DesignUnit>> getDesignunit() {

        List<DesignUnit> listConstructor = designUnitRepository.findAll();
        if (!listConstructor.isEmpty()) {
            return new ResponseEntity<>(listConstructor, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // lấy đơn vị thiết kế theo id
    public ResponseEntity<Optional<DesignUnit>> getDesignunitById(int id) {

        Optional<DesignUnit> designuntir = designUnitRepository.findById(null);
        if (!designuntir.isEmpty()) {
            return new ResponseEntity<>(designuntir, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // tạo mới đơn vị thiết kế
    public ResponseEntity<Object> createDesignunit(DesignUnit designUnit) {

        DesignUnit newRole = new DesignUnit();
        newRole.setName(designUnit.getName());
        newRole.setDescription(designUnit.getDescription());
        newRole.setAddress(designUnit.getAddress());
        newRole.setPhone(designUnit.getPhone());
        newRole.setPhone2(designUnit.getPhone2());
        newRole.setFax(designUnit.getFax());
        newRole.setEmail(designUnit.getEmail());
        newRole.setWebsite(designUnit.getWebsite());
        newRole.setNote(designUnit.getNote());

        DesignUnit savedRole = designUnitRepository.save(newRole);
        return new ResponseEntity<>(savedRole, HttpStatus.CREATED);

    }

    // cập nhật đơn vị thiết kế
    public ResponseEntity<Object> updateDesignUnit(int id, DesignUnit designuntir) {

        Optional<DesignUnit> designunitData = designUnitRepository.findDesighUnitById(id);
        if (designunitData.isPresent()) {
            DesignUnit newRole = designunitData.get();
            newRole.setName(designuntir.getName());
            newRole.setDescription(designuntir.getDescription());
            newRole.setAddress(designuntir.getAddress());
            newRole.setPhone(designuntir.getPhone());
            newRole.setPhone2(designuntir.getPhone2());
            newRole.setFax(designuntir.getFax());
            newRole.setEmail(designuntir.getEmail());
            newRole.setWebsite(designuntir.getWebsite());
            newRole.setNote(designuntir.getNote());
            DesignUnit savedcustomer = designUnitRepository.save(newRole);

            return new ResponseEntity<>(savedcustomer, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // xóa đơn vị thiết kế theo id
    public ResponseEntity<Object> deletedesignuntirById(int id) {

        Optional<DesignUnit> designunitData = designUnitRepository.findDesighUnitById(id);

        DesignUnit designunitDelete = designunitData.get();
        designUnitRepository.delete(designunitDelete);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
