package com.devcam.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query(value = "SELECT * FROM customers WHERE id=:id", nativeQuery = true)
    Optional<Customer> findCustomerById(@Param("id") int id);

    @Query(value = "SELECT * FROM customers WHERE username=:username", nativeQuery = true)
    Optional<Customer> findCustomerByUsername(@Param("username") String username);

    @Query(value = "SELECT * FROM customers WHERE contact_name=:contactname", nativeQuery = true)
    Optional<Customer> findCustomerByContactname(@Param("contactname") String contactname);

    @Query(value = "SELECT customers.* FROM customers INNER JOIN realestates ON realestates.id = customers.realestate_id WHERE realestates.id =:id", nativeQuery = true)
    List<Customer> findCustomerByRealesteId(@Param("id") int id);

}
