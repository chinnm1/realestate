package com.devcam.shop24h.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Employee;
import com.devcam.shop24h.repository.EmployeeRepository;
import com.devcam.shop24h.service.EmployeeService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class EmployeeCOntroller {
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    EmployeeService employeeService;

    @GetMapping("/employee")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<List<Employee>> getEmployee() {
        try {
            return employeeService.getEmployee();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all_employee")
    public ResponseEntity<List<Employee>> getAllEmployee() {
        try {
            return employeeService.getEmployee();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/employee/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Optional<Employee>> getEmployeeById(@PathVariable("id") int id) {
        try {
            return employeeService.getEmployeeById(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/employee/create")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> createEmployee(@RequestBody Employee employee) {
        try {
            return employeeService.createEmployee(employee);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Employee: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/employee/update/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> updateEmployee(@PathVariable("id") int id, @RequestBody Employee employee) {
        try {
            return employeeService.updateEmployee(id, employee);

        } catch (Exception e) {

            return ResponseEntity.badRequest().body("Failed to get specified employee: " + id + "  for update.");
        }

    }

    @PutMapping("/employee/update_level/{employeeId}/{roleId}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> updateEmployeeLevel(@PathVariable("employeeId") int employeeId,
            @PathVariable("roleId") int roleId) {
        try {
            return employeeService.updateEmployeeLevel(employeeId, roleId);

        } catch (Exception e) {

            return ResponseEntity.badRequest()
                    .body("Failed to get specified employee: " + employeeId + "  for update.");
        }

    }

    @DeleteMapping("/employee/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> deleteEmployeeById(@PathVariable int id) {
        try {
            return employeeService.deleteEmployeeById(id);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
