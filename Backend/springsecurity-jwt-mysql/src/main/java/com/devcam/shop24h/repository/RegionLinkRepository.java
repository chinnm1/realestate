package com.devcam.shop24h.repository;

import java.util.List;

import org.hibernate.metamodel.model.convert.spi.JpaAttributeConverter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.RegionLink;

@Repository
public interface RegionLinkRepository extends JpaRepository<RegionLink, Long> {
    @Query(value = "SELECT region_link.* from projects_regionlinks INNER JOIN project ON project.id = projects_regionlinks.project_id INNER JOIN region_link ON region_link.id = projects_regionlinks.regionlink_id WHERE project.id =:id", nativeQuery = true)
    List<RegionLink> findRegionlinkByProjectId(@Param("id") int id);

}
