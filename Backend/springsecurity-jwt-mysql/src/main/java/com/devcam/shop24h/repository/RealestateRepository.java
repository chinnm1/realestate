package com.devcam.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.Realestate;

@Repository
public interface RealestateRepository extends JpaRepository<Realestate, Long> {
    @Query(value = "SELECT * FROM realestates WHERE realestates.status='confirm' ", nativeQuery = true)
    List<Realestate> findRealestatePageableConfirm(Pageable pageable);

    @Query(value = "SELECT * FROM realestates", nativeQuery = true)
    List<Realestate> findAllRealestate();

    @Query(value = "SELECT * FROM realestates WHERE realestates.status='confirm'", nativeQuery = true)
    List<Realestate> findAllRealestateConfirm();

    @Query(value = "SELECT * FROM realestates WHERE realestates.status='confirm' ORDER BY date_create DESC", nativeQuery = true)
    List<Realestate> findAllRealestateOrderByDateCreate();

    @Query(value = "SELECT realestates.* FROM realestates INNER JOIN project ON realestates.project_id = project.id WHERE project.id =:id", nativeQuery = true)
    List<Realestate> findRealestateByProjectId(@Param("id") int id, Pageable pageable);

    @Query(value = "SELECT * FROM realestates WHERE realestates.id = :id  ", nativeQuery = true)
    Optional<Realestate> findRealestateByRealeastedId(@Param("id") int id);

    @Query(value = "SELECT * FROM realestates WHERE realestates.id = :id AND realestates.status='confirm'  ", nativeQuery = true)
    Optional<Realestate> findRealestateByRealeastedIdStatusConfirm(@Param("id") int id);

    @Query(value = "SELECT realestates.* FROM realestates INNER JOIN province ON province.id=realestates.province_id INNER JOIN district ON district.id=realestates.district_id INNER JOIN ward ON ward.id =realestates.wards_id INNER JOIN street ON street.id=realestates.street_id INNER JOIN project ON project.id=realestates.project_id WHERE province.id =:provinceId AND district.id=:districtId AND ward.id=:wardId AND street.id=:streetId AND project.id=:projectId", nativeQuery = true)
    Optional<Realestate> createRealestate(@Param("provinceId") int provinceId, @Param("districtId") int districtId,
            @Param("wardId") int wardId, @Param("streetId") int streetId, @Param("projectId") int projectId);

    @Query(value = "SELECT realestates.* FROM realestates INNER JOIN province ON realestates.province_id=province.id INNER JOIN district ON realestates.district_id=district.id INNER JOIN ward ON realestates.wards_id = ward.id WHERE province.id =:provinceId AND district.id=:districtId AND ward.id=:wardId AND realestates.status='confirm' ", nativeQuery = true)
    List<Realestate> findRealestateByProvinceDistrictWard(@Param("provinceId") int provinceId,
            @Param("districtId") int districtId, @Param("wardId") int wardId);

    @Query(value = "SELECT realestates.* FROM `realestates` INNER JOIN province ON province.id = realestates.province_id WHERE province.id=:provinceId AND realestates.status='confirm'", nativeQuery = true)
    List<Realestate> findRealestateByProvince(@Param("provinceId") int provinceId);

    @Query(value = "SELECT realestates.* FROM `realestates` INNER JOIN province ON province.id = realestates.province_id INNER JOIN district ON district.id = realestates.district_id WHERE province.id=:provinceId AND district.id=:districtId AND realestates.status='confirm'", nativeQuery = true)
    List<Realestate> findRealestateByProvinceDistrict(@Param("provinceId") int provinceId,
            @Param("districtId") int districtId);

    @Query(value = "SELECT  province_id as provinceid ,district_id as districtid, wards_id as wardid, street_id as streetid  FROM realestates WHERE id = :id", nativeQuery = true)
    ProvinceDistrictWardStreetOnly findByNativeQuery(@Param("id") int id);

    public static interface ProvinceDistrictWardStreetOnly {

        int getProvinceid();

        int getDistrictid();

        int getWardid();

        int getStreetid();
    }

    @Query(value = "SELECT  project_id as projectid  FROM realestates WHERE id = :id", nativeQuery = true)
    ProjectOnly findProjectByNativeQuery(@Param("id") int id);

    public static interface ProjectOnly {

        int getProjectid();

    }

}
