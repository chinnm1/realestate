package com.devcam.shop24h.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.DesignUnit;

@Repository
public interface DesignUnitRepository extends JpaRepository<DesignUnit, Long> {
    @Query(value = "SELECT * FROM design_unit WHERE id=:id", nativeQuery = true)
    Optional<DesignUnit> findDesighUnitById(@Param("id") int id);

}
