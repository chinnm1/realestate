package com.devcam.shop24h.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.ConstructorContractor;
import com.devcam.shop24h.repository.ConstructionContractorRepository;
import com.devcam.shop24h.service.ConstructorConstractorService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class ConstructionContractorCOntroller {
    @Autowired
    ConstructionContractorRepository constructionContractorRepository;
    @Autowired
    ConstructorConstractorService constractorService;

    @GetMapping("/constructor/detail/{id}")

    public ResponseEntity<Optional<ConstructorContractor>> getConstuctorById(@PathVariable int id) {
        try {
            return constractorService.getConstuctorById(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/constructor")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")

    public ResponseEntity<List<ConstructorContractor>> getConstrcutor() {
        try {
            return constractorService.getConstrcutor();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all_constructor")
    public ResponseEntity<List<ConstructorContractor>> getAllConstrcutor() {
        try {
            return constractorService.getConstrcutor();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/constructor/create")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> createConstructor(@RequestBody ConstructorContractor constructorContractor) {
        try {
            return constractorService.createConstructor(constructorContractor);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified constructor: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/constructor/update/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> updateConstructor(@PathVariable("id") int id,
            @RequestBody ConstructorContractor constructorContractor) {
        try {
            return constractorService.updateConstructor(id, constructorContractor);
        } catch (Exception e) {

            return ResponseEntity.badRequest().body("Failed to get specified constructor: " + id + "  for update.");
        }

    }

    @DeleteMapping("/constructor/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> deleteConstructorById(@PathVariable int id) {
        try {
            return constractorService.deleteConstructorById(id);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
