package com.devcam.shop24h.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcam.shop24h.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Set<Role> findByRoleKey(String roleKey);

    @Query(value = "SELECT * FROM t_role WHERE t_role.id = :id", nativeQuery = true)
    Set<Role> findRoleByRoleId(@Param("id") Long id);
}
