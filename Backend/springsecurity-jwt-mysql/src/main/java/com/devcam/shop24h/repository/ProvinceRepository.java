package com.devcam.shop24h.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.Province;

@Repository
public interface ProvinceRepository extends JpaRepository<Province, Long> {
    @Query(value = "SELECT * FROM province  WHERE province.id = :id ", nativeQuery = true)
    Optional<Province> findProvinceById(@Param("id") int id);

}
