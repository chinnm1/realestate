package com.devcam.shop24h.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Realestate;
import com.devcam.shop24h.entity.Role;
import com.devcam.shop24h.entity.Token;
import com.devcam.shop24h.entity.User;
import com.devcam.shop24h.repository.RealestateRepository;
import com.devcam.shop24h.repository.RoleRepository;
import com.devcam.shop24h.repository.UserRepository;
import com.devcam.shop24h.security.JwtUtil;
import com.devcam.shop24h.security.UserPrincipal;
import com.devcam.shop24h.service.TokenService;
import com.devcam.shop24h.service.UserService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class AuthController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private RoleRepository roleRepository;

    @PostMapping("/register")
    public User register(@RequestBody User user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        Set<Role> roleFound = roleRepository.findByRoleKey("ROLE_CUSTOMER");
        user.setRoles(roleFound);
        return userService.createUser(user);
    }

    @PostMapping("/employee/register")
    public User registerEmployee(@RequestBody User user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        // Set<Role> roleFound = roleRepository.findByRoleKey("ROLE_CUSTOMER");
        // user.setRoles(roleFound);
        return userService.createUser(user);
    }

    @PreAuthorize("hasRole('MANAGER')")
    @PutMapping("/employee/update/{userName}/{userlevel}")
    public User updateUserRoleBylevel(@PathVariable("userName") String userName,
            @PathVariable("userlevel") int userlevel) {
        User userupdate = userRepository.findByUsername(userName);
        Set<Role> roleFound = roleRepository.findRoleByRoleId(Long.valueOf(userlevel));
        userupdate.setRoles(roleFound);
        return userRepository.save(userupdate);
    }

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody User user) {
        User userInput = userRepository.findByUsername(user.getUsername());
        UserPrincipal userPrincipal = userService.findByUsername(user.getUsername());
        if (null == user || !new BCryptPasswordEncoder().matches(user.getPassword(), userPrincipal.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("tài khoản hoặc mật khẩu không chính xác");
        }
        Token token = new Token();
        token.setToken(jwtUtil.generateToken(userPrincipal));
        token.setTokenExpDate(jwtUtil.generateExpirationDate());
        token.setCreatedBy(userPrincipal.getUserId());
        tokenService.createToken(token);
        User userLogin = new User();
        userLogin.setId(userPrincipal.getUserId());
        userLogin.setRoles(userInput.getRoles());
        userLogin.setUsername(userInput.getUsername());

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("token", token.getToken());
        map.put("userId", userLogin.getId().toString());
        map.put("role", userLogin.getRoles());
        map.put("usename", userLogin.getUsername());

        return ResponseEntity.ok(map);
    }

    @GetMapping("/hello")
    @PreAuthorize("hasAnyAuthority('USER_CREATE', 'USER_UPDATE')")
    /*
     * @PreAuthorize("hasRole('USER_READ') " + "|| hasRole('USER_CREATE')" +
     * "|| hasRole('USER_UPDATE')" + "|| (hasRole('USER_DELETE')")
     */

    public ResponseEntity hello() {
        return ResponseEntity.ok("hello  have USER_READ OR USER_CREATE OR USER_UPDATE oR USER_DELETE");
    }

    @GetMapping("/hello2")
    @PreAuthorize("hasAnyAuthority('USER_READ','USER_DELETE')")
    // @PreAuthorize("hasRole('USER_READ') " +
    // "|| (hasRole('USER_DELETE')")

    public ResponseEntity hello2() {
        return ResponseEntity.ok("hello 2 have USER_READ OR USER_DELETE");
    }

    @GetMapping("/hello3")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity hello3() {
        return ResponseEntity.ok("hello cho ADMIN va CUSTOMER");
    }

    @GetMapping("/hello4")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity hello4() {
        return ResponseEntity.ok("hello chi cho ADMIN");
    }

    @GetMapping("/hello6")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity hello6() {
        return ResponseEntity.ok("hello chi cho mANAGER");
    }

    @GetMapping("/hello7")
    @PreAuthorize("hasAnyAuthority('USER_DELETE')")

    public ResponseEntity hello7() {
        return ResponseEntity.ok("hello 2 have USER_DELETE");
    }

    @GetMapping("/hello5")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity hello5() {
        return ResponseEntity.ok("hello chi cho ADMIN");
    }

}
