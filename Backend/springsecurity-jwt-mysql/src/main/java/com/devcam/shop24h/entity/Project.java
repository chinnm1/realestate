package com.devcam.shop24h.entity;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "project")
public class Project {

    @Transient
    private int idOfProvinceFromClient;
    @Transient
    private int idOfDistrictFromClient;
    @Transient
    private int idOfWardFromClient;
    @Transient
    private int idOfStreetFromClient;
    @Transient
    private int idOfProjectFromClient;
    @Transient
    private int idOfDesignUnitFromClient;
    @Transient
    private int idOfConstructionFromClient;
    @Transient
    private int idOfInvestorFromClient;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "_name")
    private String name;
    @Column(name = "address")
    private String address;
    @Column(name = "slogan")
    private String slogan;
    @Column(name = "description")
    private String description;
    @Column(name = "acreage")
    private BigDecimal acreage;
    @Column(name = "construct_area")
    private BigDecimal constructArea;
    @Column(name = "num_block")
    private int numBlock;
    @Column(name = "num_floors")
    private String numFloor;
    @Column(name = "num_apartment")
    private int numApartment;
    @Column(name = "apartmentt_area")
    private String apartmentArea;
    @Column(name = "utilities")
    private String utilities;
    @Column(name = "region_link")
    private String regionLink;
    @Column(name = "photo")
    private String photo;
    @Column(name = "_lat")
    private double lat;
    @Column(name = "_lng")
    private double lng;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "_province_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Province province;

    @Transient
    @JsonProperty
    private int idOfProvince;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "_district_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private District district;

    @Transient
    @JsonProperty
    private int idOfDistrict;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "_ward_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Ward ward;

    @Transient
    @JsonProperty
    private int idOfWard;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "_street_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Street street;

    @Transient
    @JsonProperty
    private int idOfStreet;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "investor", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Investor investor;

    @Transient
    @JsonProperty
    private int idOfInvestor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "construction_contractor", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private ConstructorContractor constructionContractor;

    @Transient
    @JsonProperty
    private int idOfConstruction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "design_unit", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private DesignUnit designUnit;

    @Transient
    @JsonProperty
    private int idOfDesignUnit;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "project")
    private List<MasterLayout> masterLayout;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "project")

    private List<Realestate> realestates;

    public Project() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public BigDecimal getConstructArea() {
        return constructArea;
    }

    public void setConstructArea(BigDecimal constructArea) {
        this.constructArea = constructArea;
    }

    public int getNumBlock() {
        return numBlock;
    }

    public void setNumBlock(int numBlock) {
        this.numBlock = numBlock;
    }

    public String getNumFloor() {
        return numFloor;
    }

    public void setNumFloor(String numFloor) {
        this.numFloor = numFloor;
    }

    public int getNumApartment() {
        return numApartment;
    }

    public void setNumApartment(int numApartment) {
        this.numApartment = numApartment;
    }

    public String getApartmentArea() {
        return apartmentArea;
    }

    public void setApartmentArea(String apartmentArea) {
        this.apartmentArea = apartmentArea;
    }

    public String getUtilities() {
        return utilities;
    }

    public void setUtilities(String utilities) {
        this.utilities = utilities;
    }

    public String getRegionLink() {
        return regionLink;
    }

    public void setRegionLink(String regionLink) {
        this.regionLink = regionLink;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Ward getWard() {
        return ward;
    }

    public void setWard(Ward ward) {
        this.ward = ward;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    public Investor getInvestor() {
        return investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }

    public ConstructorContractor getConstructionContractor() {
        return constructionContractor;
    }

    public void setConstructionContractor(ConstructorContractor constructionContractor) {
        this.constructionContractor = constructionContractor;
    }

    public DesignUnit getDesignUnit() {
        return designUnit;
    }

    public void setDesignUnit(DesignUnit designUnit) {
        this.designUnit = designUnit;
    }

    public List<MasterLayout> getMasterLayout() {
        return masterLayout;
    }

    public void setMasterLayout(List<MasterLayout> masterLayout) {
        this.masterLayout = masterLayout;
    }

    public List<Realestate> getRealestates() {
        return realestates;
    }

    public void setRealestates(List<Realestate> realestates) {
        this.realestates = realestates;
    }

    public int getIdOfProvinceFromClient() {
        return idOfProvinceFromClient;
    }

    public void setIdOfProvinceFromClient(int idOfProvinceFromClient) {
        this.idOfProvinceFromClient = idOfProvinceFromClient;
    }

    public int getIdOfDistrictFromClient() {
        return idOfDistrictFromClient;
    }

    public void setIdOfDistrictFromClient(int idOfDistrictFromClient) {
        this.idOfDistrictFromClient = idOfDistrictFromClient;
    }

    public int getIdOfWardFromClient() {
        return idOfWardFromClient;
    }

    public void setIdOfWardFromClient(int idOfWardFromClient) {
        this.idOfWardFromClient = idOfWardFromClient;
    }

    public int getIdOfStreetFromClient() {
        return idOfStreetFromClient;
    }

    public void setIdOfStreetFromClient(int idOfStreetFromClient) {
        this.idOfStreetFromClient = idOfStreetFromClient;
    }

    public int getIdOfProjectFromClient() {
        return idOfProjectFromClient;
    }

    public void setIdOfProjectFromClient(int idOfProjectFromClient) {
        this.idOfProjectFromClient = idOfProjectFromClient;
    }

    public int getIdOfDesignUnitFromClient() {
        return idOfDesignUnitFromClient;
    }

    public void setIdOfDesignUnitFromClient(int idOfDesignUnitFromClient) {
        this.idOfDesignUnitFromClient = idOfDesignUnitFromClient;
    }

    public int getIdOfConstructionFromClient() {
        return idOfConstructionFromClient;
    }

    public void setIdOfConstructionFromClient(int idOfConstructionFromClient) {
        this.idOfConstructionFromClient = idOfConstructionFromClient;
    }

    public int getIdOfInvestorFromClient() {
        return idOfInvestorFromClient;
    }

    public void setIdOfInvestorFromClient(int idOfInvestorFromClient) {
        this.idOfInvestorFromClient = idOfInvestorFromClient;
    }

    public int getIdOfProvince() {
        return getProvince().getId();
    }

    public void setIdOfProvince(int idOfProvince) {
        this.idOfProvince = idOfProvince;
    }

    public int getIdOfDistrict() {
        return getDistrict().getId();
    }

    public void setIdOfDistrict(int idOfDistrict) {
        this.idOfDistrict = idOfDistrict;
    }

    public int getIdOfWard() {
        return getWard().getId();
    }

    public void setIdOfWard(int idOfWard) {
        this.idOfWard = idOfWard;
    }

    public int getIdOfStreet() {
        return getStreet().getId();
    }

    public void setIdOfStreet(int idOfStreet) {
        this.idOfStreet = idOfStreet;
    }

    public int getIdOfInvestor() {
        return getInvestor().getId();
    }

    public void setIdOfInvestor(int idOfInvestor) {
        this.idOfInvestor = idOfInvestor;
    }

    public int getIdOfConstruction() {
        return getConstructionContractor().getId();
    }

    public void setIdOfConstruction(int idOfConstruction) {
        this.idOfConstruction = idOfConstruction;
    }

    public int getIdOfDesignUnit() {
        return getDesignUnit().getId();
    }

    public void setIdOfDesignUnit(int idOfDesignUnit) {
        this.idOfDesignUnit = idOfDesignUnit;
    }

}
