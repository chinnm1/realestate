package com.devcam.shop24h.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.District;
import com.devcam.shop24h.entity.Province;
import com.devcam.shop24h.repository.DistrictRepository;
import com.devcam.shop24h.repository.ProvinceRepository;
import com.devcam.shop24h.service.DistrictService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class DistrictController {

    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    ProvinceRepository provinceRepository;
    @Autowired
    DistrictService districtService;

    @GetMapping("/district")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER','SELLER' )")
    public ResponseEntity<List<District>> getDistrict() {
        try {
            return districtService.getDistrict();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all_district")

    public ResponseEntity<List<District>> getAllDistrict() {
        try {
            return districtService.getDistrict();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/district/province/{provinceId}")

    public ResponseEntity<List<District>> getDistrictByProvinceId(@PathVariable("provinceId") int id) {
        try {
            return districtService.getDistrictByProvinceId(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/district/{id}")

    public ResponseEntity<Optional<District>> getDistrictById(@PathVariable("id") int id) {
        try {
            Optional<District> district = districtRepository.findDistrictById(id);
            return districtService.getDistrictById(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/district/create/{provinceId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> createDistrict(@PathVariable("provinceId") int id,

            @RequestBody District district) {
        try {
            return districtService.createDistrict(id, district);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified district: " +
                            e.getCause().getCause().getMessage());
        }

    }

    @PutMapping("/district/update/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> updateDistrict(@PathVariable("id") int id, @RequestBody District district) {
        try {
            return districtService.updateDistrict(id, district);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified district: " + id + "  for update.");
        }

    }

    @DeleteMapping("/district/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> deleteDistrictById(@PathVariable int id) {
        try {
            return districtService.deleteDistrictById(id);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
