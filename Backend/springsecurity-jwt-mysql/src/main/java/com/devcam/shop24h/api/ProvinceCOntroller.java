package com.devcam.shop24h.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Province;
import com.devcam.shop24h.repository.ProvinceRepository;
import com.devcam.shop24h.service.ProvinceService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class ProvinceCOntroller {
    @Autowired
    ProvinceRepository provinceRepository;
    @Autowired
    ProvinceService provinceService;

    @GetMapping("/province")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER','SELLER' )")
    public ResponseEntity<List<Province>> getProvince() {
        try {
            return provinceService.getProvince();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all_province")
    public ResponseEntity<List<Province>> getAllProvince() {
        try {
            return provinceService.getProvince();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/province/{id}")
    public ResponseEntity<Optional<Province>> getProvinceById(@PathVariable("id") int id) {
        try {
            return provinceService.getProvinceById(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/province/create")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> createProvince(@RequestBody Province province) {
        try {
            return provinceService.createProvince(province);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Province: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/province/update/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> updateProvince(@PathVariable("id") int id, @RequestBody Province province) {
        try {
            return provinceService.updateProvince(id, province);

        } catch (Exception e) {

            return ResponseEntity.badRequest().body("Failed to get specified province: " + id + "  for update.");
        }

    }

    @DeleteMapping("/province/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> deleteProvinceById(@PathVariable int id) {
        try {
            return provinceService.deleteProvinceById(id);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
