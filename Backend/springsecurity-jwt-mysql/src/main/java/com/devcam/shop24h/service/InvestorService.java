package com.devcam.shop24h.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcam.shop24h.entity.Investor;
import com.devcam.shop24h.repository.InvestorRepository;

@Service
public class InvestorService {
    @Autowired
    InvestorRepository investorRepository;

    // lấy tất cả investor
    public ResponseEntity<List<Investor>> getInvestor() {

        List<Investor> listInvestor = investorRepository.findAll();
        if (!listInvestor.isEmpty()) {
            return new ResponseEntity<>(listInvestor, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // lấy investor theo id
    public ResponseEntity<Optional<Investor>> getInvestorById(int id) {

        Optional<Investor> investor = investorRepository.findInvestorById(id);
        if (!investor.isEmpty()) {
            return new ResponseEntity<>(investor, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // tạo mới
    public ResponseEntity<Object> createInvestor(Investor investor) {
        Investor newRole = new Investor();
        newRole.setName(investor.getName());
        newRole.setDescription(investor.getDescription());
        newRole.setAddress(investor.getAddress());
        newRole.setPhone(investor.getPhone());
        newRole.setPhone2(investor.getPhone2());
        newRole.setFax(investor.getFax());
        newRole.setEmail(investor.getEmail());
        newRole.setWebsite(investor.getWebsite());
        newRole.setNote(investor.getNote());

        Investor savedRole = investorRepository.save(newRole);
        return new ResponseEntity<>(savedRole, HttpStatus.CREATED);

    }

    // cập nhật
    public ResponseEntity<Object> updateEmployee(int id, Investor investor) {

        Optional<Investor> investorData = investorRepository.findInvestorById(id);
        if (investorData.isPresent()) {
            Investor newRole = investorData.get();
            newRole.setName(investor.getName());
            newRole.setDescription(investor.getDescription());
            newRole.setAddress(investor.getAddress());
            newRole.setPhone(investor.getPhone());
            newRole.setPhone2(investor.getPhone2());
            newRole.setFax(investor.getFax());
            newRole.setEmail(investor.getEmail());
            newRole.setWebsite(investor.getWebsite());
            newRole.setNote(investor.getNote());
            Investor savedcustomer = investorRepository.save(newRole);
            return new ResponseEntity<>(savedcustomer, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // xóa theo id
    public ResponseEntity<Object> deleteInvestorById(int id) {

        Optional<Investor> investorData = investorRepository.findInvestorById(id);
        Investor investorDelete = investorData.get();
        investorRepository.delete(investorDelete);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

}
