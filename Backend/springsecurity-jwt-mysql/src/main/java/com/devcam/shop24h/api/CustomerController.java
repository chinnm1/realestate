package com.devcam.shop24h.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Customer;

import com.devcam.shop24h.service.CustomerService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping("/customer")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER','SELLER' )")
    public ResponseEntity<List<Customer>> getCustomer() {
        try {
            return customerService.getCustomer();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all_customer")
    public ResponseEntity<List<Customer>> getAllCustomer() {
        try {
            return customerService.getCustomer();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<Optional<Customer>> getCustomerById(@PathVariable("id") int id) {
        try {
            return customerService.getCustomerById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customer/realestate/{realestateId}")
    public ResponseEntity<List<Customer>> getCustomerByRealestateId(@PathVariable("realestateId") int id) {
        try {
            return customerService.getCustomerByRealestateId(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/customer/create/{realestateId}")
    // @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER','SELLER','CUSTOMER' )")
    public ResponseEntity<Object> createCustomer(@PathVariable("realestateId") int id, @RequestBody Customer customer) {
        try {
            return customerService.createCustomer(id, customer);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified customer: " +
                            e.getCause().getCause().getMessage());
        }
    }

    // tạo mới customer không cần realestateId
    @PostMapping("/customer/create")
    // @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER','SELLER','CUSTOMER' )")
    public ResponseEntity<Object> createCustomerWithoutRealestate(@RequestBody Customer customer) {
        try {
            return customerService.createCustomerWithoutRealestate(customer);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified customer: " +
                            e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/customer/update/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER','SELLER','CUSTOMER' )")
    public ResponseEntity<Object> updateCustomer(@PathVariable("id") int id, @RequestBody Customer customer) {
        try {
            return customerService.updateCustomer(id, customer);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified customer: " + id + "  for update.");
        }

    }

    // update customer request
    @PutMapping("/customer/update_request/{userId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER','SELLER','CUSTOMER' )")
    public ResponseEntity<Object> updateCustomerRequest(@PathVariable("userId") int id,
            @RequestBody Customer customer) {
        try {
            return customerService.updateCustomerRequest(id, customer);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified customer: " + id + "  for update.");
        }

    }

    // update user request và realestateId cho người bán
    @PutMapping("/customer/update_seller/{realestateId}/{userId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER','SELLER','CUSTOMER' )")
    public ResponseEntity<Object> updateCustomerRequestAndRealestateForSeller(
            @PathVariable("realestateId") int realestateId,
            @PathVariable("userId") int userId) {
        try {
            return customerService.updateCustomerRequestAndRealestateForSeller(userId, realestateId);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified customer: " +
                            e.getCause().getCause().getMessage());
        }
    }

    // update user request và realestateId và một số thông tin cho người mua
    @PutMapping("/customer/update_buyer/{realestateId}/{userId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER','SELLER','CUSTOMER' )")
    public ResponseEntity<Object> updateCustomerRequestAndRealestateForBuyer(
            @PathVariable("realestateId") int realestateId,
            @PathVariable("userId") int userId, @RequestBody Customer customer) {
        try {
            return customerService.updateCustomerRequestAndRealestateForBuyer(userId, realestateId, customer);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified customer: " +
                            e.getCause().getCause().getMessage());
        }
    }

    // update khách hàng đã contact
    @PutMapping("/customer/update_iscontact/{customerId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER','SELLER' )")
    public ResponseEntity<Object> updateCustomerIsContact(
            @PathVariable("customerId") int customerId,
            @RequestBody Customer customer) {
        try {
            return customerService.updateCustomerContact(customerId, customer);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified customer: " +
                            e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/customer/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER','SELLER' )")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable int id) {
        try {
            return customerService.deleteCustomerById(id);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
