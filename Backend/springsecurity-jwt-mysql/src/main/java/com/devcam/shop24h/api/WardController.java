package com.devcam.shop24h.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.District;
import com.devcam.shop24h.entity.Province;
import com.devcam.shop24h.entity.Ward;
import com.devcam.shop24h.repository.DistrictRepository;
import com.devcam.shop24h.repository.ProvinceRepository;
import com.devcam.shop24h.repository.WardRepository;
import com.devcam.shop24h.service.WardService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class WardController {
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    WardRepository wardRepository;
    @Autowired
    ProvinceRepository provinceRepository;
    @Autowired
    WardService wardService;

    @GetMapping("/ward")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<List<Ward>> getWard() {
        try {
            return wardService.getWard();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all_ward")
    public ResponseEntity<List<Ward>> getAllWard() {
        try {
            return wardService.getWard();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/ward/district/{districtId}")
    public ResponseEntity<List<Ward>> getWardByDistrictId(@PathVariable("districtId") int id) {
        try {
            return wardService.getWardByDistrictId(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/ward/{id}")
    public ResponseEntity<Optional<Ward>> getWardById(@PathVariable("id") int id) {
        try {
            return wardService.getWardById(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/ward/create/{districtId}/{provinceId}")
    public ResponseEntity<Object> createWard(@PathVariable("districtId") int districtid,
            @PathVariable("provinceId") int provinceid,
            @RequestBody Ward ward) {
        try {
            return wardService.createWard(districtid, provinceid, ward);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified ward: " +
                            e.getCause().getCause().getMessage());
        }

    }

    @PutMapping("/ward/update/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> updateDistrict(@PathVariable("id") int id, @RequestBody Ward ward) {
        try {
            return wardService.updateDistrict(id, ward);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified ward: " + id + "  for update.");
        }

    }

    @DeleteMapping("/ward/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' )")
    public ResponseEntity<Object> deleteWardById(@PathVariable int id) {
        try {
            return wardService.deleteWardById(id);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
