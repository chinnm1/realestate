package com.devcam.shop24h.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcam.shop24h.entity.Employee;
import com.devcam.shop24h.repository.EmployeeRepository;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;

    // lấy ra tất cả nhân viên
    public ResponseEntity<List<Employee>> getEmployee() {

        List<Employee> listEmployee = employeeRepository.findAll();
        if (!listEmployee.isEmpty()) {
            return new ResponseEntity<>(listEmployee, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // Lấy ra tất cả nhân viên theo id
    public ResponseEntity<Optional<Employee>> getEmployeeById(int id) {
        Optional<Employee> employee = employeeRepository.findEmployeeById(id);
        if (!employee.isEmpty()) {
            return new ResponseEntity<>(employee, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    // tạo mới nhân viên
    public ResponseEntity<Object> createEmployee(Employee employee) {

        Employee newRole = new Employee();
        newRole.setLastName(employee.getLastName());
        newRole.setFirstName(employee.getFirstName());
        newRole.setTitle(employee.getTitle());
        newRole.setTitle(employee.getTitleOfCourtesy());
        newRole.setBirthday(employee.getBirthday());
        newRole.setHireday(employee.getHireday());
        newRole.setAddress(employee.getAddress());
        newRole.setCity(employee.getCity());
        newRole.setRegion(employee.getRegion());
        newRole.setPostalCode(employee.getPostalCode());
        newRole.setCountry(employee.getCountry());
        newRole.setHomePhone(employee.getHomePhone());
        newRole.setExtension(employee.getExtension());
        newRole.setPhoto(employee.getPhoto());
        newRole.setNote(employee.getNote());
        newRole.setReportTo(employee.getReportTo());
        newRole.setUsername(employee.getUsername());
        newRole.setPassword(employee.getPassword());
        newRole.setEmail(employee.getEmail());
        newRole.setActivated(employee.getStringEnum());
        newRole.setProfile(employee.getProfile());
        newRole.setUserLevel(0);

        Employee savedRole = employeeRepository.save(newRole);
        return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
    }

    // cập nhật nhân viên
    public ResponseEntity<Object> updateEmployee(int id, Employee employee) {
        Optional<Employee> employeeData = employeeRepository.findEmployeeById(id);
        if (employeeData.isPresent()) {
            Employee newRole = employeeData.get();
            newRole.setLastName(employee.getLastName());
            newRole.setFirstName(employee.getFirstName());
            newRole.setTitle(employee.getTitle());
            newRole.setTitle(employee.getTitleOfCourtesy());
            newRole.setBirthday(employee.getBirthday());
            newRole.setHireday(employee.getHireday());
            newRole.setAddress(employee.getAddress());
            newRole.setCity(employee.getCity());
            newRole.setRegion(employee.getRegion());
            newRole.setPostalCode(employee.getPostalCode());
            newRole.setCountry(employee.getCountry());
            newRole.setHomePhone(employee.getHomePhone());
            newRole.setExtension(employee.getExtension());
            newRole.setPhoto(employee.getPhoto());
            newRole.setNote(employee.getNote());
            newRole.setReportTo(employee.getReportTo());
            newRole.setUsername(employee.getUsername());
            newRole.setPassword(employee.getPassword());
            newRole.setEmail(employee.getEmail());
            newRole.setActivated(employee.getStringEnum());
            newRole.setProfile(employee.getProfile());

            Employee savedcustomer = employeeRepository.save(newRole);
            return new ResponseEntity<>(savedcustomer, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // cập nhật user level
    public ResponseEntity<Object> updateEmployeeLevel(int employeeId, int roleId) {
        Optional<Employee> employeeData = employeeRepository.findEmployeeById(employeeId);
        if (employeeData.isPresent()) {
            Employee newRole = employeeData.get();
            newRole.setUserLevel(roleId);
            Employee savedcustomer = employeeRepository.save(newRole);
            return new ResponseEntity<>(savedcustomer, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // xóa nhân viên
    public ResponseEntity<Object> deleteEmployeeById(int id) {

        Optional<Employee> employeeData = employeeRepository.findEmployeeById(id);
        Employee employeeDelete = employeeData.get();
        employeeRepository.delete(employeeDelete);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
