package com.devcam.shop24h.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcam.shop24h.entity.District;
import com.devcam.shop24h.entity.Province;
import com.devcam.shop24h.entity.Ward;
import com.devcam.shop24h.repository.DistrictRepository;
import com.devcam.shop24h.repository.ProvinceRepository;
import com.devcam.shop24h.repository.WardRepository;

@Service
public class WardService {
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    WardRepository wardRepository;
    @Autowired
    ProvinceRepository provinceRepository;

    // lấy tất cả phường xã
    public ResponseEntity<List<Ward>> getWard() {

        List<Ward> listWard = wardRepository.findAll();
        if (!listWard.isEmpty()) {
            return new ResponseEntity<>(listWard, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // lấy phường xã theo quận huyện
    public ResponseEntity<List<Ward>> getWardByDistrictId(int id) {

        List<Ward> listWard = wardRepository.findWardByDistrictId(id);
        if (!listWard.isEmpty()) {
            return new ResponseEntity<>(listWard, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // lấy phường xã theo id
    public ResponseEntity<Optional<Ward>> getWardById(int id) {

        Optional<Ward> listWard = wardRepository.findWardById(id);
        if (!listWard.isEmpty()) {
            return new ResponseEntity<>(listWard, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    // tạo mới phường xã
    public ResponseEntity<Object> createWard(int districtid,
            int provinceid,
            Ward ward) {

        Optional<District> districtData = districtRepository.findDistrictById(districtid);
        Optional<Province> provinceData = provinceRepository.findProvinceById(provinceid);

        if (districtData.isPresent() && provinceData.isPresent()) {
            Ward newRole = new Ward();
            newRole.setName(ward.getName());
            newRole.setPrefix(ward.getPrefix());
            District _District = districtData.get();
            newRole.setDistrict(_District);
            Province _Province = provinceData.get();
            newRole.setProvince(_Province);

            Ward savedRole = wardRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // cập nhật phường xã
    public ResponseEntity<Object> updateDistrict(int id, Ward ward) {

        Optional<Ward> districtData = wardRepository.findWardById(id);
        if (districtData.isPresent()) {
            Ward newRole = districtData.get();
            newRole.setName(ward.getName());
            newRole.setPrefix(ward.getPrefix());
            Ward savedRole = wardRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // xóa phường xã
    public ResponseEntity<Object> deleteWardById(int id) {

        Optional<Ward> districtData = wardRepository.findWardById(id);
        Ward wardDelete = districtData.get();
        wardRepository.delete(wardDelete);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
