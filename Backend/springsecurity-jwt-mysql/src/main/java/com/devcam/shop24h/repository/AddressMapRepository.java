package com.devcam.shop24h.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import com.devcam.shop24h.entity.AddressMap;

@Repository

public interface AddressMapRepository extends JpaRepository<AddressMap, Long> {
    @Query(value = "SELECT * FROM `address_map` WHERE id=:id", nativeQuery = true)
    Optional<AddressMap> getAddressMapById(@Param("id") int id);
}
