package com.devcam.shop24h.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.Investor;

@Repository
public interface InvestorRepository extends JpaRepository<Investor, Long> {
    @Query(value = "SELECT * FROM investor WHERE id=:id", nativeQuery = true)
    Optional<Investor> findInvestorById(@Param("id") int id);

}
