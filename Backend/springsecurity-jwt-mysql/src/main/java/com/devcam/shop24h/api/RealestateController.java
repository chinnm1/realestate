package com.devcam.shop24h.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Realestate;

import com.devcam.shop24h.service.RealestateService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class RealestateController {

    @Autowired
    RealestateService realestateService;

    @GetMapping("/realestate_pageable/{numberPage}")
    public ResponseEntity<List<Realestate>> getAllRealestate(@PathVariable("numberPage") int numberPage) {

        try {
            return realestateService.getAllRealestatePageable(numberPage);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/realestate")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER' ,'SELLER')")
    public ResponseEntity<List<Realestate>> getRealestate() {
        try {
            return realestateService.getRealestate();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // lấy ra tất cả bđs đã được confirm
    @GetMapping("/all_realestate")
    public ResponseEntity<List<Realestate>> getAllRealestate() {
        try {
            return realestateService.getRealestateConfirm();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestate/orderby_date")
    public ResponseEntity<List<Realestate>> getRealestateOrderByDate() {
        try {
            return realestateService.getRealestateOrderByDate();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestate/detail/{id}")
    public ResponseEntity<Optional<Realestate>> getRealestateById(@PathVariable int id) {
        try {
            return realestateService.getRealestateById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestate_confirm/detail/{id}")
    public ResponseEntity<Optional<Realestate>> getRealestateConfirmById(@PathVariable int id) {
        try {
            return realestateService.getRealestateConfirmById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestate/{provinceId}/{districtId}/{wardId}")
    public ResponseEntity<List<Realestate>> filterRealestate(@PathVariable("provinceId") int provinceId,
            @PathVariable("districtId") int districtId, @PathVariable("wardId") int wardId) {
        try {

            return realestateService.filterRealestate(provinceId, districtId, wardId);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestate/{provinceId}")
    public ResponseEntity<List<Realestate>> filterRealestateByProvince(@PathVariable("provinceId") int provinceId) {
        try {

            return realestateService.filterRealestateByProvince(provinceId);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestate/province_district_ward_street/{id}")
    public ResponseEntity<Object> getRealestateProvinceDistrictWardStreetById(@PathVariable int id) {
        try {
            return realestateService.getRealestateProvinceDistrictWardStreetById(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestate/project/{id}")
    public ResponseEntity<Object> getRealestateProjectIdById(@PathVariable int id) {
        try {
            return realestateService.getRealestateProjectIdById(id);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("project/realestate/{id}/{numberPage}")
    public ResponseEntity<List<Realestate>> getAllRealestateByProjectId(@PathVariable("id") int id,
            @PathVariable("numberPage") int numberPage) {

        try {
            return realestateService.getAllRealestateByProjectId(id, numberPage);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // update bđs
    @PutMapping("/realestate/update/{realestateid}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER','SELLER')")
    public ResponseEntity<Object> updateRealestate(@PathVariable("realestateid") int id,
            @RequestBody Realestate realestate) {
        try {
            return realestateService.updateRealestate(id, realestate);

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified realestate:" + id + " for update.");
        }
    }

    // update status bđs
    @PutMapping("/realestate/update_status/{realestateid}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER','SELLER')")
    public ResponseEntity<Object> changeRealestateStatus(@PathVariable("realestateid") int id,
            @RequestBody Realestate realestate) {
        try {
            return realestateService.changeRealestateStatus(id, realestate);

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified realestate:" + id + " for update.");
        }
    }

    @PostMapping("/realestate/create/{userId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER', 'CUSTOMER','SELLER' )")
    public ResponseEntity<Object> createRealestateNew(
            @RequestBody Realestate realestate, @PathVariable("userId") int userId) {
        try {
            return realestateService.createRealestateNew(realestate, userId);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified orderdetail: " +
                            e.getMessage());
        }

    }

    @DeleteMapping("/realestate/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
    public ResponseEntity<Object> deleteOrderDeatilById(@PathVariable int id) {
        try {
            return realestateService.deleteOrderDeatilById(id);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
