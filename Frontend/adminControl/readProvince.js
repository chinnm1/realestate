
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gPROVINCE_COLS = ["id", "name", "code", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gPROVINCE_STT_COL = 0;
const gPROVINCE_NAME_COL = 1;
const gPROVINCE_PREFIX_COL = 2;
const gPROVINCE_ACTION_COL = 3;
var table = $("#province-table").DataTable({
    columns: [
        { data: gPROVINCE_COLS[gPROVINCE_STT_COL] },
        { data: gPROVINCE_COLS[gPROVINCE_NAME_COL] },
        { data: gPROVINCE_COLS[gPROVINCE_PREFIX_COL] },
        { data: gPROVINCE_COLS[gPROVINCE_ACTION_COL] },
    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gPROVINCE_ACTION_COL,
            defaultContent: `
					<i class="fas fa-edit edit-customer"></i>
					<i class="fas fa-trash-alt delete-customer"></i>
			`
        },

    ],
    scrollX: true,
    scrollCollapse: true,

    fixedColumns: true
})
//biến chứa hàng được chọn
var gRowSelected = ''
//biến chứa danh sách province
var glistProvince = [];
//biến chứa id province 
var gProvinceIdSelected = '';
//biến chứa hàng province
var gRowProvinceSelected = '';
//biến chứa list provine update
var glistProvinceValidateUpdate = [];
//biến chứa user id
var gUserId = '';
//biến chứ header
var gHeader = ''
var gUserName = ''
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    const token = getCookie("token");
    const userId = getCookie("userId");
    const userName = getCookie('username')

    //Gọi API để lấy thông tin người dùng

    //Khai báo xác thực ở headers
    gHeader = {
        Authorization: "Token" + " " + token
    };
    //gán giá trị cho biến userId
    gUserId = Number(userId);
    gUserName = userName;
    console.log('CHECK USER ID', gUserId);
    console.log('CHECK HEADER', gHeader);
    console.log('CHECK USERNAME', gUserName);
    $('#username').html(gUserName)
    //tải trang
    onPageLoading().then((res) => {
        glistProvince = res
        console.log('check data get all  from server', res);
        table.clear();
        table.rows.add(res);
        table.draw();
    }).catch((err) => {
        console.log(err)
    });
    //create new province
    $('#create-province').on('click', function () {
        onBtnAddProvinceClick();
    })
    $('#btn-create-province').on('click', function () {

        onBtnCreateProvinceOnModalClick();
    })
    //edit province
    $('#province-table').on('click', '.edit-customer', function () {

        onBtnEditProvinceClick(this);
    })
    $('#btn-update-province').on('click', function () {
        onBtnUpdateOnModalClick()
    })

    //xóa province
    $('#province-table').on('click', '.delete-customer', function () {

        onBtnDeleteProvinceClick(this);
    })
    $('#btn-confirm-delete-province').on('click', function () {

        onBtnConfirmOnDeleteModalClick(this);
    })
    //nhấn nút Logout
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })


})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm gọi api load danh sách province từ server khi tải trang
function onPageLoading() {
    console.log('làm tới đây')
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/province",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm thực hiện khi nhấn nút add new province
function onBtnAddProvinceClick() {
    $("#create-province-modal").modal("show");

}
//hàm thực hiện khi nhấn create province trên modal
function onBtnCreateProvinceOnModalClick() {
    createNewProvince();
    $("#create-province-modal").modal("hide");
}
//hàm thực hiện khi lúc nút edit
function onBtnEditProvinceClick(paramButton) {
    $("#update-province-modal").modal("show");
    gProvinceIdSelected = getProvinceIdFromButton(paramButton)
    console.log('check id customer selected', gProvinceIdSelected)
    callApiToGetProvinceById(gProvinceIdSelected).then((res) => {
        console.log('check data customer need to update ress', res);
        showDataUpdateOnForm(res);
    }).catch((err) => {
        console.log(err)
    });
}
//hàm thực hiện khi nhấn nút update trên modal
function onBtnUpdateOnModalClick() {
    updateProvince(gProvinceIdSelected);
    $("#update-province-modal").modal("hide");

}
//Hàm thực hiện khi nhấn nút delete
function onBtnDeleteProvinceClick(paramButton) {
    $("#delete-modal").modal("show");
    gProvinceIdSelected = getProvinceIdFromButton(paramButton)

}
//hàm thực hiện khi nhấn nút confirm trên delete modal
function onBtnConfirmOnDeleteModalClick() {
    deletedProvince();
    $("#delete-modal").modal("hide");

}
//hàm thực hiện khi click nút log out 
function onBtnLogOutClick() {
    //
    redirectToLogin();

}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
//Hàm get Cookie 
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//hàm trả ra id province khi nhấn nút edit
function getProvinceIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    gRowProvinceSelected = table.row(vTableRow).data();
    console.log(gRowProvinceSelected.id);
    return gRowProvinceSelected.id;
}
//hàm gọi api để lấy province cần edit theo id
function callApiToGetProvinceById(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/province/${id}`,
            async: false,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res);

            },
            error: function (err) {
                reject(e)
            }
        })
    })


}
//hàm hiển thị data lên form edit
function showDataUpdateOnForm(paramProvince) {
    $('#input-name-update').val(paramProvince.name);
    $('#input-prefix-update').val(paramProvince.code);

}
//update province
function updateProvince(paramProvinceId) {
    let objData = {
        name: "",
        code: ""

    }
    let objDataFromForm = getDataOnFormUpdate(objData);
    callApiToUpdateData(paramProvinceId, objDataFromForm).then((res) => {
        console.log(res);
    }).catch((errr) => {
        console.log(errr)
    })
    onPageLoading();

}




//hàm thu thập dư liệu trên form update
function getDataOnFormUpdate(obj) {

    obj.name = $('#input-name-update').val();
    obj.code = $('#input-prefix-update').val();

    return obj
}
//hàm gọi api để update province
function callApiToUpdateData(id, paramOrderObject) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/province/update/${id}`,
            type: 'PUT',
            async: false,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrderObject),
            headers: gHeader,
            success: function (res) {
                resolve(res);

            },
            error: function (error) {
                reject(err)
            }
        })
    })

}

//save new province
function createNewProvince() {
    let objData = {
        name: '',
        code: '',
    }
    let objDataFromForm = getDataOnForm(objData);
    callApiToPostData(objDataFromForm);
    onPageLoading();
}
//hàm thu thập dư liệu trên form
function getDataOnForm(obj) {
    obj.name = $('#input-name').val();
    obj.code = $('#input-prefix').val();

    return obj
}
//hàm gọi api để tạo province mới
function callApiToPostData(paramOrderObject) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/province/create`,
            type: 'POST',
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrderObject),
            headers: gHeader,
            success: function (res) {

                resolve(res)

            },
            error: function (error) {
                reject(err)
            }
        })
    })

}
//xóa province
function deletedProvince() {
    callApiToDeleteData(gProvinceIdSelected);
    onPageLoading();
}
//gọi api xóa province
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/province/delete/${id}`,
        type: 'DELETE',
        async: false,
        headers: gHeader,
        success: function (res) {
            alert('Delete success')

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}




