
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gDISTRICT_COLS = ["id", "name", "prefix", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gDISTRICT_STT_COL = 0;
const gDISTRICT_NAME_COL = 1;
const gDISTRICT_PREFIX_COL = 2;
const gDISTRICT_ACTION_COL = 3;
var table = $("#district-table").DataTable({
    columns: [
        { data: gDISTRICT_COLS[gDISTRICT_STT_COL] },
        { data: gDISTRICT_COLS[gDISTRICT_NAME_COL] },
        { data: gDISTRICT_COLS[gDISTRICT_PREFIX_COL] },
        { data: gDISTRICT_COLS[gDISTRICT_ACTION_COL] },
    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gDISTRICT_ACTION_COL,
            defaultContent: `
            <i class="fas fa-edit edit-district"></i>
            <i class="fas fa-trash-alt delete-district"></i>
    `
        },

    ],
    scrollX: true,
    scrollCollapse: true,

    fixedColumns: true
})
//biến chứa hàng được chọn
var gRowSelected = ''
//biến chứa danh sách ward
var glistWard = [];
//biến chứa id ward
var gWardIdSelected = '';
//biến chứa danh sách province
var gProvinceList = [];
//biến chứa danh sách district
var gDistrictList = [];
//biến chứa id province
var gProvinceIdSelected = '';
//biến chứa  province select
var gProvinceSelectElement = $("#select-province");
//biến chứa id district
var gDistrictIdSelected = '';
//biến chứa district select
var gDistrictSelectElement = $("#select-district");
var gRowProvinceSelected = '';
var glistWardValidateUpdate = [];
//biến chứa user id
var gUserId = '';
//biến chứ header
var gHeader = ''
var gUserName = ''
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    const token = getCookie("token");
    const userId = getCookie("userId");
    const userName = getCookie('username')

    //Gọi API để lấy thông tin người dùng

    //Khai báo xác thực ở headers
    gHeader = {
        Authorization: "Token" + " " + token
    };
    //gán giá trị cho biến userId
    gUserId = Number(userId);
    gUserName = userName;
    console.log('CHECK USER ID', gUserId);
    console.log('CHECK HEADER', gHeader);
    console.log('CHECK USERNAME', gUserName);
    $('#username').html(gUserName)
    //tải trang
    onPageLoading().then((res) => {
        glistWard = res
        console.log('check data get all  from server', res);
        table.clear();
        table.rows.add(res);
        table.draw();
    }).catch((err) => {
        console.log(err)
    });
    //create new ward
    $('#create-ward').on('click', function () {
        onBtnAddWardClick();
    })
    //chọn thành phố
    //CHọn tỉnh thành phhos
    $("#select-province").on('change', function () {
        onSelectProvinceChange();
    })
    //CHọn tỉnh thành quận huyện
    $("#select-district").on('change', function () {
        onSelectDistrictChange();
    })
    $('#btn-create-ward').on('click', function () {

        onBtnCreateWardOnModalClick();
    })
    //edit ward
    $('#district-table').on('click', '.edit-district', function () {

        onBtnEditWardClick(this);
    })
    $('#btn-update-ward').on('click', function () {
        onBtnUpdateOnModalClick()
    })

    //xóa ward
    $('#district-table').on('click', '.delete-district', function () {
        onBtnDeleteWardClick(this);
    })
    $('#btn-confirm-delete-ward').on('click', function () {

        onBtnConfirmOnDeleteModalClick(this);
    })
    //nhấn nút Logout
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })

})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm gọi api load danh sách province từ server khi tải trang
function onPageLoading() {
    console.log('làm tới đây')
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/ward",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm thực hiện khi nhấn nút add new ward
function onBtnAddWardClick() {
    $("#create-ward-modal").modal("show");
    callApiToGetProvince().then((res) => {
        gProvinceList = res;
        loadDataToProvinceSelect(res)
    }).catch((err) => {
        console.log(err)
    });

}
//hàm thực hiện khi thay đổi select province
function onSelectProvinceChange() {
    if (gProvinceList && gProvinceList.length > 0) {
        gProvinceList.map(item => {
            if (item.id == $("#select-province option:selected").val()) {
                gProvinceIdSelected = item.id
            }
        })
    }

    console.log('chech province id selected', gProvinceIdSelected);
    callApiToGetDistrictByProvinceId(gProvinceIdSelected).then((res) => {
        gDistrictList = res
        console.log('check data get all  from server', res);
        loadDataToDistrictSelect(res)
        // table.clear();
        // table.rows.add(res);
        // table.draw();
    }).catch((err) => {
        console.log(err)
    });
}
//hàm thực hiện khi thay đổi select district
function onSelectDistrictChange() {
    if (gDistrictList && gDistrictList.length > 0) {
        gDistrictList.map(item => {
            if (item.id == $("#select-district option:selected").val()) {
                gDistrictIdSelected = item.id
            }
        })
    }

    console.log('chech district id selected', gDistrictIdSelected);
}

//hàm thực hiện khi nhấn create ward trên modal
function onBtnCreateWardOnModalClick() {
    createNewDistrict();
    $("#create-ward-modal").modal("hide");
}
//hàm thực hiện khi lúc nút edit
function onBtnEditWardClick(paramButton) {
    $("#update-ward-modal").modal("show");
    gWardIdSelected = getWardIdFromButton(paramButton)
    console.log('check id ward selected', gWardIdSelected)
    callApiToGetWardById(gWardIdSelected).then((res) => {
        console.log('check data customer need to update ress', res);
        showDataUpdateOnForm(res);
    }).catch((err) => {
        console.log(err)
    });
}
//hàm thực hiện khi nhấn nút update trên modal
function onBtnUpdateOnModalClick() {
    updateWard(gWardIdSelected);
    $("#update-ward-modal").modal("hide");

}
//Hàm thực hiện khi nhấn nút delete
function onBtnDeleteWardClick(paramButton) {
    $("#delete-modal").modal("show");
    gWardIdSelected = getWardIdFromButton(paramButton)

}
//hàm thực hiện khi nhấn nút confirm trên delete modal
function onBtnConfirmOnDeleteModalClick() {
    deletedWard();
    $("#delete-modal").modal("hide");

}
//hàm thực hiện khi click nút log out 
function onBtnLogOutClick() {
    //
    redirectToLogin();

}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
//Hàm get Cookie 
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//hàm trả ra id ward khi nhấn nút edit
function getWardIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    gRowProvinceSelected = table.row(vTableRow).data();
    console.log(gRowProvinceSelected.id);
    return gRowProvinceSelected.id;
}
//hàm call api để lấy ra  tỉnh thành phố
function callApiToGetProvince() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/province",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm call api để lấy ra  quận huyện theo tỉnh
function callApiToGetDistrictByProvinceId(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/district/province/${id}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm đổ dữ liệu vào select province
function loadDataToProvinceSelect(pProvinceList) {
    gProvinceSelectElement.find('option').remove()
    gProvinceSelectElement.append('<option value=none>Chọn tỉnh thành phố</option>')
    for (i = 0; i < pProvinceList.length; i++) {
        var bProvinceOption = $("<option/>");
        bProvinceOption.prop("value", pProvinceList[i].id);
        bProvinceOption.prop("text", pProvinceList[i].name);
        gProvinceSelectElement.append(bProvinceOption);

    };
}
//hàm đổ dữ liệu vào select district
function loadDataToDistrictSelect(pDistrictList) {
    gDistrictSelectElement.find('option').remove()
    gDistrictSelectElement.append('<option value=none>Chọn tỉnh quận huyện</option>')
    for (i = 0; i < pDistrictList.length; i++) {
        var bDistrictOption = $("<option/>");
        bDistrictOption.prop("value", pDistrictList[i].id);
        bDistrictOption.prop("text", pDistrictList[i].name);
        gDistrictSelectElement.append(bDistrictOption);
    };
}
//hàm gọi api để lấy ward cần edit theo id
function callApiToGetWardById(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/ward/${id}`,
            async: false,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res);
            },
            error: function (err) {
                reject(e)
            }
        })
    })


}
//hàm hiển thị data lên form edit
function showDataUpdateOnForm(paramProvince) {
    $('#input-name-update').val(paramProvince.name);
    $('#input-prefix-update').val(paramProvince.prefix);
}

function updateWard(paramWardId) {
    let objData = {
        name: "",
        prefix: ""
    }
    let objDataFromForm = getDataOnFormUpdate(objData);
    let vCheck = validateDataOnForm(objData);
    if (vCheck) {
        callApiToUpdateData(paramWardId, objDataFromForm).then((res) => {
            console.log(res);
        }).catch((errr) => {
            console.log(errr)
        })
        onPageLoading();
    }


}

//hàm thu thập dư liệu trên form update
function getDataOnFormUpdate(obj) {

    obj.name = $('#input-name-update').val();
    obj.prefix = $('#input-prefix-update').val();

    return obj
}
//hàm gọi api để update ward
function callApiToUpdateData(id, paramOrderObject) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/ward/update/${id}`,
            type: 'PUT',
            async: false,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrderObject),
            headers: gHeader,
            success: function (res) {
                console.log("CHECK DATA UPDATE", res)
                resolve(res);
            },
            error: function (error) {
                reject(err)
            }
        })
    })

}
//save new ward
function createNewDistrict() {
    let objData = {
        name: '',
        prefix: '',
    }
    let objDataFromForm = getDataOnForm(objData);
    var vCheck = validateDataOnForm(objDataFromForm);
    if (vCheck) {
        callApiToPostData(objDataFromForm, gProvinceIdSelected, gDistrictIdSelected);
        onPageLoading();
    }

}
//hàm thu thập dư liệu trên form
function getDataOnForm(obj) {
    obj.name = $('#input-name').val();
    obj.prefix = $('#input-prefix').val();
    return obj
}
//hàm validate dữ liệu
function validateDataOnForm(paramObj) {
    if (paramObj.name == "") {
        alert("phải điền tên tỉnh");
        return false
    }
    if (paramObj.prefix == "") {
        alert("phải điền tên tỉnh");
        return false
    }
    if (paramObj.name == "") {
        alert("phải điền tên thành phố");
        return false
    }
    return true;
}
//hàm gọi api để tạo province mới
function callApiToPostData(paramOrderObject, provinceId, districtId) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/ward/create/${districtId}/${provinceId}`,
            type: 'POST',
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrderObject),
            headers: gHeader,
            success: function (res) {

                resolve(res)

            },
            error: function (error) {
                reject(err)
            }
        })
    })

}
//xóa ward
function deletedWard() {
    callApiToDeleteData(gWardIdSelected);
    onPageLoading();
}
//gọi api xóa ward
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/ward/delete/${id}`,
        type: 'DELETE',
        async: false,
        headers: gHeader,
        success: function (res) {
            alert('Delete success')

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}




