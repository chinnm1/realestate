//biến ds tỉnh thành phố
var gProvinceList = {};
//biến chứa element select tỉnh thành phố
var gProvinceSelectElement = $("#select-province");
//biến chứa id tỉnh thành phố được chọn
var gProvinceIdSelectedd = ''
//biến chứa element select quận huyện
var gDistrictSelectElement = $("#select-district");
//biến ds tỉnh quận huyện
var gDistrictList = {};
//biến chứa id  quận huyện được chọn
var gDistrictIdSelectedd = ''
//biến chứa element select phường xã
var gWardSelectElement = $("#select-ward");
//biến ds  phường xã
var gWardList = {};
//biến chứa id tỉnh phường xã được chọn
var gWardIdSelectedd = ''
//biến chứa element select đường
var gStreetelectElement = $("#select-street");
//biến ds đường
var gStreetList = {};
//biến chứa id đường được chọn
var gStreetIdSelectedd = ''
//biến chứa element select dự án
var gProjectSelectElement = $("#select-project");
//biến ds dự án
var gProjectList = {};
//biến chứa id dự án được chọn
var gProjectIdSelectedd = ''
var gUserId = '';
//biến chứ header
var gHeader = ''
var gUserName = ''

$(document).ready(function () {
    console.log('chec trnag')
    const token = getCookie("token");
    const userId = getCookie("userId");
    const userName = getCookie('username')

    //Gọi API để lấy thông tin người dùng

    //Khai báo xác thực ở headers
    gHeader = {
        Authorization: "Token" + " " + token
    };
    //gán giá trị cho biến userId
    gUserId = Number(userId);
    console.log('CHECK USER ID', gUserId);
    console.log('CHECK HEADER', gHeader)
    console.log('CHECK USERNAME', gUserName);
    $('#username').html(gUserName)
    //lấy tất cả tỉnh thành phố từ server
    callApiToGetProvince().then((res) => {
        gProvinceList = res;
        //Hiển thị dữ liệu vào ô select
        loadDataToProvinceSelect(res)
    }).catch((err) => {
        console.log(err)
    });
    //Hiển thị form điền thông tin khi nhấn navbar
    changeFormDisplay()
    //Hiển thị form thông tin đầu tiên khi load trang
    $("#add-realestate-1").attr("style", "display:block");
    //CHọn tỉnh thành phố
    $("#select-province").on('change', function () {
        onSelectProvinceChange();
    })
    //CHọn quận huyện
    $("#select-district").on('change', function () {
        onSelectDistrictChange();
    })
    //CHọn phường xã
    $("#select-ward").on('change', function () {
        onSelectWardChange();
    })
    //CHọn đường
    $("#select-street").on('change', function () {
        onSelectStreetChange();
    })
    //thêm bất động sản mới
    $("#btn-add").on('click', function () {

        onBtnAddClick();
    })
    //nhấn nút Logout
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })


})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm thực hiện khi click nút log out 
function onBtnLogOutClick() {
    //
    redirectToLogin();

}

//hàm thực hiện khi thay đổi select province
function onSelectProvinceChange() {
    if (gProvinceList && gProvinceList.length > 0) {
        gProvinceList.map(item => {
            if (item.id == $("#select-province option:selected").val()) {
                gProvinceIdSelectedd = item.id
            }
        })
    }
    console.log('chech province id selected', gProvinceIdSelectedd)
    //gọi api lấy quận huyện theo id tỉnh thành phố
    callApiToGetDistrictByProvinceId().then((res) => {
        gDistrictList = res;
        //Hiển thi quận huyện vào ô select
        loadDataToSelectDistrict(res);
    }).catch((e) => {
        console.log(e)
    }

    );
}
//hàm thực hiện khi thay đổi select district
function onSelectDistrictChange() {
    if (gDistrictList && gDistrictList.length > 0) {
        gDistrictList.map(item => {
            if (item.id == $("#select-district option:selected").val()) {
                gDistrictIdSelectedd = item.id
            }
        })
    }
    console.log('chech district id selected', gDistrictIdSelectedd)
    //gọi api lấy ra danh sách phường xã theo id tỉnh thành phố
    callApiToGetWardByDistrictId().then((res) => {
        gWardList = res;
        //hiển thị thông tin phường xã lên ô select
        loadDataToSelectWard(res);
    }).catch((e) => {
        console.log(e)
    }
    );
    //gọi api lấy ra ds đường theo tỉnh thành phố id và quận huyện id
    callApiToGetStreetByProvinceIdAndDistrictId().then((res) => {
        gStreetList = res;
        loadDataToSelectStreet(res);
    }).catch((e) => {
        console.log(e)
    }
    );;

}
//hàm thực hiện khi nhấn nút add
function onBtnAddClick() {
    //B1:tạo đối tượng chứa thông
    let realEstate = {
        title: "",
        type: 0,
        request: 0,
        address: "",
        price: 0,
        priceMin: 0,
        priceTime: 0,
        acreage: 0,
        direction: "",
        numberFloor: 0,
        bath: 0,
        apartCode: "",
        wallArea: 0,
        bedroom: 0,
        balcony: 0,
        landscapeView: "",
        apartLoca: 0,
        apartType: 0,
        furnitureType: 0,
        priceRent: 0,
        returnRate: 0,
        legalDoc: 0,
        description: "",
        width: 0,
        long: 0,
        viewNum: 0,
        shape: "",
        distance2facade: 0,
        adjacentFacadeNum: 0,
        adjacentRoad: "",
        alleyMinWidth: 0,
        adjacentAlleyMinWidth: 0,
        factor: "",
        structure: "",
        dtsxd: 0,
        clcl: 0,
        ctxdPrice: 0,
        ctxdValue: 0,
        photo: "",
        lat: 0,
        lng: 0,
        idOfProvinceFromClient: -1,
        idOfDistrictFromClient: -1,
        idOfWardFromClient: -1,
        idOfStreetFromClient: -1,
        idOfProjectFromClient: -1
    }
    //B2 thu thập dữ liệu
    getDataFromForm(realEstate);
    console.log('check data', realEstate)
    //B3 validate dữ liệu
    let vCheck = validateData(realEstate);
    if (vCheck) {
        //B4 gọi api để lưu dữ liệu
        callApiToPostDataToServer(realEstate);
    }
}
//hàm thực hiện khi thay đổi select ward
function onSelectWardChange() {
    if (gWardList && gWardList.length > 0) {
        gWardList.map(item => {
            if (item.id == $("#select-ward option:selected").val()) {
                gWardIdSelectedd = item.id
            }
        })
    }

}
//hàm thực hiện khi thay đổi select street
function onSelectStreetChange() {
    if (gStreetList && gStreetList.length > 0) {
        gStreetList.map(item => {
            if (item.id == $("#select-street option:selected").val()) {
                gStreetIdSelectedd = item.id
            }
        })
    }
    console.log('chech street id selected', gStreetIdSelectedd)
    //Gọi api để lấy ra ds dự án theo tỉnh thành phố, quận huyện, phường xã, đường id
    callApiToGetProjectByProvinceDistrictWardStreet().then((res) => {
        gProjectList = res;
        //hiển thị data vào ô select
        loadDataToSelectProject(res);
    }).catch((e) => {
        console.log(e)
    }
    );;

}

/*** REGION 4- COmmon function - Vùng khai báo các hàm dùng chung*/
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

//hàm lấy cookie
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//Hàm thu thập dữ liệu trên form
function getDataFromForm(paramObject) {
    paramObject.title = $("#input-title").val();
    paramObject.type = $("#select-type").val();
    paramObject.request = $("#select-request").val();
    paramObject.address = $("#input-address").val();
    paramObject.price = $("#input-price").val();
    paramObject.priceMin = $("#input-price-min").val();
    paramObject.priceTime = $("#select-price-time").val();
    paramObject.acreage = $("#input-area").val();
    paramObject.direction = $("#input-direction").val();
    paramObject.numberFloor = $("#input-number-floor").val();
    paramObject.bath = $("#input-bath").val();
    paramObject.apartCode = $("#input-apart-code").val();
    paramObject.wallArea = $("#input-wall-area").val();
    paramObject.bedroom = $("#input-bedroom").val();
    paramObject.balcony = $("#select-balcony").val();
    paramObject.landscapeView = $("#input-landscape").val();
    paramObject.apartLoca = $("#select-apart-loca").val();
    paramObject.apartType = $("#select-apart-type").val();
    paramObject.furnitureType = $("#select-furniture-type").val();
    paramObject.priceRent = $("#input-price-rent").val();
    paramObject.returnRate = $("#input-return-rate").val();
    paramObject.legalDoc = $("#input-legal-docs").val();
    paramObject.description = $("#input-description").val();
    paramObject.Width = $("#input-width").val();
    paramObject.Long = $("#input-long").val();
    paramObject.viewNum = $("#input-view-num").val();
    paramObject.shape = $("#input-shape").val();
    paramObject.distance2facade = $("#input-distance2facade").val();
    paramObject.adjacentFacadeNum = $("#input-adjacent-facade-num").val();
    paramObject.adjacentRoad = $("#input-adjacent-road").val();
    paramObject.alleyMinWidth = $("#input-alley-min-width").val();
    paramObject.adjacentAlleyMinWidth = $("#input-adjacent-alley-min-width").val();
    paramObject.factor = $("#input-factor").val();
    paramObject.structure = $("#input-structure").val();
    paramObject.dtsxd = $("#input-dtsxd").val();
    paramObject.clcl = $("#input-clcl").val();
    paramObject.ctxdPrice = $("#input-ctxd-price").val();
    paramObject.ctxdValue = $("#input-ctxd-value").val();
    paramObject.photo = "";
    paramObject.lat = $("#input-lat").val();
    paramObject.lng = $("#input-lng").val();
    paramObject.idOfProvinceFromClient = $("#select-province").val();
    paramObject.idOfDistrictFromClient = $("#select-district").val();
    paramObject.idOfWardFromClient = $("#select-ward").val();
    paramObject.idOfStreetFromClient = $("#select-street").val();
    paramObject.idOfProjectFromClient = $("#select-project").val();
    return paramObject;

}

//hàm check validate data
function validateData(paramObject) {
    if (paramObject.idOfProvinceFromClient == -1) {
        alert("Phải chọn tỉnh thành phố")
        return false;
    }
    if (paramObject.idOfDistrictFromClient == -1) {
        alert("Phải chọn quận huyện")
        return false;

    }
    if (paramObject.idOfWardFromClient == -1) {
        alert("Phải chọn phường xã")
        return false;

    }
    if (paramObject.idOfStreetFromClient == -1) {
        alert("Phải chọn đường")
        return false;

    }
    if (paramObject.address == "") {
        alert("Phải điền địa chỉ")
        return false;

    }
    if (paramObject.idOfProjectFromClient == "diffOption" || paramObject.idOfProjectFromClient == "none") {
        paramObject.idOfProjectFromClient = 0;

    }
    return true;

}
//hàm gọi api để lưu dữ liệu
function callApiToPostDataToServer(paramObject) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/realestate/create`,
            type: 'POST',
            contentType: "application/json;charset=UTF-8",
            headers: gHeader,
            data: JSON.stringify(paramObject),
            success: function (res) {

                alert("Create success");
                window.location.href = `datareadRealestate.html`
            },
            error: function (error) {
                console.assert(error.responseText)
            }
        })

    })

}

//hàm call api để lấy ra các quận huyện theo tỉnh thành phố
function callApiToGetDistrictByProvinceId() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/district/province/${gProvinceIdSelectedd}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })
}
//hàm call api để lấy ra các phường xã theo quận huyện
function callApiToGetWardByDistrictId() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/ward/district/${gDistrictIdSelectedd}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })
}
//hàm call api để lấy ra  tỉnh thành phố
function callApiToGetProvince() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/province",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm call api để lấy ra đường theo tỉnh quận
function callApiToGetStreetByProvinceIdAndDistrictId() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/street/${gDistrictIdSelectedd}/${gProvinceIdSelectedd}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm call api để lấy ra project
function callApiToGetProjectByProvinceDistrictWardStreet() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/project/${gProvinceIdSelectedd}/${gDistrictIdSelectedd}/${gWardIdSelectedd}/${gStreetIdSelectedd}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}


//hàm đổ dữ liệu vào select tỉnh thành phố
function loadDataToProvinceSelect(pProvinceList) {
    gProvinceSelectElement.find('option').remove()
    gProvinceSelectElement.append('<option value=none>Chọn tỉnh thành phố</option>')
    for (i = 0; i < pProvinceList.length; i++) {

        var bProvinceOption = $("<option/>");
        bProvinceOption.prop("value", pProvinceList[i].id);
        bProvinceOption.prop("text", pProvinceList[i].name);
        gProvinceSelectElement.append(bProvinceOption);

    };
}
//hàm đổ dữ liệu vào select quận huyện
function loadDataToSelectDistrict(pDistrictList) {
    gDistrictSelectElement.find('option').remove()
    gDistrictSelectElement.append('<option value=none>Chọn tỉnh quận huyện</option>')
    for (i = 0; i < pDistrictList.length; i++) {

        var bDistrictOption = $("<option/>");
        bDistrictOption.prop("value", pDistrictList[i].id);
        bDistrictOption.prop("text", pDistrictList[i].name);
        gDistrictSelectElement.append(bDistrictOption);

    };
}
//hàm đổ dữ liệu vào select phường xã
function loadDataToSelectWard(pWardList) {
    gWardSelectElement.find('option').remove()
    gWardSelectElement.append('<option value=none>Chọn phường xã</option>')

    for (i = 0; i < pWardList.length; i++) {

        var bWardOption = $("<option/>");
        bWardOption.prop("value", pWardList[i].id);
        bWardOption.prop("text", pWardList[i].name);
        gWardSelectElement.append(bWardOption);

    };
}
//hàm đổ dữ liệu vào select đường
function loadDataToSelectStreet(pStreetList) {
    gStreetelectElement.find('option').remove();
    gStreetelectElement.append('<option value=none>Chọn tỉnh đường</option>')
    for (i = 0; i < pStreetList.length; i++) {

        var bStreetOption = $("<option/>");
        bStreetOption.prop("value", pStreetList[i].id);
        bStreetOption.prop("text", pStreetList[i].name);
        gStreetelectElement.append(bStreetOption);

    };
}
function loadDataToSelectProject(pProjectList) {
    // gProjectSelectElement.find('option').remove()
    gProjectSelectElement.find('option').remove();
    gProjectSelectElement.append('<option value=none>Chọn dự án</option>')
    gProjectSelectElement.append('<option value=diffproject>Dự án khác</option>')
    for (i = 0; i < pProjectList.length; i++) {

        var bProjectOption = $("<option/>");
        bProjectOption.prop("value", pProjectList[i].id);
        bProjectOption.prop("text", pProjectList[i].name);
        gProjectSelectElement.append(bProjectOption);

    };
}


//hàm thực hiện khi click navbar
function changeFormDisplay() {
    $("#nav-item-1").on("click", function () {
        $("#add-realestate-1").attr("style", "display:block");
        $("#add-realestate-2").attr("style", "display:none");
        $("#add-realestate-3").attr("style", "display:none");
        $("#add-realestate-4").attr("style", "display:none");
        $("#add-realestate-5").attr("style", "display:none");
    })
    $("#nav-item-2").on("click", function () {

        $("#add-realestate-1").attr("style", "display:none");
        $("#add-realestate-2").attr("style", "display:block");
        $("#add-realestate-3").attr("style", "display:none");
        $("#add-realestate-4").attr("style", "display:none");
        $("#add-realestate-5").attr("style", "display:none");
    })
    $("#nav-item-3").on("click", function () {

        $("#add-realestate-1").attr("style", "display:none");
        $("#add-realestate-2").attr("style", "display:none");
        $("#add-realestate-3").attr("style", "display:block");
        $("#add-realestate-4").attr("style", "display:none");
        $("#add-realestate-5").attr("style", "display:none");
    })
    $("#nav-item-4").on("click", function () {

        $("#add-realestate-1").attr("style", "display:none");
        $("#add-realestate-2").attr("style", "display:none");
        $("#add-realestate-3").attr("style", "display:none");
        $("#add-realestate-4").attr("style", "display:block");
        $("#add-realestate-5").attr("style", "display:none");
    })
    $("#nav-item-5").on("click", function () {

        $("#add-realestate-1").attr("style", "display:none");
        $("#add-realestate-2").attr("style", "display:none");
        $("#add-realestate-3").attr("style", "display:none");
        $("#add-realestate-4").attr("style", "display:none");
        $("#add-realestate-5").attr("style", "display:block");
    })

}

