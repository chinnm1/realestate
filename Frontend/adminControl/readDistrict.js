
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gPROVINCE_COLS = ["id", "name", "prefix", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gPROVINCE_STT_COL = 0;
const gPROVINCE_NAME_COL = 1;
const gPROVINCE_PREFIX_COL = 2;
const gPROVINCE_ACTION_COL = 3;
var table = $("#district-table").DataTable({
    columns: [
        { data: gPROVINCE_COLS[gPROVINCE_STT_COL] },
        { data: gPROVINCE_COLS[gPROVINCE_NAME_COL] },
        { data: gPROVINCE_COLS[gPROVINCE_PREFIX_COL] },
        { data: gPROVINCE_COLS[gPROVINCE_ACTION_COL] },
    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gPROVINCE_ACTION_COL,
            defaultContent: `
            <i class="fas fa-edit edit-district"></i>
            <i class="fas fa-trash-alt delete-district"></i>
    `
        },

    ],
    scrollX: true,
    scrollCollapse: true,

    fixedColumns: true
})
//biến chứa hàng được chọn
var gRowSelected = ''
//biến chứa danh sách district
var glistDistrict = [];
//biến chứa ds province
var gProvinceList = [];
//biến chứa province id
var gProvinceIdSelected = '';
//biến chứa province select
var gProvinceSelectElement = $("#select-province");
//biến chứa district id
var gDistrictIdSelected = '';
//biến chứa hàng provicne
var gRowProvinceSelected = '';
//biến chứa list province update
var glistDistrictValidateUpdate = [];
//biến chứa user id
var gUserId = '';
//biến chứ header
var gHeader = ''
var gUserName = ''
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    const token = getCookie("token");
    const userId = getCookie("userId");
    const userName = getCookie('username')

    //Gọi API để lấy thông tin người dùng

    //Khai báo xác thực ở headers
    gHeader = {
        Authorization: "Token" + " " + token
    };
    //gán giá trị cho biến userId
    gUserId = Number(userId);
    gUserName = userName;
    console.log('CHECK USER ID', gUserId);
    console.log('CHECK HEADER', gHeader);
    console.log('CHECK USERNAME', gUserName);
    $('#username').html(gUserName)
    //tải trang
    onPageLoading().then((res) => {
        glistDistrict = res
        console.log('check data get all  from server', res);
        table.clear();
        table.rows.add(res);
        table.draw();
    }).catch((err) => {
        console.log(err)
    });
    //create new district
    $('#create-district').on('click', function () {
        onBtnAddDistrictClick();
    })
    //chọn thành phố
    $("#select-province").on('change', function () {
        onSelectProvinceChange();
    })

    $('#btn-create-district').on('click', function () {

        onBtnCreateDistrictOnModalClick();
    })
    //edit district
    $('#district-table').on('click', '.edit-district', function () {

        onBtnEditDistrictClick(this);
    })
    $('#btn-update-district').on('click', function () {
        onBtnUpdateOnModalClick()
    })

    //xóa district
    $('#district-table').on('click', '.delete-district', function () {
        onBtnDeleteDistrictClick(this);
    })
    $('#btn-confirm-delete-district').on('click', function () {

        onBtnConfirmOnDeleteModalClick(this);
    })
    //nhấn nút Logout
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })

})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm gọi api load danh sách province từ server khi tải trang
function onPageLoading() {
    console.log('làm tới đây')
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/district",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm thực hiện khi nhấn nút add new district
function onBtnAddDistrictClick() {
    $("#create-district-modal").modal("show");
    callApiToGetProvince().then((res) => {
        gProvinceList = res;
        loadDataToProvinceSelect(res)
    }).catch((err) => {
        console.log(err)
    });

}
//hàm thực hiện khi thay đổi select province
function onSelectProvinceChange() {
    if (gProvinceList && gProvinceList.length > 0) {
        gProvinceList.map(item => {
            if (item.id == $("#select-province option:selected").val()) {
                gProvinceIdSelected = item.id
            }
        })
    }
    console.log('chech province id selected', gProvinceIdSelected)
}
//hàm thực hiện khi nhấn create district trên modal
function onBtnCreateDistrictOnModalClick() {
    createNewDistrict();
    $("#create-district-modal").modal("hide");
}
//hàm thực hiện khi lúc nút edit
function onBtnEditDistrictClick(paramButton) {
    $("#update-district-modal").modal("show");
    gDistrictIdSelected = getDistrictIdFromButton(paramButton)
    console.log('check id district selected', gDistrictIdSelected)
    callApiToGetDistrictById(gDistrictIdSelected).then((res) => {
        console.log('check data customer need to update ress', res);
        showDataUpdateOnForm(res);
    }).catch((err) => {
        console.log(err)
    });
}
//hàm thực hiện khi nhấn nút update trên modal
function onBtnUpdateOnModalClick() {
    updateDistrict(gDistrictIdSelected);
    $("#update-district-modal").modal("hide");

}
//Hàm thực hiện khi nhấn nút delete
function onBtnDeleteDistrictClick(paramButton) {
    $("#delete-modal").modal("show");
    gDistrictIdSelected = getDistrictIdFromButton(paramButton)

}
//hàm thực hiện khi nhấn nút confirm trên delete modal
function onBtnConfirmOnDeleteModalClick() {
    deletedDistrict();
    $("#delete-modal").modal("hide");

}
//hàm thực hiện khi click nút log out 
function onBtnLogOutClick() {
    //
    redirectToLogin();

}



/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
//Hàm get Cookie 
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//hàm trả ra id district khi nhấn nút edit
function getDistrictIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    gRowProvinceSelected = table.row(vTableRow).data();
    console.log(gRowProvinceSelected.id);
    return gRowProvinceSelected.id;
}
//hàm call api để lấy ra  tỉnh thành phố
function callApiToGetProvince() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/province",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm đổ dữ liệu vào select district
function loadDataToProvinceSelect(pProvinceList) {
    gProvinceSelectElement.find('option').remove()
    gProvinceSelectElement.append('<option value=none>Chọn tỉnh thành phố</option>')
    for (i = 0; i < pProvinceList.length; i++) {
        var bProvinceOption = $("<option/>");
        bProvinceOption.prop("value", pProvinceList[i].id);
        bProvinceOption.prop("text", pProvinceList[i].name);
        gProvinceSelectElement.append(bProvinceOption);

    };
}
//hàm gọi api để lấy district cần edit theo id
function callApiToGetDistrictById(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/district/${id}`,
            async: false,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res);

            },
            error: function (err) {
                reject(e)
            }
        })
    })


}
//hàm hiển thị data lên form edit
function showDataUpdateOnForm(paramProvince) {
    $('#input-name-update').val(paramProvince.name);
    $('#input-prefix-update').val(paramProvince.prefix);

}
//update district
function updateDistrict(paramDistrictId) {
    let objData = {
        name: "",
        prefix: ""
    }
    let objDataFromForm = getDataOnFormUpdate(objData);
    let vCheck = validateDataOnForm(objData);
    if (vCheck) {
        callApiToUpdateData(paramDistrictId, objDataFromForm).then((res) => {
            console.log(res);
        }).catch((errr) => {
            console.log(errr)
        })
        onPageLoading();
    }


}

//hàm thu thập dư liệu trên form update
function getDataOnFormUpdate(obj) {

    obj.name = $('#input-name-update').val();
    obj.prefix = $('#input-prefix-update').val();

    return obj
}
//hàm gọi api để update district
function callApiToUpdateData(id, paramOrderObject) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/district/update/${id}`,
            type: 'PUT',
            async: false,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrderObject),
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(err)
            }
        })
    })

}
//save new district
function createNewDistrict() {
    let objData = {
        name: '',
        prefix: '',
    }
    let objDataFromForm = getDataOnForm(objData);
    var vCheck = validateDataOnForm(objDataFromForm);
    if (vCheck) {
        callApiToPostData(objDataFromForm, gProvinceIdSelected);
        onPageLoading();
    }

}
//hàm thu thập dư liệu trên form
function getDataOnForm(obj) {
    obj.name = $('#input-name').val();
    obj.prefix = $('#input-prefix').val();
    return obj
}
//hàm validate dữ liệu
function validateDataOnForm(paramObj) {
    if (paramObj.name == "") {
        alert("phải điền tên tỉnh");
        return false
    }
    if (paramObj.prefix == "") {
        alert("phải điền tên tỉnh");
        return false
    }
    if (paramObj.name == "") {
        alert("phải điền tên thành phố");
        return false
    }
    return true;
}
//hàm gọi api để tạo district mới
function callApiToPostData(paramOrderObject, id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/district/create/${id}`,
            type: 'POST',
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrderObject),
            headers: gHeader,
            success: function (res) {

                resolve(res)

            },
            error: function (error) {
                reject(err)
            }
        })
    })

}
//xóa district
function deletedDistrict() {
    callApiToDeleteData(gProvinceIdSelected);
    onPageLoading();
}
//gọi api xóa district
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/district/delete/${id}`,
        type: 'DELETE',
        async: false,
        headers: gHeader,
        success: function (res) {
            alert('Delete success')

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}




