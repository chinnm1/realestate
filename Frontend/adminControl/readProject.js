
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gPROJECT_COLS = ["id", "idOfProvince", "idOfDistrict", "idOfInvestor", "idOfConstruction", "idOfDesignUnit", "name", "acreage", "numBlock", "numFloor", "numApartment", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gPROJECT_ID_COL = 0;
const gPROJECT_PROVINCW_COL = 1;
const gPROJECT_DISTRICT_COL = 2;
const gPROJECT_INVESTOR_COL = 3;
const gPROJECT_CONSTRUCTOR_COL = 4;
const gPROJECT_DESIGNUNIT_COL = 5;
const gPROJECT_NAME_COL = 6;
const gPROJECT_AREA_COL = 7;
const gPROJECT_NUMBLOCK_COL = 8;
const gPROJECT_NUMFLOOR_COL = 9;
const gPROJECT_NUMAPART_COL = 10;
const gPROJECT_ACTION_COL = 11;



var table = $("#project-table").DataTable({
    columns: [
        { data: gPROJECT_COLS[gPROJECT_ID_COL] },
        { data: gPROJECT_COLS[gPROJECT_PROVINCW_COL] },
        { data: gPROJECT_COLS[gPROJECT_DISTRICT_COL] },
        { data: gPROJECT_COLS[gPROJECT_INVESTOR_COL] },
        { data: gPROJECT_COLS[gPROJECT_CONSTRUCTOR_COL] },
        { data: gPROJECT_COLS[gPROJECT_DESIGNUNIT_COL] },
        { data: gPROJECT_COLS[gPROJECT_NAME_COL] },
        { data: gPROJECT_COLS[gPROJECT_AREA_COL] },
        { data: gPROJECT_COLS[gPROJECT_NUMBLOCK_COL] },
        { data: gPROJECT_COLS[gPROJECT_NUMFLOOR_COL] },
        { data: gPROJECT_COLS[gPROJECT_NUMAPART_COL] },

        { data: gPROJECT_COLS[gPROJECT_ACTION_COL] },

    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gPROJECT_ACTION_COL,
            defaultContent: `
					<i class="fas fa-edit edit-project"></i>
					<i class="fas fa-trash-alt delete-project"></i>	`
        },

    ],
    scrollX: true,
    scrollCollapse: true,

    fixedColumns: true
})
//biến chứa hàng được chọn
var gRowSelected = ''
//biến chứa project id được chọn
var gProjectIdSelected = '';
//biến chứa hàng bđs được chọn
var gRowRealestateSelected = '';
//biến chứa ds province
var gListProvince = [];
//biến chứa ds district
var gListDistrict = [];
//biến chứa ds ward
var gListWard = [];
//biến chứa ds street
var gListStreet = [];
//biến chứa ds investor
var gListInvestor = [];
//biến chứa ds constructor
var gListConstructor = [];
//biến chứa ds design unit
var gListDesignUnit = [];
//biến chứa user id
var gUserId = '';
//biến chứ header
var gHeader = ''
var gUserName = ''
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    const token = getCookie("token");
    const userId = getCookie("userId");
    const userName = getCookie('username')

    //Gọi API để lấy thông tin người dùng

    //Khai báo xác thực ở headers
    gHeader = {
        Authorization: "Token" + " " + token
    };
    //gán giá trị cho biến userId
    gUserId = Number(userId);
    gUserName = userName;
    console.log('CHECK USER ID', gUserId);
    console.log('CHECK HEADER', gHeader);
    console.log('CHECK USERNAME', gUserName);
    $('#username').html(gUserName)
    //load trang
    onPageLoading().then((res) => {
        //load dữ liệu vào table
        loadDataToTable(res)
    }).catch((e) => {
        console.log(e)
    }
    );

    $('#create-project').on('click', function () {
        window.location.href = 'dataaddProject.html'
    })
    // //edit project
    $('#project-table').on('click', '.edit-project', function () {
        onBtnEditProjectClick(this);
    })

    // //edit project
    $('#project-table').on('click', '.edit-project', function () {
        onBtnEditProjectClick(this);
    })
    //xóa project
    $('#project-table').on('click', '.delete-project', function () {
        onBtnDeleteProjectClick(this);
    })
    $('#btn-confirm-delete-order').on('click', function () {
        onBtnConfirmOnDeleteModalClick();
    })
    //nhấn nút Logout
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })

})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm gọi api load danh sách project từ server khi tải trang
function onPageLoading() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/project",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (err) {
                reject(e);
            }
        })

    })

}

//hàm thực hiện khi lúc nút edit
function onBtnEditProjectClick(paramButton) {
    gProjectIdSelected = getProjectIdFromButton(paramButton)
    console.log('check id customer selected', gProjectIdSelected)
    window.location.href = `dataeditProject.html?id=${gProjectIdSelected}`
    // callApiToGetCustomerById(gProjectIdSelected)
}

//Hàm thực hiện khi nhấn nút delete
function onBtnDeleteProjectClick(paramButton) {
    $("#delete-modal").modal("show");
    gProjectIdSelected = getProjectIdFromButton(paramButton)

}
//hàm thực hiện khi nhấn nút confirm trên delete modal
function onBtnConfirmOnDeleteModalClick() {
    deletedProject();
    $("#delete-modal").modal("hide");

}
//hàm thực hiện khi click nút log out 
function onBtnLogOutClick() {
    //
    redirectToLogin();

}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
//Hàm get Cookie 

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//hàm load data to table
function loadDataToTable(paramList) {
    //gọi api lấy ra province
    callApiToGetProvince().then((res) => {
        gListProvince = res;
        console.log("CHECK P", gListProvince)
    }).catch((e) => {
        console.log(e)
    });
    //gọi api lấy ra investor
    callApiToGetInvestor().then((res) => {
        gListInvestor = res;
        console.log("CHECK I", gListInvestor)
    }).catch((e) => {
        console.log(e)
    }
    );
    //gọi api lấy ra constructor
    callApiToGetConstructor().then((res) => {
        gListConstructor = res;
        console.log("CHECK C", gListConstructor)
    }).catch((e) => {
        console.log(e)
    }
    );
    //gọi api lấy ra design unit
    callApiToGetDesignUnit().then((res) => {
        gListDesignUnit = res;
        console.log("CHECK D", gListDesignUnit)
    }).catch((e) => {
        console.log(e)
    }
    );
    //gọi api lấy ra district
    callApiTogetDistrict().then((res) => {
        gListDistrict = res;
        console.log("CHECK D", gListDistrict);
        //chỉnh sửa data project điền vào table
        customizeData(paramList);
    }).catch((e) => {
        console.log(e)
    }
    );


}
//hàm xử lí data điền table
function customizeData(paramList) {
    if (gListProvince.length > 0 && gListDistrict.length > 0) {
        paramList.map(item => {
            console.log('làm tới đây');
            for (let i = 0; i < gListProvince.length; i++) {
                if (item.idOfProvince == gListProvince[i].id) {
                    item.idOfProvince = gListProvince[i].name
                }
            }
            for (let i = 0; i < gListDistrict.length; i++) {
                if (item.idOfDistrict == gListDistrict[i].id) {
                    item.idOfDistrict = gListDistrict[i].name
                }
            }
            for (let i = 0; i < gListInvestor.length; i++) {
                if (item.idOfInvestor == gListInvestor[i].id) {
                    item.idOfInvestor = gListInvestor[i].name
                }
            }

            for (let i = 0; i < gListConstructor.length; i++) {
                if (item.idOfConstruction == gListConstructor[i].id) {
                    item.idOfConstruction = gListConstructor[i].name
                }
            }

            for (let i = 0; i < gListDesignUnit.length; i++) {
                if (item.idOfDesignUnit == gListDesignUnit[i].id) {
                    item.idOfDesignUnit = gListDesignUnit[i].name
                }
            }
            return item
        })
        //hiển thị data lên table
        customizeTable(paramList);
        console.log('CHECK RESULT', paramList)
    }

}
//hàm custom lại bảng project
function customizeTable(data) {
    table.clear();
    table.rows.add(data);
    table.draw();

}
//hàm gọi api để lấy province
function callApiToGetProvince() {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/province`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hàm gọi api để lấy investor
function callApiToGetInvestor() {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/investor`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hàm gọi api để lấy constructor
function callApiToGetConstructor() {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/constructor`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hàm gọi api để lấy constructor
function callApiToGetDesignUnit() {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/designunit`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hàm gọi api để lấy province
function callApiTogetDistrict() {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/district`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hàm gọi api để lấy ward

//hàm trả ra id album khi nhấn nút edit
function getProjectIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    gRowRealestateSelected = table.row(vTableRow).data();
    console.log(gRowRealestateSelected.id);
    return gRowRealestateSelected.id;
}
//hàm thực hiện xóa project
function deletedProject() {
    callApiToDeleteData(gProjectIdSelected);
    onPageLoading();
}
//hàm gọi api để xóa project
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/project/delete/${id}`,
        type: 'DELETE',
        async: false,
        headers: gHeader,
        success: function (res) {
            alert('Delete success')
        },
        error: function (xhr, status, error) {
            if (xhr.responseJSON.message == "Forbidden") {
                alert("Bạn không có đủ quyền hạn để thực hiện chức năng này")
            } else {
                console.log(xhr, status, error);

            }
        }
    })
}




