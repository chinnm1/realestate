
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var type = [
    { key: 0, val: "Đất" },
    { key: 1, val: "Nhà" },
    { key: 2, val: "Căn hộ" },
    { key: 3, val: "Văn phòng" },
    { key: 4, val: "Kinh doanh" },
    { key: 5, val: "Phòng trọ" },
]
var request = [
    { key: 0, val: "Cần bán" },
    { key: 1, val: "Cần mua" },
    { key: 2, val: "Cho thuê" },
    { key: 3, val: "Cần thuê" },

]
var priceTime = [
    { key: 0, val: "Bán thường" },
    { key: 1, val: "Bán nhanh 24h" },
    { key: 2, val: "Bán nhanh 72h" },
    { key: 3, val: "Bán nhanh 1 tuần" },

]

// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gREALESTATE_COLS = ["action", "id", "idOfProvince", "idOfDistrict", "type", "createdBy", "price", "acreage", "numberFloor", "priceTime", "createdAt", "status"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gREALESTATE_ACTION_COL = 0;
const gREALESTATE_ID_COL = 1;
const gREALESTATE_PROVINCW_COL = 2;
const gREALESTATE_DISTRICT_COL = 3;
const gREALESTATE_TYPE_COL = 4;
const gREALESTATE_CREATEDBY_COL = 5;
const gREALESTATE_PRICE_COL = 6;
const gREALESTATE_AREA_COL = 7;
const gREALESTATE_FLOOR_COL = 8;
const gREALESTATE_PRICE_TIME_COL = 9;
const gREALESTATE_CREATAT = 10;
const gREALESTATE_STATUS = 11;


//khai báo bảng
var table = $("#realestate-table").removeAttr('width').DataTable({
    bAutoWidth: false,
    columns: [
        { data: gREALESTATE_COLS[gREALESTATE_ACTION_COL] },
        { data: gREALESTATE_COLS[gREALESTATE_ID_COL] },
        { data: gREALESTATE_COLS[gREALESTATE_PROVINCW_COL] },
        { data: gREALESTATE_COLS[gREALESTATE_DISTRICT_COL] },
        { data: gREALESTATE_COLS[gREALESTATE_TYPE_COL] },
        { data: gREALESTATE_COLS[gREALESTATE_CREATEDBY_COL] },
        { data: gREALESTATE_COLS[gREALESTATE_PRICE_COL] },
        { data: gREALESTATE_COLS[gREALESTATE_AREA_COL] },
        { data: gREALESTATE_COLS[gREALESTATE_FLOOR_COL] },
        { data: gREALESTATE_COLS[gREALESTATE_PRICE_TIME_COL] },
        { data: gREALESTATE_COLS[gREALESTATE_CREATAT] },
        { data: gREALESTATE_COLS[gREALESTATE_STATUS] },



    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gREALESTATE_ACTION_COL,
            defaultContent: `
					<i class="fas fa-edit edit-realestate"></i>
					<i class="fas fa-trash-alt delete-realestate"></i>
                    <i class="fas fa-check-circle confirm-realestate"></i>	`
        },

    ],

    scrollX: true,
    scrollCollapse: true,


    fixedColumns: true
})
//biến chứa hàng được chọn
var gRowSelected = ''

//biến chứa id bđs được chọn
var gRelestateIdSelected = '';
//biến chứa hàng bđs được chọn
var gRowRealestateSelected = '';
//biến chứa ds tỉnh thành phố
var gListProvince = [];
//biến chứa ds quận huyện
var gListDistrict = [];
//biến chứa ds phường xã
var gListWard = [];
//biến chứa ds đường
var gListStreet = [];
//biến chứa user id
var gUserId = '';
//biến chứ header
var gHeader = ''
var gUserName = ''
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {

    const token = getCookie("token");
    const userId = getCookie("userId");
    const userName = getCookie('username')

    //Gọi API để lấy thông tin người dùng

    //Khai báo xác thực ở headers
    gHeader = {
        Authorization: "Token" + " " + token
    };
    //gán giá trị cho biến userId
    gUserId = Number(userId);
    gUserName = userName;
    console.log('CHECK USER ID', gUserId);
    console.log('CHECK HEADER', gHeader);
    console.log('CHECK USERNAME', gUserName);
    $('#username').html(gUserName)

    //load trang
    onPageLoading().then((res) => {
        loadDataToTable(res)
    }).catch((e) => {
        console.log(e)
    }
    );
    //nhấn nút confirm status
    $('#realestate-table').on('click', '.confirm-realestate', function () {
        onBtnConfirmRealestateClick(this);
    })
    //nhấn nút confirm status trên modal
    $('#btn-confirm-status').on('click', function () {
        onBtnConfirmRealestateClickOnModal();
    })
    //nhấn nút cancel status trên modal
    $('#btn-cancel-status').on('click', function () {
        onBtnCancelRealestateClickOnModal();
    })

    //nhấn nút edit
    $('#realestate-table').on('click', '.edit-realestate', function () {
        onBtnEditRealestateClick(this);
    })
    //nhấn nút xóa
    $('#realestate-table').on('click', '.delete-realestate', function () {
        onBtnDeleteRealestateClick(this);
    })
    //nhấn nút confirm trên modal
    $('#btn-confirm-delete-order').on('click', function () {
        onBtnConfirmOnDeleteModalClick();
    })
    //nhấn nút add realestate
    $('#create-realestate').on('click', function () {
        onBtnAddClick();
    })
    //nhấn nút Logout
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })

})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm gọi api load danh sách bđs từ server khi tải trang
function onPageLoading() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/realestate",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (err) {
                reject(e);
            }
        })

    })

}
//hàm thực hiện khi lúc nút confirm
//INPUT: nút confirm
function onBtnConfirmRealestateClick(paramButton) {
    $("#confirm-status-modal").modal("show");
    gRelestateIdSelected = getRealestateIdFromButton(paramButton)
    console.log('check id customer selected', gRelestateIdSelected)
    // window.location.href = `dataeditRealestate.html?id=${gRelestateIdSelected}`

}
//hàm click nút confirm trên modal review status
function onBtnConfirmRealestateClickOnModal() {
    let obj = {
        status: "confirm"
    }
    //gọi api để update status của bđs
    callApiToUpdateStatus(obj, gRelestateIdSelected).then(res => {
        alert("Update status succeed !")
        $("#confirm-status-modal").modal("hide");
        console.log('check STATUS', res);
        onPageLoading().then((res) => {
            loadDataToTable(res)
        }).catch((e) => {
            console.log(e)
        }
        );
    }).catch(e => {
        console.log(e)
    });
}
//hàm click nút cancel trên modal review status
function onBtnCancelRealestateClickOnModal() {
    let obj = {
        status: "cancel"
    }
    //gọi api để update status của bđs
    callApiToUpdateStatus(obj, gRelestateIdSelected).then(res => {
        alert("Update status succeed !")
        $("#confirm-status-modal").modal("hide");
        console.log('check STATUS', res);
        onPageLoading().then((res) => {
            loadDataToTable(res)
        }).catch((e) => {
            console.log(e)
        }
        );
    }).catch(e => {
        console.log(e)
    });
}


//hàm thực hiện khi lúc nút edit
//INPUT: nút edit
function onBtnEditRealestateClick(paramButton) {
    $("#update-customer-modal").modal("show");
    gRelestateIdSelected = getRealestateIdFromButton(paramButton)
    console.log('check id customer selected', gRelestateIdSelected)
    window.location.href = `dataeditRealestate.html?id=${gRelestateIdSelected}`

}

//Hàm thực hiện khi nhấn nút delete
//INPUT: nút delete
function onBtnDeleteRealestateClick(paramButton) {
    $("#delete-modal").modal("show");
    gRelestateIdSelected = getRealestateIdFromButton(paramButton)

}
//hàm thực hiện khi nhấn nút confirm trên delete modal
function onBtnConfirmOnDeleteModalClick() {
    deletedRealestate();
    $("#delete-modal").modal("hide");

}
//hàm thực hiện nút add
function onBtnAddClick() {
    window.location.href = 'dataaddRealestate.html'
}
//hàm thực hiện khi click nút log out 
function onBtnLogOutClick() {
    //
    redirectToLogin();

}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

//Hàm get Cookie 
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//hàm load data to table
//INPUT: mảng bđs
function loadDataToTable(paramList) {
    //gọi api để lấy ra ds tỉnh thành phố
    callApiToGetProvince().then((res) => {
        gListProvince = res;
    }).catch((e) => {
        console.log(e)
    }
    );
    //gọi api để lấy ra ds quận huyện
    callApiTogetDistrict().then((res) => {
        gListDistrict = res;
        //hàm xử lí data cho bảng
        customizeData(paramList);
    }).catch((e) => {
        console.log(e)
    }
    );




}
//hàm xử lí data cho bảng
function customizeData(paramList) {
    if (gListProvince.length > 0 && gListDistrict.length > 0) {
        paramList.map(item => {
            for (let i = 0; i < type.length; i++) {
                if (item.type == type[i].key) {
                    item.type = type[i].val
                }
            }
            for (let i = 0; i < request.length; i++) {
                if (item.request == request[i].key) {
                    item.request = request[i].val
                }
            }
            for (let i = 0; i < priceTime.length; i++) {
                if (item.priceTime == priceTime[i].key) {
                    item.priceTime = priceTime[i].val
                }
            }
            for (let i = 0; i < gListProvince.length; i++) {
                if (item.idOfProvince == gListProvince[i].id) {
                    item.idOfProvince = gListProvince[i].name
                }
            }
            for (let i = 0; i < gListDistrict.length; i++) {
                if (item.idOfDistrict == gListDistrict[i].id) {
                    item.idOfDistrict = gListDistrict[i].name
                }
            }
            return item
        })
        //hàm hiển thị data ra bảng
        customizeTable(paramList);
    }

}
//hàm hiển thị data lên bảng
function customizeTable(data) {
    table.clear();
    table.rows.add(data);
    table.draw();


}
//hàm gọi api để lấy province
function callApiToGetProvince() {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/province`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hàm gọi api để lấy district
function callApiTogetDistrict() {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/district`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hàm gọi api để lấy ward
function callApiTogetWard() {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/ward`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hàm gọi api để lấy street
function callApiTogetStreet() {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/street`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hàm gọi api để update status
function callApiToUpdateStatus(paramObj, paramRealestateId) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/realestate/update_status/${paramRealestateId}`,
            type: 'PUT',
            async: false,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramObj),
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (xhr, status, error) {
                reject(xhr, status, error)
            }
        })
    })

}
//hàm trả ra id bđs khi nhấn nút edit
function getRealestateIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    gRowRealestateSelected = table.row(vTableRow).data();
    console.log(gRowRealestateSelected.id);
    return gRowRealestateSelected.id;
}
//hàm thực hiện khi nhấn nút xóa
function deletedRealestate() {
    callApiToDeleteData(gRelestateIdSelected);
    onPageLoading();
}
//hàm gọi api xóa bđs theo id
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/realestate/delete/${id}`,
        type: 'DELETE',
        async: false,
        headers: gHeader,
        success: function (res) {
            alert('Delete success')

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}




