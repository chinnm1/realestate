//Biến chứa id bđs
var gRealestateId = '';
//biến chứa ds province
var gProvinceList = {};
//biến chứa element select province
var gProvinceSelectElement = $("#select-province");
//biến chứa id province được chọn
var gProvinceIdSelectedd = ''
//biến chứa element select district
var gDistrictSelectElement = $("#select-district");
//biến chứa ds district
var gDistrictList = {};
//biến chứa id district được chọn
var gDistrictIdSelectedd = ''
//biến chứa element select ward
var gWardSelectElement = $("#select-ward");
//biến chứa ds ward
var gWardList = {};
//biến chứa id ward được chọn
var gWardIdSelectedd = ''
//biến chứa element select street
var gStreetelectElement = $("#select-street");
//biến chứa ds street
var gStreetList = {};
//biến chứa id street được chọn
var gStreetIdSelectedd = ''
//biến chứa element select project
var gProjectSelectElement = $("#select-project");
//biến chứa ds project
var gProjectList = {};
//biến chứa id project được chọn
var gProjectIdSelectedd = ''
//biến chứa user id
var gUserId = '';
//biến chứ header
var gHeader = ''
var gUserName = ''
$(document).ready(function () {
    const token = getCookie("token");
    const userId = getCookie("userId");
    const userName = getCookie('username')
    //Gọi API để lấy thông tin người dùng

    //Khai báo xác thực ở headers
    gHeader = {
        Authorization: "Token" + " " + token
    };
    //gán giá trị cho biến userId
    gUserId = Number(userId);
    gUserName = userName;
    console.log('CHECK USER ID', gUserId);
    console.log('CHECK HEADER', gHeader);
    console.log('CHECK USERNAME', gUserName);
    $('#username').html(gUserName)

    //hàm hiện thị form eit đầu tiên khi load trang
    $("#add-realestate-1").attr("style", "display:block");
    //thay đối hiện thị khi click navbar
    changeFormDisplay()
    //Lấy id trên url
    getIdFromUrl();
    //CHọn tỉnh thành phố
    $("#select-province").on('change', function () {
        onSelectProvinceChange();
    })
    //CHọn quận huyện
    $("#select-district").on('change', function () {
        onSelectDistrictChange();
    })
    //CHọn phường xã
    $("#select-ward").on('change', function () {
        onSelectWardChange();
    })
    //CHọn đường
    $("#select-street").on('change', function () {
        onSelectStreetChange();
    })
    //thêm bất động sản mới
    $("#btn-edit").on('click', function () {
        onBtnEditClick();
    })
    //nhấn nút Logout
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })


})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm thực hiện khi click nút log out 
function onBtnLogOutClick() {
    //
    redirectToLogin();

}
//hàm lấy id trên url
function getIdFromUrl() {
    var url_string = window.location.href;
    var url = new URL(url_string);
    gRealestateId = url.searchParams.get("id");
    if (gRealestateId != "") {
        //gọi api để lấy bđs theo id
        callApiToGetRealestate(gRealestateId).then((res) => {
            console.log('check realestate', res)
            loadDataToForm(res);
        }).catch((e) => {
            console.log(e)
        }
        );
    }

}
//hàm thực hiện khi thay đổi select province
function onSelectProvinceChange() {
    if (gProvinceList && gProvinceList.length > 0) {
        gProvinceList.map(item => {
            if (item.id == $("#select-province option:selected").val()) {
                gProvinceIdSelectedd = item.id
            }
        })
    }
    console.log('CHECK PROVINCE ID SELECTED', gProvinceIdSelectedd)
    //gọi api để lấy district theo provinceid
    callApiToGetDistrictByProvinceId().then((res) => {
        gDistrictList = res;
        loadDataToSelectDistrict(res);
    }).catch((e) => {
        console.log(e)
    }
    );
}
//hàm thực hiện khi thay đổi select district
function onSelectDistrictChange() {
    if (gDistrictList && gDistrictList.length > 0) {
        gDistrictList.map(item => {
            if (item.id == $("#select-district option:selected").val()) {
                gDistrictIdSelectedd = item.id
            }
        })
    }
    console.log('chech district id selected', gDistrictIdSelectedd)
    //gọi api để lấy ward theo district id
    callApiToGetWardByDistrictId().then((res) => {
        gWardList = res;
        //hiển thị dữ liệu lên ô select ward
        loadDataToSelectWard(res);
    }).catch((e) => {
        console.log(e)
    }
    );
    //gọi api để lấy ds đường theo district id và province id
    callApiToGetStreetByProvinceIdAndDistrictId().then((res) => {
        gStreetList = res;
        loadDataToSelectStreet(res);
    }).catch((e) => {
        console.log(e)
    }
    );;

}
//hàm thực hiện khi nhấn nút add
function onBtnEditClick() {
    //B1:tạo đối tượng chứa thông
    let realEstate = {
        title: "",
        type: 0,
        request: 0,
        address: "",
        price: 0,
        priceMin: 0,
        priceTime: 0,
        acreage: 0,
        direction: "",
        numberFloor: 0,
        bath: 0,
        apartCode: "",
        wallArea: 0,
        bedroom: 0,
        balcony: 0,
        landscapeView: "",
        apartLoca: 0,
        apartType: 0,
        furnitureType: 0,
        priceRent: 0,
        returnRate: 0,
        legalDoc: 0,
        description: "",
        Width: 0,
        Long: 0,
        viewNum: 0,
        shape: "",
        distance2facade: 0,
        adjacentFacadeNum: 0,
        adjacentRoad: "",
        alleyMinWidth: 0,
        adjacentAlleyMinWidth: 0,
        factor: "",
        structure: "",
        dtsxd: 0,
        clcl: 0,
        ctxdPrice: 0,
        ctxdValue: 0,
        photo: "",
        lat: 0,
        lng: 0,
        idOfProvinceFromClient: -1,
        idOfDistrictFromClient: -1,
        idOfWardFromClient: -1,
        idOfStreetFromClient: -1,
        idOfProjectFromClient: -1
    }
    //B2 thu thập dữ liệu
    getDataFromForm(realEstate);

    //B3 validate dữ liệu
    let vCheck = validateData(realEstate);
    if (vCheck) {
        //B4 gọi api để lưu dữ liệu
        callApiToPutDataToServer(realEstate, gRealestateId);
    }
}
//hàm thực hiện khi thay đổi select ward
function onSelectWardChange() {
    if (gWardList && gWardList.length > 0) {
        gWardList.map(item => {
            if (item.id == $("#select-ward option:selected").val()) {
                gWardIdSelectedd = item.id
            }
        })
    }



}
//hàm thực hiện khi thay đổi select street
function onSelectStreetChange() {
    if (gStreetList && gStreetList.length > 0) {
        gStreetList.map(item => {
            if (item.id == $("#select-street option:selected").val()) {
                gStreetIdSelectedd = item.id
            }
        })
    }
    console.log('chech street id selected', gStreetIdSelectedd)
    //gọi api để lấy ds project theo province,district,ward,street id
    callApiToGetProjectByProvinceDistrictWardStreet().then((res) => {
        gProjectList = res;
        loadDataToSelectProject(res);
    }).catch((e) => {
        console.log(e)
    }
    );;

}


/*** REGION 4- COmmon function - Vùng khai báo các hàm dùng chung*/
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//hàm hiển thi data lên form
function loadDataToForm(paramObject) {
    $("#input-title").val(paramObject.title);
    $("#select-type").val(paramObject.type);
    $("#select-request").val(paramObject.request);
    $("#input-address").val(paramObject.address);
    $("#input-price").val(paramObject.price);
    $("#input-price-min").val(paramObject.priceMin);
    $("#select-price-time").val(paramObject.priceTime);
    $("#input-area").val(paramObject.acreage);
    $("#input-direction").val(paramObject.direction);
    $("#input-number-floor").val(paramObject.numberFloor);
    $("#input-bath").val(paramObject.bath);
    $("#input-apart-code").val(paramObject.apartCode);
    $("#input-wall-area").val(paramObject.wallArea);
    $("#input-bedroom").val(paramObject.bedroom);
    $("#select-balcony").val(paramObject.balcony);
    $("#input-landscape").val(paramObject.landscapeView);
    $("#select-apart-loca").val(paramObject.apartLoca);
    $("#select-apart-type").val(paramObject.apartType);
    $("#select-furniture-type").val(paramObject.furnitureType);
    $("#input-price-rent").val(paramObject.priceRent);
    $("#input-return-rate").val(paramObject.returnRate);
    $("#input-legal-docs").val(paramObject.legalDoc);
    $("#input-description").val(paramObject.description);
    $("#input-width").val(paramObject.Width);
    $("#input-long").val(paramObject.Long);
    $("#input-view-num").val(paramObject.viewNum);
    $("#input-shape").val(paramObject.shape);
    $("#input-distance2facade").val(paramObject.distance2facade);
    $("#input-adjacent-facade-num").val(paramObject.adjacentFacadeNum);
    $("#input-adjacent-road").val(paramObject.adjacentRoad);
    $("#input-alley-min-width").val(paramObject.alleyMinWidth);
    $("#input-adjacent-alley-min-width").val(paramObject.adjacentAlleyMinWidth);
    $("#input-factor").val(paramObject.factor);
    $("#input-structure").val(paramObject.structure);
    $("#input-dtsxd").val(paramObject.dtsxd);
    $("#input-clcl").val(paramObject.clcl);
    $("#input-ctxd-price").val(paramObject.ctxdPrice);
    $("#input-ctxd-value").val(paramObject.ctxdValue);

    $("#input-lat").val(paramObject.lat);
    $("#input-lng").val(paramObject.lng);
    //hiển thị dữ liệu lên select province
    loadDataOfRealestateToSelectProvince(paramObject.idOfProvince)
    //hiển thị dữ liệu lên select district
    loadDataOfRealestateToSelectDistrict(paramObject.idOfDistrict)
    //hiển thị dữ liệu lên select ward
    loadDataOfRealestateToSelectWard(paramObject.idOfWard)
    //hiển thị dữ liệu lên select street
    loadDataOfRealestateToSelectStreet(paramObject.idOfStreet)
    if (paramObject.idOfProject != null) {
        //hiển thị dữ liệu lên select project
        loadDataOfRealestateToSelectProject(paramObject.idOfProject)
    }

}
//Hàm hiển thị data lên select province
function loadDataOfRealestateToSelectProvince(paramId) {
    //gọi api để lấy ds province
    callApiToGetProvinceFromServer().then((res) => {
        gProvinceList = res;
        console.log('CHECK PROVINCE', res)
        //hiển thị province
        displayOnProvinceSelect(res, paramId);
    }).catch((e) => {
        console.log(e)
    }
    );

}
//Hàm hiển thị data lên select district
function loadDataOfRealestateToSelectDistrict(paramId) {
    //gọi api để lấy ds district
    callApiToGetDistrictFromServer(paramId).then((res) => {
        //hiển thị district
        displayOnDistrictSelect(res);
    }).catch((e) => {
        console.log(e)
    }
    );

}
//Hàm hiển thị data lên select ward
function loadDataOfRealestateToSelectWard(paramId) {
    //gọi api để lấy ds ward
    callApiToGetWardFromServer(paramId).then((res) => {
        //hiển thị ward
        displayOnWardSelect(res);
    }).catch((e) => {
        console.log(e)
    }
    );

}
//Hàm hiển thị data lên select street
function loadDataOfRealestateToSelectStreet(paramId) {
    //Hàm hiển thị data lên select đường
    callApiToGetStreetFromServer(paramId).then((res) => {

        //hiển thị đường
        displayOnStreetSelect(res);
    }).catch((e) => {
        console.log(e)
    }
    );


}
//Hàm hiển thị data lên select project
function loadDataOfRealestateToSelectProject(paramId) {
    //Hàm hiển thị data lên select project
    callApiToGetProjectFromServer(paramId).then((res) => {

        //hiển thị project
        displayOnProjectSelect(res);
    }).catch((e) => {
        console.log(e)
    }
    );


}
//hàm gọi api để lấy project theo id
function callApiToGetProjectFromServer(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/project/detail/${id}`,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })
}

//hàm gọi api để lấy streettheo id
function callApiToGetStreetFromServer(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/street/${id}`,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })
}

//hàm gọi api để lấy ward theo id
function callApiToGetWardFromServer(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/ward/${id}`,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })
}
//hàm gọi api để lấy district theo id
function callApiToGetDistrictFromServer(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/district/${id}`,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })
}
//hiển thị district lên ô select
function displayOnDistrictSelect(paramDistrict) {
    $("#select-district").append(`<option selected value=${paramDistrict.id}>${paramDistrict.name}</option>`)

}
//hiển thị ward lên ô select
function displayOnWardSelect(paramWard) {
    $("#select-ward").append(`<option selected value=${paramWard.id}>${paramWard.name}</option>`)

}
//hiển thị street lên ô select
function displayOnStreetSelect(paramStreet) {
    $("#select-street").append(`<option selected value=${paramStreet.id}>${paramStreet.name}</option>`)

}
//hiển thị street lên ô project
function displayOnProjectSelect(paramProject) {
    $("#select-project").append(`<option selected value=${paramProject.id}>${paramProject.name}</option>`)

}
//hàm gọi api để lấy province theo id
function callApiToGetProvinceFromServer() {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/province`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hàm hiển province của bđs lên ô select
function displayOnProvinceSelect(paramProvince, id) {
    console.log('CHECK PARAMPROVINCE', paramProvince)
    for (let i = 0; i < paramProvince.length; i++) {
        $("#select-province").append(`<option value=${paramProvince[i].id}>${paramProvince[i].name}</option>`)
    }
    paramProvince.map(item => {
        paramProvince.map(item => {
            if (item.id == id) {
                $("#select-province").val(item.id)
            }

        })
    })


}

//Hàm thu thập dữ liệu trên form
function getDataFromForm(paramObject) {
    paramObject.title = $("#input-title").val();
    paramObject.type = $("#select-type").val();
    paramObject.request = $("#select-request").val();
    paramObject.address = $("#input-address").val();
    paramObject.price = $("#input-price").val();
    paramObject.priceMin = $("#input-price-min").val();
    paramObject.priceTime = $("#select-price-time").val();
    paramObject.acreage = $("#input-area").val();
    paramObject.direction = $("#input-direction").val();
    paramObject.numberFloor = $("#input-number-floor").val();
    paramObject.bath = $("#input-bath").val();
    paramObject.apartCode = $("#input-apart-code").val();
    paramObject.wallArea = $("#input-wall-area").val();
    paramObject.bedroom = $("#input-bedroom").val();
    paramObject.balcony = $("#select-balcony").val();
    paramObject.landscapeView = $("#input-landscape").val();
    paramObject.apartLoca = $("#select-apart-loca").val();
    paramObject.apartType = $("#select-apart-type").val();
    paramObject.furnitureType = $("#select-furniture-type").val();
    paramObject.priceRent = $("#input-price-rent").val();
    paramObject.returnRate = $("#input-return-rate").val();
    paramObject.legalDoc = $("#input-legal-docs").val();
    paramObject.description = $("#input-description").val();
    paramObject.Width = $("#input-width").val();
    paramObject.Long = $("#input-long").val();
    paramObject.viewNum = $("#input-view-num").val();
    paramObject.shape = $("#input-shape").val();
    paramObject.distance2facade = $("#input-distance2facade").val();
    paramObject.adjacentFacadeNum = $("#input-adjacent-facade-num").val();
    paramObject.adjacentRoad = $("#input-adjacent-road").val();
    paramObject.alleyMinWidth = $("#input-alley-min-width").val();
    paramObject.adjacentAlleyMinWidth = $("#input-adjacent-alley-min-width").val();
    paramObject.factor = $("#input-factor").val();
    paramObject.structure = $("#input-structure").val();
    paramObject.dtsxd = $("#input-dtsxd").val();
    paramObject.clcl = $("#input-clcl").val();
    paramObject.ctxdPrice = $("#input-ctxd-price").val();
    paramObject.ctxdValue = $("#input-ctxd-value").val();
    paramObject.photo = "";
    paramObject.lat = $("#input-lat").val();
    paramObject.lng = $("#input-lng").val();
    paramObject.idOfProvinceFromClient = $("#select-province").val();
    paramObject.idOfDistrictFromClient = $("#select-district").val();
    paramObject.idOfWardFromClient = $("#select-ward").val();
    paramObject.idOfStreetFromClient = $("#select-street").val();
    paramObject.idOfProjectFromClient = $("#select-project").val();
    return paramObject;


}

//hàm check validate data
function validateData(paramObject) {
    if (paramObject.idOfProvinceFromClient == -1) {
        alert("Phải chọn tỉnh thành phố")
        return false;
    }
    if (paramObject.idOfDistrictFromClient == -1) {
        alert("Phải chọn quận huyện")
        return false;

    }
    if (paramObject.idOfWardFromClient == -1) {
        alert("Phải chọn phường xã")
        return false;

    }
    if (paramObject.idOfStreetFromClient == -1) {
        alert("Phải chọn đường")
        return false;

    }
    if (paramObject.address == "") {
        alert("Phải điền địa chỉ")
        return false;

    }
    if (paramObject.idOfProjectFromClient == "diffOption" || paramObject.idOfProjectFromClient == "none") {
        paramObject.idOfProjectFromClient = 0;

    }
    return true;

}
//hàm gọi api để lưu dữ liệu
function callApiToPutDataToServer(paramObject, paramId) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/realestate/update/${paramId}`,
            type: 'PUT',
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramObject),
            headers: gHeader,
            success: function (res) {
                alert("Edit success");
                window.location.href = `datareadRealestate.html`
            },
            error: function (error) {
                console.assert(error.responseText)
            }
        })

    })

}
//hàm gọi api để lưu dữ liệu
function callApiToGetRealestate(paramId) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/realestate/detail/${paramId}`,
            type: 'GET',
            dataType: 'json',

            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}

//hàm call api để lấy ra các quận huyện theo tỉnh thành phố
function callApiToGetDistrictByProvinceId() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/district/province/${gProvinceIdSelectedd}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })
}
//hàm call api để lấy ra các phường xã theo quận huyện
function callApiToGetWardByDistrictId() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/ward/district/${gDistrictIdSelectedd}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })
}
//hàm call api để lấy ra  tỉnh thành phố
function callApiToGetProvince() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/province",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm call api để lấy ra đường theo tỉnh quận
function callApiToGetStreetByProvinceIdAndDistrictId() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/street/${gDistrictIdSelectedd}/${gProvinceIdSelectedd}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm call api để lấy ra project
function callApiToGetProjectByProvinceDistrictWardStreet() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/project/${gProvinceIdSelectedd}/${gDistrictIdSelectedd}/${gWardIdSelectedd}/${gStreetIdSelectedd}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}


//hàm đổ dữ liệu vào select tỉnh thành phố
function loadDataToProvinceSelect(pProvinceList) {
    gProvinceSelectElement.find('option').remove()
    gProvinceSelectElement.append('<option value=none>Chọn tỉnh thành phố</option>')
    for (i = 0; i < pProvinceList.length; i++) {
        var bProvinceOption = $("<option/>");
        bProvinceOption.prop("value", pProvinceList[i].id);
        bProvinceOption.prop("text", pProvinceList[i].name);
        gProvinceSelectElement.append(bProvinceOption);

    };
}
//hàm đổ dữ liệu vào select quận huyện
function loadDataToSelectDistrict(pDistrictList) {
    gDistrictSelectElement.find('option').remove()
    gDistrictSelectElement.append('<option value=none>Chọn tỉnh quận huyện</option>')
    for (i = 0; i < pDistrictList.length; i++) {

        var bDistrictOption = $("<option/>");
        bDistrictOption.prop("value", pDistrictList[i].id);
        bDistrictOption.prop("text", pDistrictList[i].name);
        gDistrictSelectElement.append(bDistrictOption);

    };
}
//hàm đổ dữ liệu vào select phường xã
function loadDataToSelectWard(pWardList) {
    gWardSelectElement.find('option').remove()
    gWardSelectElement.append('<option value=none>Chọn phường xã</option>')

    for (i = 0; i < pWardList.length; i++) {

        var bWardOption = $("<option/>");
        bWardOption.prop("value", pWardList[i].id);
        bWardOption.prop("text", pWardList[i].name);
        gWardSelectElement.append(bWardOption);

    };
}
//hàm đổ dữ liệu vào select đường
function loadDataToSelectStreet(pStreetList) {
    gStreetelectElement.find('option').remove();
    gStreetelectElement.append('<option value=none>Chọn tỉnh đường</option>')
    for (i = 0; i < pStreetList.length; i++) {

        var bStreetOption = $("<option/>");
        bStreetOption.prop("value", pStreetList[i].id);
        bStreetOption.prop("text", pStreetList[i].name);
        gStreetelectElement.append(bStreetOption);

    };
}
function loadDataToSelectProject(pProjectList) {
    // gProjectSelectElement.find('option').remove()
    gProjectSelectElement.find('option').remove();
    gProjectSelectElement.append('<option value=none>Chọn dự án</option>')
    gProjectSelectElement.append('<option value=diffproject>Dự án khác</option>')
    for (i = 0; i < pProjectList.length; i++) {

        var bProjectOption = $("<option/>");
        bProjectOption.prop("value", pProjectList[i].id);
        bProjectOption.prop("text", pProjectList[i].name);
        gProjectSelectElement.append(bProjectOption);

    };
}
//hiển thị form khi load trang
function changeFormDisplay() {
    $("#nav-item-1").on("click", function () {
        $("#add-realestate-1").attr("style", "display:block");
        $("#add-realestate-2").attr("style", "display:none");
        $("#add-realestate-3").attr("style", "display:none");
        $("#add-realestate-4").attr("style", "display:none");
        $("#add-realestate-5").attr("style", "display:none");
    })
    $("#nav-item-2").on("click", function () {

        $("#add-realestate-1").attr("style", "display:none");
        $("#add-realestate-2").attr("style", "display:block");
        $("#add-realestate-3").attr("style", "display:none");
        $("#add-realestate-4").attr("style", "display:none");
        $("#add-realestate-5").attr("style", "display:none");
    })
    $("#nav-item-3").on("click", function () {

        $("#add-realestate-1").attr("style", "display:none");
        $("#add-realestate-2").attr("style", "display:none");
        $("#add-realestate-3").attr("style", "display:block");
        $("#add-realestate-4").attr("style", "display:none");
        $("#add-realestate-5").attr("style", "display:none");
    })
    $("#nav-item-4").on("click", function () {

        $("#add-realestate-1").attr("style", "display:none");
        $("#add-realestate-2").attr("style", "display:none");
        $("#add-realestate-3").attr("style", "display:none");
        $("#add-realestate-4").attr("style", "display:block");
        $("#add-realestate-5").attr("style", "display:none");
    })
    $("#nav-item-5").on("click", function () {

        $("#add-realestate-1").attr("style", "display:none");
        $("#add-realestate-2").attr("style", "display:none");
        $("#add-realestate-3").attr("style", "display:none");
        $("#add-realestate-4").attr("style", "display:none");
        $("#add-realestate-5").attr("style", "display:block");
    })

}

