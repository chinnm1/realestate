
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var isContact = [
    { key: 0, val: "Chưa liên lạc" },
    { key: 1, val: "Đã liên lạc" },

]
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gCUSTOMER_COLS = ["id", "contactName", "userRequest", "address", "mobile", "email", "isContact", "idRealestate", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gCUSTOMER_STT_COL = 0;
const gCUSTOMER_NAME_COL = 1;
const gCUSTOMER_TITLE_COL = 2;
const gCUSTOMER_ADDR_COL = 3;
const gCUSTOMER_PHONE_COL = 4;
const gCUSTOMER_EMAIL_COL = 5;
const gCUSTOMER_NOTE_COL = 6;
const gCUSTOMER_REALESTATE_COL = 7;
const gCUSTOMER_ACTION_COL = 8;
var table = $("#customer-table").removeAttr('width').DataTable({
    columns: [
        { data: gCUSTOMER_COLS[gCUSTOMER_STT_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_NAME_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_TITLE_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_ADDR_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_PHONE_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_EMAIL_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_NOTE_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_REALESTATE_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_ACTION_COL] },
    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gCUSTOMER_ACTION_COL,
            defaultContent: `
					<i class="fas fa-edit edit-district"></i>
					<i class="fas fa-trash-alt delete-district"></i>
                    <i class="fas fa-check-circle confirm-realestate"></i>
			`
        },

    ],

    scrollX: true,
    scrollCollapse: true,
    fixedColumns: true
})
//biến chứa hàng được chọn
var gRowSelected = ''
//biến chưa ds customer
var glistCustomer = [];
//biến chưa ds realestate
var gListRealestate = [];
//biến chứa id realestate được chọn
var gSelectRealestateId = '';
//biến chứa id realestate update
var gSelectRealestateIdUpdate = '';
//biến chứa id province được chọn
var gProvinceIdSelected = '';
//biến chứa element select realestate
var gRealestateSelectElement = $("#select-realestate");
//biến chứa element select realestate update
var gRealestateSelectUpdateElement = $("#select-realestate-update");
//biến chứa id customer update
var gCustomerIdSelected = '';
//biến chứa hàng đucọe chọn
var gRowProvinceSelected = '';
//biến chứa ds customer validate update
var glistCustomerValidateUpdate = [];
var gNameSelect = $('#select-name')
//biến chứa user id
var gUserId = '';
//biến chứ header
var gHeader = ''
var gUserName = ''

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    const token = getCookie("token");
    const userId = getCookie("userId");
    const userName = getCookie('username')


    //Gọi API để lấy thông tin người dùng

    //Khai báo xác thực ở headers
    gHeader = {
        Authorization: "Token" + " " + token
    };
    //gán giá trị cho biến userId
    gUserId = Number(userId);
    gUserName = userName;
    console.log('CHECK USER ID', gUserId);
    console.log('CHECK HEADER', gHeader);
    console.log('CHECK USERNAME', gUserName);
    $('#username').html(gUserName)
    //tải trang
    onPageLoading().then((res) => {
        glistCustomer = res
        loadDataToSelectName(glistCustomer);
        console.log('check data get all  from server', res);
        loadDataToTable(res)
    }).catch((err) => {
        console.log(err)
    });
    $("#select-name").on('change', function () {
        onSelectNameChange();
    })

    //create new customer
    $('#create-customer').on('click', function () {
        onBtnAddCustomerClick();
    })

    //CHọn bđs
    $("#select-realestate").on('change', function () {
        onSelectRealestateChange();
    })
    //CHọn bđs trên form update
    $("#select-realestate-update").on('change', function () {
        onSelectRealestateUpdateChange();
    })
    $('#btn-create-customer').on('click', function () {

        onBtnCreateCustomerOnModalClick();
    })
    //edit customer
    $('#customer-table').on('click', '.edit-district', function () {

        onBtnEditCustomerClick(this);
    })
    $('#btn-update-customer').on('click', function () {
        onBtnUpdateOnModalClick()
    })

    //xóa album
    $('#customer-table').on('click', '.delete-district', function () {
        onBtnDeleteCustomerClick(this);
    })
    $('#btn-confirm-delete-district').on('click', function () {

        onBtnConfirmOnDeleteModalClick(this);
    })
    //nhấn nút Logout
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })
    //nhấn nút confirm status
    $('#customer-table').on('click', '.confirm-realestate', function () {

        onBtnConfirmRealestateClick(this);
    })
    //nhấn nút confirm status trên modal
    $('#btn-confirm-status').on('click', function () {
        onBtnConfirmRealestateClickOnModal();
    })
    //nhấn nút cancel status trên modal
    $('#btn-cancel-status').on('click', function () {
        onBtnCancelCustomerClickOnModal();
    })
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })


})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm thực hiện khi thay đổi select province
function onSelectNameChange() {
    let gListFilter = [];
    if (glistCustomer && glistCustomer.length > 0) {
        glistCustomer.map(item => {
            if (item.id == $("#select-name option:selected").val()) {
                gListFilter.push(item);
                console.log('CHECK ITEM', item)
                customizeTable(gListFilter)
            }
        })
    }



}
//hàm click nút confirm trên modal review status
function onBtnConfirmRealestateClickOnModal() {
    let obj = {
        isContact: "1"
    }
    //gọi api để update status của bđs
    callApiToUpdateStatus(obj, gCustomerIdSelected).then(res => {
        alert("Update contact succeed !")
        $("#confirm-status-modal").modal("hide");
        console.log('check STATUS', res);
        onPageLoading().then((res) => {
            loadDataToTable(res)
        }).catch((e) => {
            console.log(e)
        }
        );
    }).catch(e => {
        console.log(e)
    });
}
function onBtnCancelCustomerClickOnModal() {
    let obj = {
        isContact: "0"
    }
    //gọi api để update status của bđs
    callApiToUpdateStatus(obj, gCustomerIdSelected).then(res => {
        alert("Update contact succeed !")
        $("#confirm-status-modal").modal("hide");
        console.log('check STATUS', res);
        onPageLoading().then((res) => {
            loadDataToTable(res)
        }).catch((e) => {
            console.log(e)
        }
        );
    }).catch(e => {
        console.log(e)
    });
}
//INPUT: nút confirm
function onBtnConfirmRealestateClick(paramButton) {
    $("#confirm-status-modal").modal("show");
    gCustomerIdSelected = getCustomerIdFromButton(paramButton)
    console.log('check id customer selected', gCustomerIdSelected)
    // window.location.href = `dataeditRealestate.html?id=${gRelestateIdSelected}`

}
//hàm gọi api load danh sách customer từ server khi tải trang
function onPageLoading() {
    console.log('làm tới đây')
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/customer",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm thực hiện khi nhấn nút add new album
function onBtnAddCustomerClick() {
    $("#create-customer-modal").modal("show");
    callApiToGetRealestate().then((res) => {
        gListRealestate = res;
        loadDataToRealestateSelect(gListRealestate)
    }).catch((err) => {
        console.log(err)
    });

}
//hàm thực hiện khi thay đổi select  bđs trên form post
function onSelectRealestateChange() {
    gSelectRealestateId = $("#select-realestate option:selected").val()

    console.log('chech realestate id selected', gSelectRealestateId)
}
//hàm thực hiện khi thay đổi select bđs trên form update
function onSelectRealestateUpdateChange() {
    gSelectRealestateIdUpdate = $("#select-realestate-update option:selected").val()

    console.log('chech realestate id update selected', gSelectRealestateIdUpdate)
}
//hàm thực hiện khi nhấn create customer trên modal
function onBtnCreateCustomerOnModalClick() {
    createNewCustomer();
    $("#create-customer-modal").modal("hide");
}
//hàm thực hiện khi lúc nút edit
function onBtnEditCustomerClick(paramButton) {
    $("#update-customer-modal").modal("show");
    gCustomerIdSelected = getCustomerIdFromButton(paramButton)
    console.log('check id CUSTOMER selected', gCustomerIdSelected)
    callApiToGetCustomerById(gCustomerIdSelected).then((res) => {
        console.log('check data customer need to update ress', res);
        showDataUpdateOnForm(res);
    }).catch((err) => {
        console.log(err)
    });
}
//hàm thực hiện khi nhấn nút update trên modal
function onBtnUpdateOnModalClick() {
    updateCustomer(gCustomerIdSelected);
    $("#update-customer-modal").modal("hide");

}
//Hàm thực hiện khi nhấn nút delete
function onBtnDeleteCustomerClick(paramButton) {
    $("#delete-modal").modal("show");
    gCustomerIdSelected = getCustomerIdFromButton(paramButton)

}
//hàm thực hiện khi nhấn nút confirm trên delete modal
function onBtnConfirmOnDeleteModalClick() {
    deletedCustomer();
    $("#delete-modal").modal("hide");

}

//hàm thực hiện khi click nút log out 
function onBtnLogOutClick() {
    //
    redirectToLogin();

}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function loadDataToSelectName(pCustomerList) {
    // gProjectSelectElement.find('option').remove()
    gNameSelect.find('option').remove();
    gNameSelect.append('<option value=none>Chọn tên khách hàng</option>')
    for (i = 0; i < pCustomerList.length; i++) {

        var bProjectOption = $("<option/>");
        bProjectOption.prop("value", pCustomerList[i].id);
        bProjectOption.prop("text", pCustomerList[i].contactName);
        gNameSelect.append(bProjectOption);

    };
}

//hàm gọi api để update status
function callApiToUpdateStatus(paramObj, paramCustomerId) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/customer/update_iscontact/${paramCustomerId}`,
            type: 'PUT',
            async: false,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramObj),
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (xhr, status, error) {
                reject(xhr, status, error)
            }
        })
    })

}
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
//Hàm get Cookie 
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//hàm load data to table
function loadDataToTable(paramList) {
    callApiToGetRealestate().then((res) => {
        gListRealestate = res;
        console.log("CHECK Real", gListRealestate);
        customizeData(paramList);
    }).catch((e) => {
        console.log(e)
    }
    );

}
//hàm xử lí data
function customizeData(paramList) {
    if (gListRealestate.length > 0) {
        paramList.map(item => {
            for (let i = 0; i < gListRealestate.length; i++) {
                if (item.idRealestate == gListRealestate[i].id) {
                    item.idRealestate = gListRealestate[i].title
                }
            }
            for (let i = 0; i < isContact.length; i++) {
                if (item.isContact == isContact[i].key) {
                    item.isContact = isContact[i].val
                }
            }

            return item
        })
        console.log('CHECK RESULT', paramList)
        customizeTable(paramList);
    }

}
//hàm custom lại bảng bđs
function customizeTable(data) {
    table.clear();
    table.rows.add(data);
    table.draw();

}
//hàm gọi api để lấy realestate
function callApiToGetRealestate() {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/realestate`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}


//hàm trả ra id customer khi nhấn nút edit
function getCustomerIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    gRowProvinceSelected = table.row(vTableRow).data();
    console.log(gRowProvinceSelected.id);
    return gRowProvinceSelected.id;
}
//hàm đổ dữ liệu vào select realestate 
function loadDataToRealestateSelect(pRealestateList) {
    gRealestateSelectElement.find('option').remove()
    gRealestateSelectElement.append('<option value=none>Chọn bất động sản</option>')
    for (i = 0; i < pRealestateList.length; i++) {
        var bRealestateOption = $("<option/>");
        bRealestateOption.prop("value", pRealestateList[i].id);
        bRealestateOption.prop("text", pRealestateList[i].title);
        gRealestateSelectElement.append(bRealestateOption);
    };
}
//hàm đổ dữ liệu vào select realestate on update form
function loadDataToRealestateSelectUpdate(pRealestateList) {
    gRealestateSelectUpdateElement.find('option').remove()
    gRealestateSelectUpdateElement.append('<option value=none>Chọn bất động sản</option>')
    for (i = 0; i < pRealestateList.length; i++) {
        var bRealestateOption = $("<option/>");
        bRealestateOption.prop("value", pRealestateList[i].id);
        bRealestateOption.prop("text", pRealestateList[i].title);
        gRealestateSelectUpdateElement.append(bRealestateOption);
    };
}
//hàm gọi api để lấy customer cần edit theo id
function callApiToGetCustomerById(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/customer/${id}`,
            async: false,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res);

            },
            error: function (err) {
                reject(e)
            }
        })
    })


}
//hàm hiển thị data lên form edit
function showDataUpdateOnForm(obj) {
    callApiToGetRealestate().then((res) => {
        gListRealestate = res;
        loadDataToRealestateSelectUpdate(gListRealestate);
        $('#input-contactName-update').val(obj.contactName);
        $('#input-contactTitle-update').val(obj.contactTitle);
        $('#input-address-update').val(obj.address);
        $('#input-mobile-update').val(obj.mobile);
        $('#input-email-update').val(obj.email);
        $('#input-note-update').val(obj.note);
        $("#select-realestate-update").val(obj.idRealestate)
    }).catch((err) => {
        console.log(err)
    });


}

function updateCustomer(paramRealestateId) {
    let objData = {
        contactName: '',
        contactTitle: '',
        address: '',
        mobile: '',
        email: "",
        note: ''
    }
    let objDataFromForm = getDataOnFormUpdate(objData);
    let vCheck = validateDataOnForm(objData);
    if (vCheck) {
        callApiToUpdateData(paramRealestateId, objDataFromForm).then((res) => {
            console.log(res);
        }).catch((errr) => {
            console.log(errr)
        })
        onPageLoading();
    }


}

//hàm thu thập dư liệu trên form update
function getDataOnFormUpdate(obj) {

    obj.contactName = $('#input-contactName-update').val();
    obj.contactTitle = $('#input-contactTitle-update').val();
    obj.address = $('#input-address-update').val();
    obj.mobile = $('#input-mobile-update').val();
    obj.email = $('#input-email-update').val();
    obj.note = $('#input-note-update').val();

    return obj
}
//hàm gọi api để update album
function callApiToUpdateData(id, paramOrderObject) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/customer/update/${id}`,
            type: 'PUT',
            async: false,
            headers: gHeader,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrderObject),
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(err)
            }
        })
    })

}
//save new customer
function createNewCustomer() {
    let objData = {
        contactName: '',
        contactTitle: '',
        address: '',
        mobile: '',
        email: "",
        note: ''
    }
    let objDataFromForm = getDataOnForm(objData);
    var vCheck = validateDataOnForm(objDataFromForm);
    if (vCheck) {
        console.log('ĐÃ CHECK')
        callApiToPostData(objDataFromForm, gSelectRealestateId).then(res => {
            console.log('DATA POST', res)
        }

        ).catch(e => {
            console.log(e)
        });
        // onPageLoading();
    }

}
//hàm thu thập dư liệu trên form
function getDataOnForm(obj) {
    obj.contactName = $('#input-contactName').val();
    obj.contactTitle = $('#input-contactTitle').val();
    obj.address = $('#input-address').val();
    obj.mobile = $('#input-mobile').val();
    obj.email = $('#input-email').val();
    obj.note = $('#input-note').val();

    return obj
}
//hàm validate dữ liệu
function validateDataOnForm(paramObj) {
    var arrField = ["contactName", "contactTitle", "address", "mobile", "email"]
    for (let i = 0; i < arrField.length; i++) {
        if (paramObj[arrField[i]] == "") {
            alert(`Missing ${arrField[i]}`);
            return false;
        }

    }
    return true;
}
//hàm gọi api để tạo customer mới
function callApiToPostData(paramOrderObject, id) {
    console.log("LÀM TỚI ĐÂY")
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/customer/create/${id}`,
            type: 'POST',
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrderObject),
            headers: gHeader,
            success: function (res) {
                console.log(res)
                resolve(res)
            },
            error: function (error) {
                reject(err)
            }
        })
    })

}
//hàm xóa customer
function deletedCustomer() {
    callApiToDeleteData(gCustomerIdSelected);
    onPageLoading();
}
//gọi api xóa customer
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/customer/delete/${id}`,
        type: 'DELETE',
        async: false,
        headers: gHeader,
        success: function (res) {
            alert('Delete success')

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}




