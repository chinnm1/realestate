
//biến chứa ds investor
var gListInvestor = {};
//biến chứa element select investor
var gInvestorSelectElement = $("#select-investor");
//biến biến chứa investor id được chọn
var gInvestorIdSelectedd = '';
//biến chứa ds constructor
var gListConstruction = {};
//biến chứa element select constructor
var gConstructionSelectElement = $("#select-construction-contractorr");
//biến biến chứa constructor id được chọn
var gConstructionIdSelectedd = '';
//biến chứa ds design unit
var gListDesignUnit = {};
//biến chứa element select design unit
var gDesignUnitSelectElement = $("#select-design-unit");
//biến biến chứa design unit id được chọn
var gDesignUnitIdSelectedd = '';
//biến chứa id relaesttae
var gProjectId = '';
//biến chứa ds province
var gProvinceList = {};
//biến chứa element select province
var gProvinceSelectElement = $("#select-province");
//biến biến chứa province id được chọn
var gProvinceIdSelectedd = ''
//biến chứa element select district
var gDistrictSelectElement = $("#select-district");
//biến chứa ds district
var gDistrictList = {};
//biến biến chứa district id được chọn
var gDistrictIdSelectedd = ''
//biến chứa element select ward
var gWardSelectElement = $("#select-ward");
//biến chứa ds ward
var gWardList = {};
//biến biến chứa ward id được chọn
var gWardIdSelectedd = ''
//biến chứa element select street
var gStreetelectElement = $("#select-street");
//biến chứa ds street
var gStreetList = {};
//biến biến chứa street id được chọn
var gStreetIdSelectedd = ''
//biến chứa element select project
var gProjectSelectElement = $("#select-project");
//biến chứa ds project
var gProjectList = {};
//biến biến chứa project id được chọn
var gProjectIdSelectedd = ''
//biến chứa user id
var gUserId = '';
//biến chứ header
var gHeader = ''
var gUserName = ''
$(document).ready(function () {
    const token = getCookie("token");
    const userId = getCookie("userId");
    const userName = getCookie('username')

    //Gọi API để lấy thông tin người dùng

    //Khai báo xác thực ở headers
    gHeader = {
        Authorization: "Token" + " " + token
    };
    //gán giá trị cho biến userId
    gUserId = Number(userId);
    gUserName = userName;
    console.log('CHECK USER ID', gUserId);
    console.log('CHECK HEADER', gHeader);
    console.log('CHECK USERNAME', gUserName);
    $('#username').html(gUserName)
    //load trang
    onPageLoading().then((res) => {
        //load dữ liệu vào table
        loadDataToTable(res)
    }).catch((e) => {
        console.log(e)
    }
    );
    //hiển thị form khi load trang
    $("#add-project-1").attr("style", "display:block");
    changeFormDisplay()
    //lấy id trên url 
    getIdFromUrl();
    //CHọn tỉnh thành phhos
    $("#select-province").on('change', function () {
        onSelectProvinceChange();
    })
    //CHọn quận huyện
    $("#select-district").on('change', function () {
        onSelectDistrictChange();
    })
    //CHọn phường xã
    $("#select-ward").on('change', function () {
        onSelectWardChange();
    })
    //CHọn đường
    $("#select-street").on('change', function () {
        onSelectStreetChange();
    })
    //thêm bất động sản mới
    $("#btn-edit").on('click', function () {

        onBtnEditClick();
    })
    //nhấn nút Logout
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })



})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm lấy id trên url
function getIdFromUrl() {
    var url_string = window.location.href;
    var url = new URL(url_string);
    gProjectId = url.searchParams.get("id");
    if (gProjectId != "") {
        //gọi api lấy project theo id
        callApiToGetProject(gProjectId).then((res) => {
            console.log('check realestate', res)
            //hiển thị thông tin lên form
            loadDataToForm(res);
        }).catch((e) => {
            console.log(e)
        }
        );
    }

}
//hàm thực hiện khi thay đổi select province
function onSelectProvinceChange() {
    if (gProvinceList && gProvinceList.length > 0) {
        gProvinceList.map(item => {
            if (item.id == $("#select-province option:selected").val()) {
                gProvinceIdSelectedd = item.id
            }
        })
    }
    console.log('CHECK PROVINCE ID SELECTED', gProvinceIdSelectedd)
    //gọi api lấy district theo province id
    callApiToGetDistrictByProvinceId().then((res) => {
        gDistrictList = res;
        loadDataToSelectDistrict(res);
    }).catch((e) => {
        console.log(e)
    }

    );
}
//hàm thực hiện khi thay đổi select district
function onSelectDistrictChange() {
    if (gDistrictList && gDistrictList.length > 0) {
        gDistrictList.map(item => {
            if (item.id == $("#select-district option:selected").val()) {
                gDistrictIdSelectedd = item.id
            }
        })
    }
    console.log('chech district id selected', gDistrictIdSelectedd)
    //gọi apir lấy ward theo district id
    callApiToGetWardByDistrictId().then((res) => {
        gWardList = res;
        //load data vào select ward
        loadDataToSelectWard(res);
    }).catch((e) => {
        console.log(e)
    }
    );
    //gọi apir lấy street theo district id và provicne id
    callApiToGetStreetByProvinceIdAndDistrictId().then((res) => {
        gStreetList = res;
        //load data vào select street
        loadDataToSelectStreet(res);
    }).catch((e) => {
        console.log(e)
    }
    );;

}
//hàm thực hiện khi nhấn nút add
function onBtnEditClick() {
    //B1:tạo đối tượng chứa thông
    let project = {
        name: "",
        address: "",
        slogan: "",
        description: "",
        acreage: 0,
        constructArea: 0,
        numBlock: 0,
        numFloor: "",
        numApartment: 0,
        apartmentArea: "",
        utilities: "",
        regionLink: "",
        lat: 0,
        lng: 0,
        idOfProvinceFromClient: -1,

        idOfDistrictFromClient: -1,

        idOfWardFromClient: -1,

        idOfStreetFromClient: -1,

        idOfProjectFromClient: -1,

        idOfDesignUnitFromClient: -1,

        idOfConstructionFromClient: -1,

        idOfInvestorFromClient: -1,

    }
    //B2 thu thập dữ liệu
    getDataFromForm(project);
    console.log('check data', project)
    //B3 validate dữ liệu
    let vCheck = validateData(project);
    if (vCheck) {
        //B4 gọi api để lưu dữ liệu
        callApiToPutDataToServer(project, gProjectId).then(res => {
            alert('Update succeed !')
        }).catch((xhr, status, error) => {
            if (xhr.responseJSON.message == "Forbidden") {
                alert("Bạn không có đủ quyền hạn để thực hiện chức năng này")
            } else {
                console.log(xhr, status, error);
                console.log()
            }
        });
    }
}
//hàm thực hiện khi thay đổi select ward
function onSelectWardChange() {
    if (gWardList && gWardList.length > 0) {
        gWardList.map(item => {
            if (item.id == $("#select-ward option:selected").val()) {
                gWardIdSelectedd = item.id
            }
        })
    }
    console.log('chech ward id selected', gWardIdSelectedd)


}
//hàm thực hiện khi thay đổi select street
function onSelectStreetChange() {
    if (gStreetList && gStreetList.length > 0) {
        gStreetList.map(item => {
            if (item.id == $("#select-street option:selected").val()) {
                gStreetIdSelectedd = item.id
            }
        })
    }
    console.log('chech street id selected', gStreetIdSelectedd)
    //gọi api lấy project theo province, district, ward, street id
    callApiToGetProjectByProvinceDistrictWardStreet().then((res) => {
        gProjectList = res;
        //hiển thị data lên select project
        loadDataToSelectProject(res);
    }).catch((e) => {
        console.log(e)
    }
    );;

}
//hàm thực hiện khi click nút log out 
function onBtnLogOutClick() {
    //
    redirectToLogin();

}


/*** REGION 4- COmmon function - Vùng khai báo các hàm dùng chung*/
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

//Hàm get Cookie 
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//hàm hiển thi data lên form
function loadDataToForm(paramObject) {
    $("#input-name").val(paramObject.name);
    $("#input-address").val(paramObject.address);
    $("#input-slogan").val(paramObject.slogan);
    $("#input-description ").val(paramObject.description);
    $("#input-acreage").val(paramObject.acreage);
    $("#input-construct-area").val(paramObject.constructArea);
    $("#input-num-block").val(paramObject.numBlock);
    $("#input-num-floors").val(paramObject.numFloor);
    $("#input-num-apartment").val(paramObject.numApartment);
    $("#input-apartmentt-area").val(paramObject.apartmentArea);
    $("#input-utilities").val(paramObject.utilities);
    $("#input-region-links").val(paramObject.regionLink);
    $("#input-lat").val(paramObject.lat);
    $("#input-lng").val(paramObject.lng);
    //hiển thị data province
    loadDataOfProjectToSelectProvince(paramObject.idOfProvince)
    //hiển thị data district
    loadDataOfProjectToSelectDistrict(paramObject.idOfDistrict)
    //hiển thị data ward
    loadDataOfProjectToSelectWard(paramObject.idOfWard)
    //hiển thị data street
    loadDataOfProjectToSelectStreet(paramObject.idOfStreet)
    if (paramObject.idOfProject != null) {
        //hiển thị data realestate
        loadDataOfRealestateToSelectProject(paramObject.idOfProject)
    }
    //hiển thị data investor
    loadDataOfProjectToSelectInvestor(paramObject.idOfStreet)
    //hiển thị data cóntructor
    loadDataOfProjectToSelectConstructor(paramObject.idOfStreet)
    //hiển thị data design unit
    loadDataOfProjectToSelectDesignUnit(paramObject.idOfStreet)

}
//Hàm hiển thị data lên select province
function loadDataOfProjectToSelectProvince(paramId) {
    callApiToGetProvinceFromServer().then((res) => {
        gProvinceList = res;
        console.log('CHECK PROVINCE', res)
        displayOnProvinceSelect(res, paramId);
    }).catch((e) => {
        console.log(e)
    }
    );

}
//Hàm hiển thị data lên select district
function loadDataOfProjectToSelectDistrict(paramId) {
    callApiToGetDistrictFromServer(paramId).then((res) => {
        displayOnDistrictSelect(res);
    }).catch((e) => {
        console.log(e)
    }
    );

}
//Hàm hiển thị data lên select ward
function loadDataOfProjectToSelectWard(paramId) {
    callApiToGetWardFromServer(paramId).then((res) => {

        displayOnWardSelect(res);
    }).catch((e) => {
        console.log(e)
    }
    );

}
//Hàm hiển thị data lên select street
function loadDataOfProjectToSelectStreet(paramId) {
    callApiToGetStreetFromServer(paramId).then((res) => {
        console.log('check ward', res);
        displayOnStreetSelect(res);
    }).catch((e) => {
        console.log(e)
    }
    );


}

//Hàm hiển thị data lên select investor
function loadDataOfProjectToSelectInvestor(paramId) {
    callApiToGetInvestorFromServer().then((res) => {
        console.log('check investor', res);
        displayOnInvestorSelect(res, paramId);
    }).catch((e) => {
        console.log(e)
    }
    );


}
//Hàm hiển thị data lên select constructor
function loadDataOfProjectToSelectConstructor(paramId) {
    callApiToGetConstructorFromServer().then((res) => {
        console.log('check constructor', res);
        displayOnConstructorSelect(res, paramId);
    }).catch((e) => {
        console.log(e)
    }
    );


}
//Hàm hiển thị data lên select design unit
function loadDataOfProjectToSelectDesignUnit(paramId) {
    callApiToGetDesignUnitFromServer().then((res) => {
        console.log('check design unit', res);
        displayOnDesignUnitSelect(res, paramId);
    }).catch((e) => {
        console.log(e)
    }
    );


}
//hàm gọi api để lấy project theo id
function callApiToGetProjectFromServer(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/project/detail/${id}`,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })
}

//hàm gọi api để lấy streettheo id
function callApiToGetStreetFromServer(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/street/${id}`,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })
}

//hàm gọi api để lấy ward theo id
function callApiToGetWardFromServer(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/ward/${id}`,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })
}
//hàm gọi api để lấy district theo id
function callApiToGetDistrictFromServer(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/district/${id}`,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })
}
//hàm gọi api để lấy province theo id
function callApiToGetProvinceFromServer() {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/province`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hàm gọi api để lấy investor
function callApiToGetInvestorFromServer() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/investor`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hàm gọi api để lấy constructor
function callApiToGetConstructorFromServer() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/constructor`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hàm gọi api để lấy design unit
function callApiToGetDesignUnitFromServer() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/designunit`,
            type: 'GET',
            dataType: 'json',
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}
//hiển thị district lên ô select
function displayOnDistrictSelect(paramDistrict) {
    $("#select-district").append(`<option selected value=${paramDistrict.id}>${paramDistrict.name}</option>`)

}
//hiển thị ward lên ô select
function displayOnWardSelect(paramWard) {
    $("#select-ward").append(`<option selected value=${paramWard.id}>${paramWard.name}</option>`)

}
//hiển thị street lên ô select
function displayOnStreetSelect(paramStreet) {
    $("#select-street").append(`<option selected value=${paramStreet.id}>${paramStreet.name}</option>`)

}
//hiển thị street lên ô project
function displayOnProjectSelect(paramProject) {
    $("#select-project").append(`<option selected value=${paramProject.id}>${paramProject.name}</option>`)

}

//hàm hiển province của bđs lên ô select
function displayOnProvinceSelect(paramProvince, id) {
    console.log('CHECK PARAMPROVINCE', paramProvince)
    for (let i = 0; i < paramProvince.length; i++) {
        $("#select-province").append(`<option value=${paramProvince[i].id}>${paramProvince[i].name}</option>`)
    }
    paramProvince.map(item => {
        paramProvince.map(item => {
            if (item.id == id) {
                $("#select-province").val(item.id)
            }

        })
    })


}
//hàm hiển investor của bđs lên ô select
function displayOnInvestorSelect(paramInvestor, id) {
    // console.log('CHECK PARAMPROVINCE', paramProvince)
    for (let i = 0; i < paramInvestor.length; i++) {
        $("#select-investor").append(`<option value=${paramInvestor[i].id}>${paramInvestor[i].name}</option>`)
    }
    paramInvestor.map(item => {
        paramInvestor.map(item => {
            if (item.id == id) {
                $("#select-investor").val(item.id)
            }

        })
    })


}
//hàm hiển constructor của bđs lên ô select
function displayOnConstructorSelect(paramConstructor, id) {
    // console.log('CHECK PARAMPROVINCE', paramProvince)
    for (let i = 0; i < paramConstructor.length; i++) {
        $("#select-construction-contractorr").append(`<option value=${paramConstructor[i].id}>${paramConstructor[i].name}</option>`)
    }
    paramConstructor.map(item => {
        paramConstructor.map(item => {
            if (item.id == id) {
                $("#select-construction-contractorr").val(item.id)
            }

        })
    })


}
//hàm hiển design unit của bđs lên ô select
function displayOnDesignUnitSelect(paramDesignUnit, id) {
    // console.log('CHECK PARAMPROVINCE', paramProvince)
    for (let i = 0; i < paramDesignUnit.length; i++) {
        $("#select-design-unit").append(`<option value=${paramDesignUnit[i].id}>${paramDesignUnit[i].name}</option>`)
    }
    paramDesignUnit.map(item => {
        paramDesignUnit.map(item => {
            if (item.id == id) {
                $("#select-design-unit").val(item.id)
            }

        })
    })


}

//Hàm thu thập dữ liệu trên form
function getDataFromForm(paramObject) {
    paramObject.name = $("#input-name").val();
    paramObject.address = $("#input-address").val();
    paramObject.slogan = $("#input-slogan").val();
    paramObject.description = $("#input-description ").val();
    paramObject.acreage = $("#input-acreage").val();
    paramObject.constructArea = $("#input-construct-area").val();
    paramObject.numBlock = $("#input-num-block").val();
    paramObject.numFloor = $("#input-num-floors").val();
    paramObject.numApartment = $("#input-num-apartment").val();
    paramObject.apartmentArea = $("#input-apartmentt-area").val();
    paramObject.utilities = $("#input-utilities").val();
    paramObject.regionLink = $("#input-region-links").val();
    paramObject.lat = $("#input-lat").val();
    paramObject.lng = $("#input-lng").val();

    paramObject.idOfProvinceFromClient = $("#select-province").val();
    paramObject.idOfDistrictFromClient = $("#select-district").val();
    paramObject.idOfWardFromClient = $("#select-ward").val();
    paramObject.idOfStreetFromClient = $("#select-street").val();
    paramObject.idOfInvestorFromClient = $("#select-investor").val();
    paramObject.idOfConstructionFromClient = $("#select-construction-contractorr").val();
    paramObject.idOfDesignUnitFromClient = $("#select-design-unit").val();
    return paramObject;


}

//hàm check validate data
function validateData(paramObject) {
    if (paramObject.idOfProvinceFromClient == -1) {
        alert("Phải chọn tỉnh thành phố")
        return false;
    }
    if (paramObject.idOfDistrictFromClient == -1) {
        alert("Phải chọn quận huyện")
        return false;

    }
    if (paramObject.idOfWardFromClient == -1) {
        alert("Phải chọn phường xã")
        return false;

    }
    if (paramObject.idOfStreetFromClient == -1) {
        alert("Phải chọn đường")
        return false;

    }
    if (paramObject.idOfConstructionFromClient == -1) {
        alert("Phải chọn nhà thầu")
        return false;

    }
    if (paramObject.idOfInvestorFromClient == -1) {
        alert("Phải chọn chủ đầu tư")
        return false;

    }
    if (paramObject.idOfDesignUnitFromClient == -1) {
        alert("Phải chọn đơn vị thiết kế")
        return false;

    }

    if (paramObject.address == "") {
        alert("Phải điền địa chỉ")
        return false;

    }
    return true;

}
//hàm gọi api để lưu dữ liệu
function callApiToPutDataToServer(paramObject, paramId) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/project/update/${paramId}`,
            type: 'PUT',
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramObject),
            headers: gHeader,
            success: function (res) {

                alert("Edit success");
                window.location.href = `datareadProject.html`
            },
            error: function (xhr, status, error) {
                reject(xhr, status, error)
            }
        })

    })

}
//hàm gọi api để lưu dữ liệu
function callApiToGetProject(paramId) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/project/detail/${paramId}`,
            type: 'GET',
            dataType: 'json',

            success: function (res) {
                resolve(res);
            },
            error: function (error) {
                reject(error)
            }
        })

    })

}

//hàm call api để lấy ra các quận huyện theo tỉnh thành phố
function callApiToGetDistrictByProvinceId() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/district/province/${gProvinceIdSelectedd}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })
}
//hàm call api để lấy ra các phường xã theo quận huyện
function callApiToGetWardByDistrictId() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/ward/district/${gDistrictIdSelectedd}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })
}
//hàm call api để lấy ra  tỉnh thành phố
function callApiToGetProvince() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/province",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm call api để lấy ra đường theo tỉnh quận
function callApiToGetStreetByProvinceIdAndDistrictId() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/street/${gDistrictIdSelectedd}/${gProvinceIdSelectedd}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm call api để lấy ra project
function callApiToGetProjectByProvinceDistrictWardStreet() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/project/${gProvinceIdSelectedd}/${gDistrictIdSelectedd}/${gWardIdSelectedd}/${gStreetIdSelectedd}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}


//hàm đổ dữ liệu vào select tỉnh thành phố
function loadDataToProvinceSelect(pProvinceList) {
    gProvinceSelectElement.find('option').remove()
    gProvinceSelectElement.append('<option value=none>Chọn tỉnh thành phố</option>')
    for (i = 0; i < pProvinceList.length; i++) {
        var bProvinceOption = $("<option/>");
        bProvinceOption.prop("value", pProvinceList[i].id);
        bProvinceOption.prop("text", pProvinceList[i].name);
        gProvinceSelectElement.append(bProvinceOption);

    };
}
//hàm đổ dữ liệu vào select quận huyện
function loadDataToSelectDistrict(pDistrictList) {
    gDistrictSelectElement.find('option').remove()
    gDistrictSelectElement.append('<option value=none>Chọn tỉnh quận huyện</option>')
    for (i = 0; i < pDistrictList.length; i++) {

        var bDistrictOption = $("<option/>");
        bDistrictOption.prop("value", pDistrictList[i].id);
        bDistrictOption.prop("text", pDistrictList[i].name);
        gDistrictSelectElement.append(bDistrictOption);

    };
}
//hàm đổ dữ liệu vào select phường xã
function loadDataToSelectWard(pWardList) {
    gWardSelectElement.find('option').remove()
    gWardSelectElement.append('<option value=none>Chọn phường xã</option>')

    for (i = 0; i < pWardList.length; i++) {

        var bWardOption = $("<option/>");
        bWardOption.prop("value", pWardList[i].id);
        bWardOption.prop("text", pWardList[i].name);
        gWardSelectElement.append(bWardOption);

    };
}
//hàm đổ dữ liệu vào select đường
function loadDataToSelectStreet(pStreetList) {
    gStreetelectElement.find('option').remove();
    gStreetelectElement.append('<option value=none>Chọn tỉnh đường</option>')
    for (i = 0; i < pStreetList.length; i++) {

        var bStreetOption = $("<option/>");
        bStreetOption.prop("value", pStreetList[i].id);
        bStreetOption.prop("text", pStreetList[i].name);
        gStreetelectElement.append(bStreetOption);

    };
}
function loadDataToSelectProject(pProjectList) {
    // gProjectSelectElement.find('option').remove()
    gProjectSelectElement.find('option').remove();
    gProjectSelectElement.append('<option value=none>Chọn dự án</option>')
    gProjectSelectElement.append('<option value=diffproject>Dự án khác</option>')
    for (i = 0; i < pProjectList.length; i++) {

        var bProjectOption = $("<option/>");
        bProjectOption.prop("value", pProjectList[i].id);
        bProjectOption.prop("text", pProjectList[i].name);
        gProjectSelectElement.append(bProjectOption);

    };
}

//hiển thị form khi nhán navbar
function changeFormDisplay() {
    $("#nav-item-1").on("click", function () {
        $("#add-project-1").attr("style", "display:block");
        $("#add-project-2").attr("style", "display:none");

    })
    $("#nav-item-2").on("click", function () {

        $("#add-project-1").attr("style", "display:none");
        $("#add-project-2").attr("style", "display:block");

    })


}

