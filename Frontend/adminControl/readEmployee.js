
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var activated = [{ key: 1, val: "Y" }, { key: 2, val: "N" }]
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gEMPLOYEE_COLS = ["action", "employeeID", "name", "username", "userLevel", "title", "birthday", "hireday", "address", "city", "country", "homePhone", "email", "activated"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gEMPLOYEE_ACTION_COL = 0;
const gEMPLOYEE_STT_COL = 1;
const gEMPLOYEE_NAME_COL = 2;
const gEMPLOYEE_USERNAME_COL = 3;
const gEMPLOYEE_LEVEL_COL = 4;
const gEMPLOYEE_TITLE_COL = 5;
const gEMPLOYEE_BIRTH_COL = 6;
const gEMPLOYEE_HIRE_COL = 7;
const gEMPLOYEE_ADDRESS_COL = 8;
const gEMPLOYEE_CITY_COL = 9;
const gEMPLOYEE_COUNTRY_COL = 10;
const gEMPLOYEE_PHONE_COL = 11;

const gEMPLOYEE_EMAIL_COL = 12;
const gEMPLOYEE_ACTIVATED_COL = 13;





var table = $("#employee-table").DataTable({
    columns: [
        { data: gEMPLOYEE_COLS[gEMPLOYEE_ACTION_COL] },
        { data: gEMPLOYEE_COLS[gEMPLOYEE_STT_COL] },
        { data: gEMPLOYEE_COLS[gEMPLOYEE_NAME_COL] },
        { data: gEMPLOYEE_COLS[gEMPLOYEE_USERNAME_COL] },
        { data: gEMPLOYEE_COLS[gEMPLOYEE_LEVEL_COL] },

        { data: gEMPLOYEE_COLS[gEMPLOYEE_TITLE_COL] },
        { data: gEMPLOYEE_COLS[gEMPLOYEE_BIRTH_COL] },
        { data: gEMPLOYEE_COLS[gEMPLOYEE_HIRE_COL] },
        { data: gEMPLOYEE_COLS[gEMPLOYEE_ADDRESS_COL] },
        { data: gEMPLOYEE_COLS[gEMPLOYEE_CITY_COL] },
        { data: gEMPLOYEE_COLS[gEMPLOYEE_COUNTRY_COL] },
        { data: gEMPLOYEE_COLS[gEMPLOYEE_PHONE_COL] },

        { data: gEMPLOYEE_COLS[gEMPLOYEE_EMAIL_COL] },
        { data: gEMPLOYEE_COLS[gEMPLOYEE_ACTIVATED_COL] },



    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gEMPLOYEE_ACTION_COL,
            defaultContent: `
					<i class="fas fa-edit edit-employee"></i>
					<i class="fas fa-trash-alt delete-customer"></i>
                    <i class="fas fa-wrench update-level"></i>
			`
        },


    ],
    scrollX: true,
    scrollCollapse: true,

    fixedColumns: true
})
//biến chứa hàng được chọn
var gRowSelected = ''
//biến chứa ds employee
var glistEmployee = [];
//biến chứa id employee
var gEmployeeIdSelected = '';
//biến chứa username của employee
var gEmployeeUsernameSelected = '';
//biến chứa danh sách role
var gRoleList = [];
//biến chứa role select
var gElementRoleSelect = $("#select-level");
//biến chứ role í
var gRoleIdSelected = ''
//biến chứa hàng province
var gRowSelected = '';
//biến chứa list employee update
var glistEmployeeValidateUpdate = [];
//biến chứa user id
var gUserId = '';
//biến chứ header
var gHeader = ''
var gUserName = ''
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    const token = getCookie("token");
    const userId = getCookie("userId");
    const userName = getCookie('username')

    //Gọi API để lấy thông tin người dùng

    //Khai báo xác thực ở headers
    gHeader = {
        Authorization: "Token" + " " + token
    };
    //gán giá trị cho biến userId
    gUserId = Number(userId);
    gUserName = userName;
    console.log('CHECK USER ID', gUserId);
    console.log('CHECK HEADER', gHeader);
    console.log('CHECK USERNAME', gUserName);
    $('#username').html(gUserName)
    //tải trang
    onPageLoading().then((res) => {
        glistEmployee = res;
        //load dữ liệu vào table
        displayDataOnTable(glistEmployee);
        // console.log('check data get all  from server', res);

    }).catch((err) => {
        console.log(err)
    });
    //create new emplouee
    $('#create-employee').on('click', function () {
        onBtnAddEmployeeClick();
    })
    $('#btn-create-employee').on('click', function () {

        onBtnCreateInvestorOnModalClick();
    })
    //edit employee
    $('#employee-table').on('click', '.edit-employee', function () {

        onBtnEditEmployeeClick(this);
    })
    $('#btn-update-investor').on('click', function () {
        onBtnUpdateOnModalClick()
    })

    //xóa employee
    $('#employee-table').on('click', '.delete-customer', function () {

        onBtnDeleteInvestorClick(this);
    })
    $('#btn-confirm-delete-investor').on('click', function () {

        onBtnConfirmOnDeleteModalClick(this);
    })
    //update level employee
    $('#employee-table').on('click', '.update-level', function () {

        onBtnUpdateLevelClick(this);
    })
    $('#btn-update-level-employee').on('click', function () {

        onBtnUpdateLevelOnModalClick();
    })
    //nhấn nút Logout
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })



})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm gọi api load danh sách employee từ server khi tải trang
function onPageLoading() {
    console.log('làm tới đây')
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/employee",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm thực hiện khi nhấn nút add new amployee
function onBtnAddEmployeeClick() {
    $("#create-employee-modal").modal("show");

}
//hàm thực hiện khi nhấn create employee trên modal
function onBtnCreateInvestorOnModalClick() {
    createNewEmployee();
    $("#create-employee-modal").modal("hide");
}
//hàm thực hiện khi lúc nút edit
function onBtnEditEmployeeClick(paramButton) {
    $("#update-employee-modal").modal("show");
    gEmployeeIdSelected = getEmployeeFromButton(paramButton);
    console.log('check id customer selected', gEmployeeIdSelected)
    callApiToGetEmployeeById(gEmployeeIdSelected).then((res) => {
        console.log('check data customer need to update ress', res);
        showDataUpdateOnForm(res);
    }).catch((err) => {
        console.log(err)
    });
}
//hàm thực hiện khi nhấn nút update trên modal
function onBtnUpdateOnModalClick() {
    updateEmployee(gEmployeeIdSelected);
    $("#update-employee-modal").modal("hide");

}
//Hàm thực hiện khi nhấn nút delete
function onBtnDeleteInvestorClick(paramButton) {
    $("#delete-modal").modal("show");
    gEmployeeIdSelected = getEmployeeFromButton(paramButton)

}
//hàm thực hiện khi nhấn nút confirm trên delete modal
function onBtnConfirmOnDeleteModalClick() {
    deletedEmployee();
    $("#delete-modal").modal("hide");

}
//Hàm thực hiện khi nhấn nút update level
function onBtnUpdateLevelClick(paramButton) {
    $("#update-level-modal").modal("show");
    gEmployeeIdSelected = getEmployeeFromButton(paramButton);
    gEmployeeUsernameSelected = getEmployeeUsernameFromButton(paramButton);
    //hiển thị thông tin lên form
    disPlayDataOnFormUpdateLevel(gEmployeeUsernameSelected);
}
//hàm thực hiện khi nhấn nút update level trên modal
function onBtnUpdateLevelOnModalClick() {
    //thu thập data on form
    gRoleIdSelected = $("#select-level").val();
    gEmployeeUsernameSelected = $("#input-username-update-level").val();
    //validate dữ liệu
    var vCheck = validateDataUpdateLevel(gRoleIdSelected, gEmployeeUsernameSelected);
    if (vCheck) {
        //call api to update level user
        callApiToUpdateLeval(gEmployeeIdSelected, gRoleIdSelected).then(res => {
            onPageLoading().then((res) => {
                glistEmployee = res;
                //load dữ liệu vào table
                displayDataOnTable(glistEmployee);
                // console.log('check data get all  from server', res);

            }).catch((err) => {
                console.log(err)
            });
        }).catch((xhr, status, error) => {

            console.log(xhr, status, error);
        })
        //call api để update role user
        callApiToUpdateRole(gRoleIdSelected, gEmployeeUsernameSelected).then(res => {
            console.log('CHECL ROLE UPDATE', res)
        }).catch((xhr, status, error) => {
            if (xhr.responseJSON.message == "Forbidden") {
                alert("Bạn không có đủ quyền hạn để thực hiện chức năng này")
            } else {
                console.log(xhr, status, error);
                console.log()
            }
        })
        $("#update-level-modal").modal("hide");
    }
}
//hàm thực hiện khi click nút log out 
function onBtnLogOutClick() {
    //
    redirectToLogin();

}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
//Hàm get Cookie 
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//hàm gọi api để update level
function callApiToUpdateLeval(paramEmployeeId, paramRoleId) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/employee/update_level/${paramEmployeeId}/${paramRoleId}`,
            type: 'PUT',
            async: false,
            headers: gHeader,
            contentType: "application/json;charset=UTF-8",
            // data: JSON.stringify(paramOrderObject),
            success: function (res) {
                resolve(res);
            },
            error: function (xhr, status, error) {
                reject(xhr, status, error)
            }
        })
    })
}
//hàm gọi api để update role
function callApiToUpdateRole(paramRoleId, paramUsername) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/employee/update/${paramUsername}/${paramRoleId}`,
            type: 'PUT',
            async: false,
            headers: gHeader,
            contentType: "application/json;charset=UTF-8",
            // data: JSON.stringify(paramOrderObject),
            success: function (res) {
                resolve(res);
            },
            error: function (xhr, status, error) {
                reject(xhr, status, error)
            }
        })
    })
}

//hàm validate dữ liệu trên form uodate level
function validateDataUpdateLevel(paramRoleId, paramUsername) {
    if (paramRoleId == 0) {
        alert('Chưa chọn level');
        return false;
    }
    if (paramUsername = '') {
        alert('Chưa điền user name');
        return false;
    }
    return true;
}
//hàm hiển thị dữ liệu lên form update level
function disPlayDataOnFormUpdateLevel(paramUsername) {
    $("#input-username-update-level").val(paramUsername);
    //gọi api để lấy dữ liệu tất cả role 
    callApiToGetRole().then(res => {
        console.log('check role', res);
        gRoleList = res
        loadDataToSelectRole(gRoleList)
    }).catch(e => {
        console.log(e);
    })
}
//hàm load data vào ô select role
function loadDataToSelectRole(pRoleList) {
    gElementRoleSelect.find('option').remove()
    gElementRoleSelect.append('<option value=0 selected>Chọn level</option>')
    for (i = 0; i < pRoleList.length; i++) {
        var bRoleOption = $("<option/>");
        bRoleOption.prop("value", pRoleList[i].id);
        bRoleOption.prop("text", pRoleList[i].roleName);
        gElementRoleSelect.append(bRoleOption);

    };
}
//hàm gọi api để lấy tất cả role
function callApiToGetRole() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/role`,
            async: false,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res);
            },
            error: function (err) {
                reject(err)
            }
        })
    })
}




//hàm format dữ liệu hiển thị
function displayDataOnTable(data) {
    if (glistEmployee && glistEmployee.length > 0) {
        glistEmployee.map(item => {
            item.name = item.firstName + " " + item.lastName
            return item;
        })


    }
    table.clear();
    table.rows.add(glistEmployee);
    table.draw();
}
//hàm trả ra id employee khi nhấn nút edit
function getEmployeeFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    gRowSelected = table.row(vTableRow).data();
    console.log(gRowSelected.employeeID);
    return gRowSelected.employeeID;
}
//hàm trả ra username của employee khi nhấn nút update level
function getEmployeeUsernameFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    gRowSelected = table.row(vTableRow).data();
    console.log('CHECK USERNAME', gRowSelected.username);
    return gRowSelected.username;
}
//hàm gọi api để lấy employee cần edit theo id
function callApiToGetEmployeeById(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/employee/${id}`,
            async: false,
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res);
            },
            error: function (err) {
                reject(e)
            }
        })
    })


}
//hàm hiển thị data lên form edit
function showDataUpdateOnForm(obj) {
    // console.log('CHECK DATE', new Date(Date.parse(obj.birthday)))
    $('#input-birthday-update').val(obj.birthday);
    $('#input-hireday-update').val(obj.hireday)
    $('#input-lastName-update').val(obj.lastName);
    $('#input-firstName-update').val(obj.firstName);
    $('#input-title-update').val(obj.title);
    $('#input-titleOfCourtesy-update').val(obj.titleOfCourtesy);
    $('#input-address-update').val(obj.address);
    $('#input-city-update').val(obj.city);
    $('#input-region-update').val(obj.region);
    $('#input-postalCode-update').val(obj.postalCode);
    $('#input-country-update').val(obj.country);
    $('#input-homePhone-update').val(obj.homePhone);
    $('#input-extension-update').val(obj.extension);
    $('#input-note-update').val(obj.note);
    $('#input-reportTo-update').val(obj.reportTo);
    $('#input-username-update').val(obj.username);
    $('#input-password-update').val(obj.password);
    $('#input-email-update').val(obj.email);
    $("#select-activated-update").val(obj.activated);
    $('#input-profile-update').val(obj.profile);


}
//hàm update employee
function updateEmployee(paramInvestorId) {
    let objData = {
        lastName: '',
        firstName: '',
        title: '',
        titleOfCourtesy: '',
        birthday: '',
        hireday: '',
        address: '',
        city: '',
        region: '',
        postalCode: '',
        country: '',
        homePhone: '',
        extension: '',
        note: '',
        reportTo: -1,
        username: "",
        password: "",
        email: '',
        stringEnum: '',
        profile: '',

    }
    //thu thập data
    let objDataFromForm = getDataOnFormUpdate(objData);
    console.log('CHECK OB', objDataFromForm)
    //validate
    var vCheck = validateDataOnForm(objDataFromForm);
    if (vCheck) {
        //gọi api update
        callApiToUpdateData(paramInvestorId, objDataFromForm).then((res) => {
            console.log(res);
        }).catch((errr) => {
            console.log(errr)
        })
        onPageLoading().then((res) => {
            glistEmployee = res;
            displayDataOnTable(glistEmployee);
            // console.log('check data get all  from server', res);

        }).catch((err) => {
            console.log(err)
        });


    }
}




//hàm thu thập dư liệu trên form update
function getDataOnFormUpdate(obj) {
    obj.lastName = $('#input-lastName-update').val();
    obj.firstName = $('#input-firstName-update').val();
    obj.title = $('#input-title-update').val();
    obj.titleOfCourtesy = $('#input-titleOfCourtesy-update').val();
    obj.birthday = $('#input-birthday-update').val();
    obj.hireday = $('#input-hireday-update').val();
    obj.address = $('#input-address-update').val();
    obj.city = $('#input-city-update').val();
    obj.region = $('#input-region-update').val();
    obj.postalCode = $('#input-postalCode-update').val();
    obj.country = $('#input-country-update').val();
    obj.homePhone = $('#input-homePhone-update').val();
    obj.extension = $('#input-extension-update').val();
    obj.note = $('#input-note-update').val();
    obj.reportTo = $('#input-reportTo-update').val();
    obj.username = $('#input-username-update').val();
    obj.password = $('#input-password-update').val();
    obj.email = $('#input-email-update').val();
    obj.stringEnum = $("#select-activated-update").val();
    obj.profile = $('#input-profile-update').val();

    return obj
}
//hàm gọi api để update employee
function callApiToUpdateData(id, paramOrderObject) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/employee/update/${id}`,
            type: 'PUT',
            async: false,
            contentType: "application/json;charset=UTF-8",
            headers: gHeader,
            data: JSON.stringify(paramOrderObject),
            success: function (res) {
                resolve(res);

            },
            error: function (error) {
                reject(err)
            }
        })
    })

}

//save new employee
function createNewEmployee() {
    let objData = {
        lastName: '',
        firstName: '',
        title: '',
        titleOfCourtesy: '',
        birthday: '',
        hireday: '',
        address: '',
        city: '',
        region: '',
        postalCode: '',
        country: '',
        homePhone: '',
        extension: '',
        note: '',
        reportTo: -1,
        username: "",
        password: "",
        email: '',
        stringEnum: '',
        profile: '',


    }
    //thu thập data
    let objDataFromForm = getDataOnForm(objData);
    //validate
    var vCheck = validateDataOnForm(objDataFromForm);
    if (vCheck) {
        //call api
        callApiToPostData(objDataFromForm);
        onPageLoading().then((res) => {
            glistEmployee = res;
            displayDataOnTable(glistEmployee);
        }).catch((err) => {
            console.log(err)
        });
    }


}
//hàm validate dữ liệu
function validateDataOnForm(paramObj) {
    let arrField = ["lastName", "firstName", "title", "titleOfCourtesy", "birthday", "hireday", "address", "city",
        "region", "country", "homePhone", "extension", "note", "reportTo", "username", "password", "email",
        "stringEnum", "profile",]
    console.log("CHECK data", paramObj[arrField[1]])
    for (let i = 0; i < arrField.length; i++) {
        // console.log("CHECK data", paramObj.arrField[0])
        if (paramObj[arrField[i]] == "") {
            alert("Missing " + arrField[i])
            return false
        }
        return true;
    }
}
//hàm thu thập dư liệu trên form
function getDataOnForm(obj) {
    obj.lastName = $('#input-lastName').val();
    obj.firstName = $('#input-firstName').val();
    obj.title = $('#input-title').val();
    obj.titleOfCourtesy = $('#input-titleOfCourtesy').val();
    obj.birthday = $('#input-birthday').val();
    obj.hireday = $('#input-hireday').val();
    obj.address = $('#input-address').val();
    obj.city = $('#input-city').val();
    obj.region = $('#input-region').val();
    obj.postalCode = $('#input-postalCode').val();
    obj.country = $('#input-country').val();
    obj.homePhone = $('#input-homePhone').val();
    obj.extension = $('#input-extension').val();
    obj.note = $('#input-note').val();
    obj.reportTo = $('#input-reportTo').val();
    obj.username = $('#input-username').val();
    obj.password = $('#input-password').val();
    obj.email = $('#input-email').val();
    obj.stringEnum = $("#select-activated").val();
    obj.profile = $('#input-profile').val();

    return obj
}
//hàm gọi api để tạo employee mới
function callApiToPostData(paramOrderObject) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/employee/create`,
            type: 'POST',
            contentType: "application/json;charset=UTF-8",
            headers: gHeader,
            data: JSON.stringify(paramOrderObject),
            success: function (res) {

                resolve(res)

            },
            error: function (error) {
                reject(error)
            }
        })
    })

}
//xóa employee
function deletedEmployee() {
    callApiToDeleteData(gEmployeeIdSelected);
    onPageLoading().then((res) => {
        glistEmployee = res;
        displayDataOnTable(glistEmployee);


    }).catch((err) => {
        console.log(err)
    });
}
//gọi api xóa employee
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/employee/delete/${id}`,
        type: 'DELETE',
        async: false,
        headers: gHeader,
        success: function (res) {
            alert('Delete success')

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}




