
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gSTREET_COLS = ["id", "name", "prefix", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gSTREET_STT_COL = 0;
const gSTREET_NAME_COL = 1;
const gSTREET_PREFIX_COL = 2;
const gSTREET_ACTION_COL = 3;
var table = $("#ward-table").DataTable({
    columns: [
        { data: gSTREET_COLS[gSTREET_STT_COL] },
        { data: gSTREET_COLS[gSTREET_NAME_COL] },
        { data: gSTREET_COLS[gSTREET_PREFIX_COL] },
        { data: gSTREET_COLS[gSTREET_ACTION_COL] },
    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gSTREET_ACTION_COL,
            defaultContent: `
					<i class="fas fa-edit edit-street"></i>
					<i class="fas fa-trash-alt delete-district"></i>
			`
        },

    ],
    scrollX: true,
    scrollCollapse: true,

    fixedColumns: true
})
//biến chứa hàng được chọn
var gRowSelected = ''
//biến chứa danh sách street
var glistStreet = [];
//biến chứa id street
var gStreetIdSelected = '';
//biến chứa danh sách province
var gProvinceList = [];
//biến chứa danh sách district
var gDistrictList = [];
//biến chứa id province
var gProvinceIdSelected = '';
//biến chứa  province select
var gProvinceSelectElement = $("#select-province");
//biến chứa id district
var gDistrictIdSelected = '';
//biến chứa  district select
var gDistrictSelectElement = $("#select-district");
var gRowProvinceSelected = '';
var glistStreetValidateUpdate = [];
//biến chứa user id
var gUserId = '';
//biến chứ header
var gHeader = ''
var gUserName = ''
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    const token = getCookie("token");
    const userId = getCookie("userId");
    const userName = getCookie('username')

    //Gọi API để lấy thông tin người dùng

    //Khai báo xác thực ở headers
    gHeader = {
        Authorization: "Token" + " " + token
    };
    //gán giá trị cho biến userId
    gUserId = Number(userId);
    gUserName = userName;
    console.log('CHECK USER ID', gUserId);
    console.log('CHECK HEADER', gHeader);
    console.log('CHECK USERNAME', gUserName);
    $('#username').html(gUserName)
    //tải trang
    onPageLoading().then((res) => {
        glistStreet = res
        console.log('check data get all  from server', res);
        table.clear();
        table.rows.add(res);
        table.draw();
    }).catch((err) => {
        console.log(err)
    });
    //create new ward
    $('#create-street').on('click', function () {
        onBtnAddStreetClick();
    })
    //chọn thành phố
    //CHọn tỉnh thành phhos
    $("#select-province").on('change', function () {
        onSelectProvinceChange();
    })
    //CHọn tỉnh thành quận huyện
    $("#select-district").on('change', function () {
        onSelectDistrictChange();
    })
    $('#btn-create-street').on('click', function () {

        onBtnCreateWardOnModalClick();
    })
    //edit street
    $('#ward-table').on('click', '.edit-street', function () {

        onBtnEditStreetClick(this);
    })
    $('#btn-update-street').on('click', function () {
        onBtnUpdateOnModalClick()
    })

    //xóa street
    $('#ward-table').on('click', '.delete-district', function () {
        onBtnDeleteWardClick(this);
    })
    $('#btn-confirm-delete-street').on('click', function () {

        onBtnConfirmOnDeleteModalClick(this);
    })
    //nhấn nút Logout
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })


})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm gọi api load danh sách province từ server khi tải trang
function onPageLoading() {
    console.log('làm tới đây')
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/street",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })
}
//hàm thực hiện khi nhấn nút add new street
function onBtnAddStreetClick() {
    $("#create-street-modal").modal("show");
    callApiToGetProvince().then((res) => {
        gProvinceList = res;
        loadDataToProvinceSelect(res)
    }).catch((err) => {
        console.log(err)
    });

}
//hàm thực hiện khi thay đổi select province
function onSelectProvinceChange() {
    if (gProvinceList && gProvinceList.length > 0) {
        gProvinceList.map(item => {
            if (item.id == $("#select-province option:selected").val()) {
                gProvinceIdSelected = item.id
            }
        })
    }

    console.log('chech province id selected', gProvinceIdSelected);
    callApiToGetDistrictByProvinceId(gProvinceIdSelected).then((res) => {
        gDistrictList = res
        console.log('check data get all  from server', res);
        loadDataToDistrictSelect(res)
        // table.clear();
        // table.rows.add(res);
        // table.draw();
    }).catch((err) => {
        console.log(err)
    });
}
//hàm thực hiện khi thay đổi select district
function onSelectDistrictChange() {
    if (gDistrictList && gDistrictList.length > 0) {
        gDistrictList.map(item => {
            if (item.id == $("#select-district option:selected").val()) {
                gDistrictIdSelected = item.id
            }
        })
    }

    console.log('chech district id selected', gDistrictIdSelected);
}

//hàm thực hiện khi nhấn create district trên modal
function onBtnCreateWardOnModalClick() {
    createNewStreet();
    $("#create-street-modal").modal("hide");
}
//hàm thực hiện khi lúc nút edit
function onBtnEditStreetClick(paramButton) {
    $("#update-street-modal").modal("show");
    gStreetIdSelected = getStreetIdFromButton(paramButton)
    console.log('check id ward selected', gStreetIdSelected)
    callApiToGetStreetById(gStreetIdSelected).then((res) => {
        console.log('check data customer need to update ress', res);
        showDataUpdateOnForm(res);
    }).catch((err) => {
        console.log(err)
    });
}
//hàm thực hiện khi nhấn nút update trên modal
function onBtnUpdateOnModalClick() {
    updateStreet(gStreetIdSelected);
    $("#update-street-modal").modal("hide");

}
//Hàm thực hiện khi nhấn nút delete
function onBtnDeleteWardClick(paramButton) {
    $("#delete-modal").modal("show");
    gStreetIdSelected = getStreetIdFromButton(paramButton)

}
//hàm thực hiện khi nhấn nút confirm trên delete modal
function onBtnConfirmOnDeleteModalClick() {
    deletedStreet();
    $("#delete-modal").modal("hide");

}
//hàm thực hiện khi click nút log out 
function onBtnLogOutClick() {
    //
    redirectToLogin();

}



/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
//Hàm get Cookie 
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//hàm trả ra id street khi nhấn nút edit
function getStreetIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    gRowProvinceSelected = table.row(vTableRow).data();
    console.log(gRowProvinceSelected.id);
    return gRowProvinceSelected.id;
}
//hàm call api để lấy ra  tỉnh thành phố
function callApiToGetProvince() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/province",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm call api để lấy ra  quận huyện theo tỉnh
function callApiToGetDistrictByProvinceId(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/district/province/${id}`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm đổ dữ liệu vào select province
function loadDataToProvinceSelect(pProvinceList) {
    gProvinceSelectElement.find('option').remove()
    gProvinceSelectElement.append('<option value=none>Chọn tỉnh thành phố</option>')
    for (i = 0; i < pProvinceList.length; i++) {
        var bProvinceOption = $("<option/>");
        bProvinceOption.prop("value", pProvinceList[i].id);
        bProvinceOption.prop("text", pProvinceList[i].name);
        gProvinceSelectElement.append(bProvinceOption);

    };
}
//hàm đổ dữ liệu vào select district
function loadDataToDistrictSelect(pDistrictList) {
    gDistrictSelectElement.find('option').remove()
    gDistrictSelectElement.append('<option value=none>Chọn tỉnh quận huyện</option>')
    for (i = 0; i < pDistrictList.length; i++) {
        var bDistrictOption = $("<option/>");
        bDistrictOption.prop("value", pDistrictList[i].id);
        bDistrictOption.prop("text", pDistrictList[i].name);
        gDistrictSelectElement.append(bDistrictOption);
    };
}
//hàm gọi api để lấy street cần edit theo id
function callApiToGetStreetById(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/street/${id}`,
            async: false,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res);
            },
            error: function (err) {
                reject(e)
            }
        })
    })


}
//hàm hiển thị data lên form edit
function showDataUpdateOnForm(paramProvince) {
    $('#input-name-update').val(paramProvince.name);
    $('#input-prefix-update').val(paramProvince.prefix);
}

function updateStreet(paramStreetdId) {
    let objData = {
        name: "",
        prefix: ""
    }
    let objDataFromForm = getDataOnFormUpdate(objData);
    let vCheck = validateDataOnForm(objData);
    if (vCheck) {
        callApiToUpdateData(paramStreetdId, objDataFromForm).then((res) => {
            console.log(res);
        }).catch((errr) => {
            console.log(errr)
        })
        onPageLoading();
    }


}

//hàm thu thập dư liệu trên form update
function getDataOnFormUpdate(obj) {

    obj.name = $('#input-name-update').val();
    obj.prefix = $('#input-prefix-update').val();

    return obj
}
//hàm gọi api để update street
function callApiToUpdateData(id, paramOrderObject) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/street/update/${id}`,
            type: 'PUT',
            async: false,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrderObject),
            headers: gHeader,
            success: function (res) {
                console.log("CHECK DATA UPDATE", res)
                resolve(res);
            },
            error: function (error) {
                reject(err)
            }
        })
    })

}
//save new street
function createNewStreet() {
    let objData = {
        name: '',
        prefix: '',
    }
    let objDataFromForm = getDataOnForm(objData);
    var vCheck = validateDataOnForm(objDataFromForm);
    if (vCheck) {
        callApiToPostData(objDataFromForm, gProvinceIdSelected, gDistrictIdSelected);
        onPageLoading();
    }

}
//hàm thu thập dư liệu trên form
function getDataOnForm(obj) {
    obj.name = $('#input-name').val();
    obj.prefix = $('#input-prefix').val();
    return obj
}
//hàm validate dữ liệu
function validateDataOnForm(paramObj) {
    if (paramObj.name == "") {
        alert("phải điền tên tỉnh");
        return false
    }
    if (paramObj.prefix == "") {
        alert("phải điền tên tỉnh");
        return false
    }
    if (paramObj.name == "") {
        alert("phải điền tên thành phố");
        return false
    }
    return true;
}
//hàm gọi api để tạo province mới
function callApiToPostData(paramOrderObject, provinceId, districtId) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/street/create/${districtId}/${provinceId}`,
            type: 'POST',
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrderObject),
            headers: gHeader,
            success: function (res) {

                resolve(res)

            },
            error: function (error) {
                reject(err)
            }
        })
    })

}
function deletedStreet() {
    callApiToDeleteData(gStreetIdSelected);
    onPageLoading();
}
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/street/delete/${id}`,
        type: 'DELETE',
        async: false,
        headers: gHeader,
        success: function (res) {
            alert('Delete success')

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}




