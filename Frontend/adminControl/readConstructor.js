
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gCONSTRUCTOR_COLS = ["id", "name", "description", "address", "phone", "phone2", "fax", "email", "website", "note", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gCONSTRUCTOR_STT_COL = 0;
const gCONSTRUCTOR_NAME_COL = 1;
const gCONSTRUCTOR_DES_COL = 2;
const gCONSTRUCTOR_ADDR_COL = 3;
const gCONSTRUCTOR_PHONE_COL = 4;
const gCONSTRUCTOR_PHONE2_COL = 5;
const gCONSTRUCTOR_FAX_COL = 6;
const gCONSTRUCTOR_EMAIL_COL = 7;
const gCONSTRUCTOR_WEBSITE_COL = 8;
const gCONSTRUCTOR_NOTE_COL = 9;
const gCONSTRUCTOR_ACTION_COL = 10;

var table = $("#constructor-table").DataTable({
    columns: [
        { data: gCONSTRUCTOR_COLS[gCONSTRUCTOR_STT_COL] },
        { data: gCONSTRUCTOR_COLS[gCONSTRUCTOR_NAME_COL] },
        { data: gCONSTRUCTOR_COLS[gCONSTRUCTOR_DES_COL] },
        { data: gCONSTRUCTOR_COLS[gCONSTRUCTOR_ADDR_COL] },
        { data: gCONSTRUCTOR_COLS[gCONSTRUCTOR_PHONE_COL] },
        { data: gCONSTRUCTOR_COLS[gCONSTRUCTOR_PHONE2_COL] },
        { data: gCONSTRUCTOR_COLS[gCONSTRUCTOR_FAX_COL] },
        { data: gCONSTRUCTOR_COLS[gCONSTRUCTOR_EMAIL_COL] },
        { data: gCONSTRUCTOR_COLS[gCONSTRUCTOR_WEBSITE_COL] },
        { data: gCONSTRUCTOR_COLS[gCONSTRUCTOR_NOTE_COL] },
        { data: gCONSTRUCTOR_COLS[gCONSTRUCTOR_ACTION_COL] },


    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gCONSTRUCTOR_ACTION_COL,
            defaultContent: `
					<i class="fas fa-edit edit-constructor"></i>
					<i class="fas fa-trash-alt delete-constructor"></i>
			`
        },

    ],
    scrollX: true,
    scrollCollapse: true,

    fixedColumns: true
})
//biến chứa hàng được chọn
var gRowSelected = ''
//biến chứa list constructor
var glistConstructor = [];
//biến chứa id constructor
var gConstructorIdSelected = '';
var gRowConstructorSelected = '';
var glistConstructorValidateUpdate = [];
//biến chứa user id
var gUserId = '';
//biến chứ header
var gHeader = ''
var gUserName = ''
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    const token = getCookie("token");
    const userId = getCookie("userId");
    const userName = getCookie('username')

    //Gọi API để lấy thông tin người dùng

    //Khai báo xác thực ở headers
    gHeader = {
        Authorization: "Token" + " " + token
    };
    //gán giá trị cho biến userId
    gUserId = Number(userId);
    gUserName = userName;
    console.log('CHECK USER ID', gUserId);
    console.log('CHECK HEADER', gHeader);
    console.log('CHECK USERNAME', gUserName);
    $('#username').html(gUserName)
    //tải trang
    onPageLoading().then((res) => {
        glistConstructor = res
        console.log('check data get all  from server', res);
        table.clear();
        table.rows.add(res);
        table.draw();
    }).catch((err) => {
        console.log(err)
    });
    //create new constructor
    $('#create-constructor').on('click', function () {
        onBtnAddConstructorClick();
    })
    $('#btn-create-constructor').on('click', function () {

        onBtnCreateConstructorOnModalClick();
    })
    //edit constructor
    $('#constructor-table').on('click', '.edit-constructor', function () {

        onBtnEditConstructorClick(this);
    })
    $('#btn-update-constructor').on('click', function () {
        onBtnUpdateOnModalClick()
    })

    //xóa constructor
    $('#constructor-table').on('click', '.delete-constructor', function () {

        onBtnDeleteConstructorClick(this);
    })
    $('#btn-confirm-delete-constructor').on('click', function () {

        onBtnConfirmOnDeleteModalClick(this);
    })
    //nhấn nút Logout
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })




})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm gọi api load danh sách constructor từ server khi tải trang
function onPageLoading() {

    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/constructor",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm thực hiện khi nhấn nút add new constructor
function onBtnAddConstructorClick() {
    $("#create-constructor-modal").modal("show");

}
//hàm thực hiện khi nhấn create constructor trên modal
function onBtnCreateConstructorOnModalClick() {
    createNewInvestor();
    $("#create-constructor-modal").modal("hide");
}
//hàm thực hiện khi lúc nút edit
function onBtnEditConstructorClick(paramButton) {
    $("#update-investor-modal").modal("show");
    gConstructorIdSelected = getInvestorIdFromButton(paramButton)
    console.log('check id customer selected', gConstructorIdSelected)
    callApiToGetConstructorById(gConstructorIdSelected).then((res) => {
        console.log('check data customer need to update ress', res);
        showDataUpdateOnForm(res);
    }).catch((err) => {
        console.log(err)
    });
}
//hàm thực hiện khi nhấn nút update trên modal
function onBtnUpdateOnModalClick() {
    updateConstructor(gConstructorIdSelected);
    $("#update-investor-modal").modal("hide");

}
//Hàm thực hiện khi nhấn nút delete
function onBtnDeleteConstructorClick(paramButton) {
    $("#delete-modal").modal("show");
    gConstructorIdSelected = getInvestorIdFromButton(paramButton)

}
//hàm thực hiện khi nhấn nút confirm trên delete modal
function onBtnConfirmOnDeleteModalClick() {
    deletedInvestor();
    $("#delete-modal").modal("hide");

}
//hàm thực hiện khi click nút log out 
function onBtnLogOutClick() {
    //
    redirectToLogin();

}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
//Hàm get Cookie 
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//hàm trả ra id constructor khi nhấn nút edit
function getInvestorIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    gRowConstructorSelected = table.row(vTableRow).data();
    console.log(gRowConstructorSelected.id);
    return gRowConstructorSelected.id;
}
//hàm gọi api để lấy constructor cần edit theo id
function callApiToGetConstructorById(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/constructor/detail/${id}}`,
            async: false,
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res);

            },
            error: function (err) {
                reject(e)
            }
        })
    })


}
//hàm hiển thị data lên form edit
function showDataUpdateOnForm(obj) {
    $('#input-name-update').val(obj.name);
    $('#input-description-update').val(obj.description);
    $('#input-address-update').val(obj.address);
    $('#input-phone-update').val(obj.phone);
    $('#input-phone2-update').val(obj.phone2);
    $('#input-fax-update').val(obj.fax);
    $('#input-email-update').val(obj.email);
    $('#input-website-update').val(obj.website);
    $('#input-note-update').val(obj.note);

}

function updateConstructor(paramInvestorId) {
    let objData = {
        name: '',
        description: '',
        address: '',
        phone: '',
        phone2: '',
        fax: '',
        email: '',
        website: '',
        note: ''
    }
    let objDataFromForm = getDataOnFormUpdate(objData);
    callApiToUpdateData(paramInvestorId, objDataFromForm).then((res) => {
        console.log(res);
    }).catch((xhr, status, error) => {
        if (xhr.responseJSON.message == "Forbidden") {
            alert("Bạn không có đủ quyền hạn để thực hiện chức năng này")
        } else {
            console.log(xhr, status, error);
            console.log()
        }
    })
    onPageLoading().then((res) => {
        glistConstructor = res
        console.log('check data get all  from server', res);
        table.clear();
        table.rows.add(res);
        table.draw();
    }).catch((err) => {
        console.log(err)
    });;

}




//hàm thu thập dư liệu trên form update
function getDataOnFormUpdate(obj) {
    obj.name = $('#input-name-update').val();
    obj.description = $('#input-description-update').val();
    obj.address = $('#input-address-update').val();
    obj.phone = $('#input-phone-update').val();
    obj.phone2 = $('#input-phone2-update').val();
    obj.fax = $('#input-fax-update').val();
    obj.email = $('#input-email-update').val();
    obj.website = $('#input-website-update').val();
    obj.note = $('#input-note-update').val();

    return obj
}
//hàm gọi api để update constructor
function callApiToUpdateData(id, paramOrderObject) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/constructor/update/${id}`,
            type: 'PUT',
            async: false,
            contentType: "application/json;charset=UTF-8",
            headers: gHeader,
            data: JSON.stringify(paramOrderObject),

            success: function (res) {
                resolve(res);

            },
            error: function (xhr, status, error) {
                reject(xhr, status, error)
            }
        })
    })

}

//save new constructor
function createNewInvestor() {
    let objData = {
        name: '',
        description: '',
        address: '',
        phone: '',
        phone2: '',
        fax: '',
        email: '',
        website: '',
        note: ''
    }
    let objDataFromForm = getDataOnForm(objData);
    var vCheck = validateDataOnForm(objDataFromForm);
    callApiToPostData(objDataFromForm).then(res => {

    }).catch((xhr, status, error) => {
        if (xhr.responseJSON.message == "Forbidden") {
            alert("Bạn không có đủ quyền hạn để thực hiện chức năng này")
        } else {
            console.log(xhr, status, error);
            console.log()
        }
    });;
    onPageLoading().then((res) => {
        glistConstructor = res
        console.log('check data get all  from server', res);
        table.clear();
        table.rows.add(res);
        table.draw();
    }).catch((err) => {
        console.log(err)
    });;
}
//hàm validate dữ liệu
function validateDataOnForm(paramObj) {
    if (paramObj.name == "") {
        alert("phải điền tên nhà đầu tư");
        return false
    }
    if (paramObj.phone == "") {
        alert("phải điền số điện thoại");
        return false
    }
    if (paramObj.email == "") {
        alert("phải điền email");
        return false
    }
    return true;
}
//hàm thu thập dư liệu trên form
function getDataOnForm(obj) {
    obj.name = $('#input-name').val();
    obj.description = $('#input-description').val();
    obj.address = $('#input-address').val();
    obj.phone = $('#input-phone').val();
    obj.phone2 = $('#input-phone2').val();
    obj.fax = $('#input-fax').val();
    obj.email = $('#input-email').val();
    obj.website = $('#input-website').val();
    obj.note = $('#input-note').val();
    return obj
}
//hàm gọi api để tạo province mới
function callApiToPostData(paramOrderObject) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/constructor/create`,
            type: 'POST',
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrderObject),
            headers: gHeader,
            success: function (res) {

                resolve(res)

            },
            error: function (xhr, status, error) {
                reject(xhr, status, error)
            }
        })
    })

}
function deletedInvestor() {
    callApiToDeleteData(gConstructorIdSelected);
    onPageLoading().then((res) => {
        glistConstructor = res
        console.log('check data get all  from server', res);
        table.clear();
        table.rows.add(res);
        table.draw();
    }).catch((err) => {
        console.log(err)
    });
}
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/constructor/delete/${id}`,
        type: 'DELETE',
        async: false,
        headers: gHeader,

        success: function (res) {
            alert('Delete success')

        },
        error: function (xhr, status, error) {
            if (xhr.responseJSON.message == "Forbidden") {
                alert("Bạn không có đủ quyền hạn để thực hiện chức năng này")
            } else {
                console.log(xhr, status, error);
                console.log()
            }
        }
    })
}




